#!/bin/bash

npm unlink \@ub-unibas/rdv_viewer-lib
mv publish.npmrc .npmrc
rm package-lock.json
yarn add \@ub-unibas/rdv_viewer-lib

