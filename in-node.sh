#!/bin/bash

nodeRunnng=`docker ps |grep node-ap-app`

if [ -z "${nodeRunnng}" ]; then
    # --service-ports enables http://localhost:3000 access!
    docker compose run --rm --service-ports --name afrikaportal node-ap-app "$@"
else
    docker exec -it afrikaportal "$@"
fi