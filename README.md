# Portal for African research collections

Based on Research Data Viewer (RDV)

## Configuration

The application is highly configurable by providing a configuration file. A couple of examples
can be found in `src/environments`:

* `elastic` (default) und `production` (production-ready, i.e. optimized for performance)
* `freidok` and `freidok-prod` (production-ready)
* `bwsts` and `bwsts-prod` (production-ready)
* `elastic-mh` and `elastic-mh-prod` (production-ready)

In order to use another configuration than the default one, start the server with the `-c` option and 
provide the name of the desired configuration, e.g. `ng serve -c freidok-prod`.

### Additional configurations

1. Copy an existing configuration and change the deviating values. The new configuration should implement
`SettingsModel`.
2. In the `angular.json` file, add respective configuration blocks in the `targets` where you want to use
your new configuration (i.e. at least the `build` and `serve` targets with paths 
`targets.build.configurations.new_config` and `targets.serve.configurations.new_config`).

An example configuration:

```json
{
  "targets": {
    "build": {
      "configurations": {
        "new_config": {
          "optimization": true,
          "outputHashing": "all",
          "sourceMap": false,
          "extractCss": true,
          "namedChunks": false,
          "aot": true,
          "extractLicenses": true,
          "vendorChunk": false,
          "buildOptimizer": true,
          "fileReplacements": [
            {
              "replace": "src/environments/environment.ts",
              "with": "src/environments/environment.new_config.ts"
            }
          ]
        }
      }
    } 
  }
}
```

Apart from `fileReplacements` all fields can be left out. To avoid repetition, you can also link
a settings block for a configuration from another target definition:

```json
{
  "targets": {
    "serve": {
      "configurations": {
        "new_config": {
          "browserTarget": "afrikaportal:build:new_config"
        }
      }
    }
  }
}
```

__Beware__: If you adjust the index and/or the type of the Elasticsearch cluster, you have to change also the respective settings in `angularx_elasticsearch_proxy_unibas.php`.


## Building documentation

You can build documentation locally with [compodoc](https://compodoc.github.io/compodoc/):

1. Install: `yarn global add @compodoc/compodoc` or `npm -g install @compodoc/compodoc`
2. Run in application root folder: `compodoc -swp src/tsconfig.app.json`


## Fonts

"Universität Basel" licensed some fonts which can't be published under
MIT license. To use your own fonts, edit src/scss/styles.scss,
comment out '@import "fonts"' and uncomment '@import "fonts-custom"'.

Finally place your font faces and declarations in src/scss/fonts-custom.scss.

## Local Development

Copy "src/assets/js/env.js.j2":

```
cp src/assets/js/env.js.j2 src/assets/js/env.js
```

Replace {{MAPBOX_TOKEN}} and {{MAPBOX_STYLE ...}} in env.js with your token and your style. E.g.:
```
sed -i -e "s/{{MAPBOX_TOKEN}}/<your mapbox token>/" env.js
sed -i -E -e "s/\{\{MAPBOX_STYLE.+\}\}/<your style url>/" env.js
```

Comment out this line with leading "//"
```
window.MAPBOX_STYLE = "...";
```
if you want to use a default mapbox style (mapbox://styles/mapbox/streets-v11):
```
// window.MAPBOX_STYLE = "...";
```

Then start the project (see below).

In the case of maplibre, create an empty file.

### Local Devleopment with docker

At first build your custom docker image:
```
docker compose build \
  --build-arg "USER=`git config user.name`" \
  --build-arg "EMAIL=`git config user.email`" \
  --build-arg "CI_JOB_TOKEN=<DEPLOY_TOKEN>"
```

The command above expects that git is locally working and configured. Replace `<DEPLOY_TOKEN>` with your personal gitlab deploy token. If you don't want to deploy something use the empty bash string ('').

Afterward use the new docker image to install and start the app:
```
./in-node.sh bash
yarn install
yarn run start
```
or after yarn install simply:
```
./in-node.sh yarn run start
```
Ctrl-C stops the app.

Finally point your browser to: http://localhost:4300/de

END