import {
  MiradorPlugins,
  initRdvLib,
} from "@ub-unibas/rdv_viewer-lib/components";
import { initMapReducer } from "@ub-unibas/rdv_viewer-lib/tool";
import { ParcSettingsModel } from "./shared/models/AfrikaportalSettingsModel";

export function initParcLib(
  environment: ParcSettingsModel,
  pluginsToDisable?: MiradorPlugins[]
): void {
  initRdvLib(environment, pluginsToDisable);
  const mapConfig = environment.mapConfig.config;
  initMapReducer(mapConfig.zoom, mapConfig.lat, mapConfig.lng);
}
