import { ChangeDetectionStrategy, Component, OnInit } from "@angular/core";
import { Title } from "@angular/platform-browser";
import { environment } from "@env/environment";
import { Store } from "@ngrx/store";
import { TranslateService } from "@ngx-translate/core";
import {
  AbstractAppComponent,
  RemoteFilterConfigState,
  SearchStateInterface,
} from "@ub-unibas/rdv_viewer-lib/components";
import { LoadCountries } from "./countries/actions/countries.actions";
import { LoadExhibitions } from "./exhibitions/actions/exhibitions.actions";

const root = document.querySelector(":root") as HTMLElement;

// 100vh mobile fix: sets css variable --vh100
const vh100Fix = () => {
  root.style.setProperty("--vh100", window.innerHeight + "px");
  root.style.setProperty("--vw100", window.innerWidth + "px");
  root.style.setProperty("--viw", Math.min(1680, window.innerWidth) + "");
};
window.addEventListener("resize", vh100Fix);
vh100Fix();

/**
 * Main entry point to the application.
 */
@Component({
  selector: "app-root",
  template: ` <router-outlet class="g-0"></router-outlet> `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent extends AbstractAppComponent implements OnInit {
  constructor(
    protected rootStore: Store<RemoteFilterConfigState>,
    protected translate: TranslateService,
    protected titleService: Title,
    protected searchStore: Store<SearchStateInterface>
  ) {
    super(rootStore, translate, titleService);
  }

  ngOnInit(): void {
    const countryFacetKey = environment.countryFacetKey;
    const facet = environment.facetFields[countryFacetKey];

    this.searchStore.dispatch(new LoadCountries({ facet, language: "de" }));

    this.searchStore.dispatch(
      new LoadExhibitions({ language: this.translate.currentLang })
    );
  }
}
