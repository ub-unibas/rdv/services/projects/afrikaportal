import {
  Location,
  LocationStrategy,
  PathLocationStrategy,
} from "@angular/common";
import { HttpClient } from "@angular/common/http";
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { LocalizeRouterService } from "@gilsdav/ngx-translate-router";
import { Actions } from "@ngrx/effects";
import { Store } from "@ngrx/store";
import { TranslateService } from "@ngx-translate/core";
import {
  BackendSearchService,
  DetailedComponent,
  I18nToastrService,
  PreviousRouteServiceProvider,
  ResetUtilService,
  SearchStateInterface,
  ViewerType,
} from "@ub-unibas/rdv_viewer-lib/components";
import { BehaviorSubject, take } from "rxjs";
import { environment } from "@env/environment";

interface IiifPreviewImageResponse {
  "@id": string;
  width: number;
  height: number;
  sizes: { width: number; height: number }[];
  tiles: { height: number; width: number; scaleFactors: number[] }[];
  profile: [
    string,
    {
      formats: ("jpg" | "gif" | "png" | "tif")[];
      qualities: ("bitonal" | "default" | "gray" | "color")[];
    }
  ];
}

interface PreviewUrl {
  id: any;
  url: string;
}

let globalShowIiiFViewer = false;

@Component({
  selector: "app-custom-detail",
  template: `
    <app-page-layout
      baseClass="page-detail"
      [facetsInitiallyClosed]="true"
      [showSearchAndFilter]="false"
    >
      <div>
        <div class="me-auto" class="page-detail__nav-container d-flex">
          <a class="back-to-search link-black" (click)="backToSearch()">{{
            "detailed.search_result" | translate
          }}</a>
          <div class="nav flex-grow-1 justify-content-start">
            <button
              *ngIf="hasCursor(cursor)"
              type="button"
              class="previous-doc btn btn-link link-black"
              (click)="showPreviousDoc()"
            ></button>
            <button
              *ngIf="hasCursor(cursor)"
              type="button"
              class="next-doc btn btn-link link-black"
              (click)="showNextDoc()"
            ></button>
          </div>
        </div>
      </div>
      <div class="d-sm-flex g-0 flex-sm-row-reverse">
        <div class="flex-grow-1">
          <ng-container *ngIf="!empty(objectType(meta$ | async))">
            <div class="search-hit__type">
              <app-search-hit-value
                [fieldValue]="objectType(meta$ | async)"
              ></app-search-hit-value>
            </div>
          </ng-container>
          <div class="row g-0">
            <h3
              *ngIf="!empty(objectTitle(meta$ | async))"
              class="detailed__title col"
              [class.detailed__title--no-preview]="
                !hasPreviewUrl(meta$ | async)
              "
            >
              <app-search-hit-value
                [fieldValue]="objectTitle(meta$ | async)"
              ></app-search-hit-value>
            </h3>
          </div>
          <ng-container *ngIf="doctype">
            <div class="search-hit__type">
              <app-search-hit-value
                [fieldValue]="doctypeLabel()"
              ></app-search-hit-value>
            </div>
            <div class="row g-0">
              <h3 class="detailed__title col">
                <app-search-hit-value
                  [fieldValue]="'detailed.new' | translate"
                ></app-search-hit-value>
              </h3>
            </div>
          </ng-container>
          <div>
            <div
              *ngIf="
                (showIiifViewer$ | async) === false &&
                hasPreviewUrl(meta$ | async)
              "
              class="page-detail__preview"
            >
              <div class="page-detail__preview-img-container">
                <div #previewContainer class="page-detail__preview-img">
                  <img
                    src="{{
                      getPreviewUrl(
                        meta$ | async,
                        previewImageUrl$ | async,
                        previewContainer
                      )
                    }}"
                  />
                </div>
              </div>
              <ng-container *ngIf="manifestUrl">
                <button
                  (click)="showViewer(true)"
                  class="page-detail__viewer-control"
                >
                  {{ "detailed.show_preview_in_viewer" | translate }}
                </button>
              </ng-container>
            </div>
            <ng-container
              *ngIf="
                (showIiifViewer$ | async) === true &&
                hasPreviewUrl(meta$ | async)
              "
            >
              <app-universal-viewer
                *ngIf="displayInUV(resultDisplayMode$ | async)"
                [manifestUrl]="manifestUrl"
                version="{{ resultDisplayMode$ | async }}"
              ></app-universal-viewer>
              <app-mirador
                *ngIf="displayInMirador(resultDisplayMode$ | async)"
                [manifestUrl]="manifestUrl"
                version="{{ resultDisplayMode$ | async }}"
              ></app-mirador>
              <div class="page-detail__preview">
                <button
                  (click)="showViewer(false)"
                  class="page-detail__viewer-control"
                >
                  {{ "detailed.hide_preview_in_viewer" | translate }}
                </button>
              </div>
            </ng-container>
          </div>
          <div
            class="detailed__fields"
            [class.detailed__fields--wide]="horizEmbedIiifViewer()"
          >
            <ul class="no-list-style">
              <li
                *ngFor="let field of fields$ | async"
                class="page-detail__no-group-fields"
              >
                <app-detailed-field
                  *ngIf="!editField(field)"
                  [fieldId]="field.field_id"
                  [label]="field.label"
                  [value]="field.value"
                  [meta]="field.edit_service"
                ></app-detailed-field>
              </li>
              <li *ngFor="let fieldGroup of fieldGroups$ | async">
                <app-collapsible
                  [title]="fieldGroup.name | translate"
                  [open]="(openGroups$ | async)[fieldGroup.name]"
                  [key]="'fg-' + fieldGroup.name"
                  (changed)="toggleGroupOpen(fieldGroup.name, $event)"
                >
                  <ul class="no-list-style">
                    <li *ngFor="let field of fieldGroup.fields">
                      <app-detailed-field
                        *ngIf="!editField(field)"
                        [fieldId]="field.field_id"
                        [label]="field.label"
                        [value]="field.value"
                        [meta]="field.edit_service"
                      ></app-detailed-field>
                    </li>
                  </ul>
                </app-collapsible>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </app-page-layout>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class DetailPageComponent
  extends DetailedComponent
  implements AfterViewInit
{
  protected showIiifViewer$: BehaviorSubject<boolean>;
  protected previewImageUrl$: BehaviorSubject<PreviewUrl | null>;

  protected returnPath: string;

  constructor(
    _backendSearchService: BackendSearchService,
    _route: ActivatedRoute,
    router: Router,
    _location: Location,
    _searchStore: Store<SearchStateInterface>,
    localize: LocalizeRouterService,
    activeRoute: ActivatedRoute,
    _translate: TranslateService,
    resetUtilService: ResetUtilService,
    actions$: Actions,
    toastr: I18nToastrService,
    protected http: HttpClient,
    protected previousRoute: PreviousRouteServiceProvider
  ) {
    super(
      _backendSearchService,
      _route,
      router,
      _location,
      _searchStore,
      localize,
      activeRoute,
      _translate,
      resetUtilService,
      actions$,
      toastr
    );
    this.showIiifViewer$ = new BehaviorSubject<boolean>(false);
    this.previewImageUrl$ = new BehaviorSubject<PreviewUrl | null>(null);
    let pr: string;
    previousRoute.previousRoutePath.pipe(take(1)).subscribe((v) => (pr = v));
    const routeMatch = pr.match(/^\/[^\/]+(\/[^\?]+)(\?.*)?$/);
    if (routeMatch && routeMatch.length >= 2) {
      pr = routeMatch[1];
    }
    this.returnPath = pr;
  }

  /**
   * @override
   *
   * @param resultDisplayMode
   * @returns
   */
  displayInUV(resultDisplayMode): boolean {
    return (
      !this.doctype &&
      resultDisplayMode !== "List" &&
      environment.documentViewer[resultDisplayMode].type === ViewerType.UV
    );
  }

  /**
   * @override
   *
   * @param resultDisplayMode
   * @returns
   */
  displayInMirador(resultDisplayMode): boolean {
    return (
      !this.doctype &&
      resultDisplayMode !== "List" &&
      environment.documentViewer[resultDisplayMode].type === ViewerType.MIRADOR
    );
  }

  // geo search returns cursor with value "null"!
  hasCursor(cursor: any): boolean {
    if (!cursor) {
      return false;
    }
    if (typeof cursor === "string") {
      return cursor !== "null";
    }
    return true;
  }

  objectType(meta): string {
    if (meta) {
      return meta.object_type;
    } else {
      return "";
    }
  }

  objectTitle(meta): string {
    if (meta) {
      return meta.title;
    } else {
      return "";
    }
  }

  showViewer(flag: boolean) {
    this.showIiifViewer$.next(flag);
    globalShowIiiFViewer = flag;
  }

  backToSearch() {
    // user started with deep detail page link
    if (this.returnPath.startsWith("/detail") || this.returnPath.length === 0) {
      this.router.navigate([this.localize.translateRoute("/")]);
    } else {
      this.router.navigate([this.localize.translateRoute(this.returnPath)]);
    }
  }

  asText(a: any): string {
    return JSON.stringify(a);
  }

  ngAfterViewInit(): void {
    super.ngAfterViewInit();
    this.resultDisplayModeSubject.next("MiradorSinglePage");
    this.showIiifViewer$.next(globalShowIiiFViewer);
  }

  protected getPreviewJson(
    meta: any,
    thumbnailUrl: string,
    previewContainer: HTMLDivElement
  ): any {
    const url = meta?.preview_image;
    if (url) {
      // can't use @angular/common/http with json response type, because then the "origin" header would
      // be added to the request; but it works with response type "text"!
      this.http
        .get(url, { responseType: "text" })
        .pipe(take(1))
        .subscribe((response) => {
          try {
            const previewJson: IiifPreviewImageResponse = JSON.parse(response);
            const availableWidth = Math.floor(
              previewContainer.getBoundingClientRect().width
            );
            // compare max-height: 50vh of <img> in _page-detail.scss
            const availableHeight = Math.floor(window.innerHeight * 0.5);
            // https://media.ub.unibas.ch/bablibrary-010/x 2356/iiif/full/200,/0/default.jpg
            const [
              _url,
              baseImgUrl,
              mode,
              _widthHeight,
              rotation,
              quality,
              format,
            ] = thumbnailUrl.match(
              /^(.*)\/([a-z]+)\/([^/]+)\/([0-9]+)\/(.+)\.(.+)$/
            );
            let imgWidth = availableWidth;
            const lastSize = previewJson.sizes[previewJson.sizes.length - 1];
            const imgScale = lastSize.height / lastSize.width;
            if (availableWidth * imgScale > availableHeight) {
              imgWidth = Math.round(availableHeight / imgScale);
            }
            const imgUrl = `${baseImgUrl}/${mode}/${imgWidth},/${rotation}/${quality}.${format}`;
            this.previewImageUrl$.next({ id: this.id, url: imgUrl });
          } catch (ex) {
            console.error("Can't parse json: " + response, ex);
          }
        });
    }
  }

  hasPreviewUrl(meta: any): boolean {
    return !!meta?.preview_image;
  }

  getPreviewUrl(
    meta: any,
    currentPreviewUrl: PreviewUrl | null,
    previewContainer: HTMLDivElement
  ): string {
    if (currentPreviewUrl == null || this.id !== currentPreviewUrl.id) {
      if (this.manifestUrl) {
        let url = this.manifestUrl;
        if (!url.endsWith("/") && !url.includes("?")) {
          url += "/";
        }
        this.http
          .get(url, { responseType: "text" })
          .pipe(take(1))
          .subscribe((response) => {
            try {
              const manifestJson = JSON.parse(response);
              const thumbnailUrl = manifestJson.items[0]?.thumbnail[0]?.id;
              if (thumbnailUrl) {
                this.getPreviewJson(meta, thumbnailUrl, previewContainer);
              }
            } catch (ex) {
              console.error("Can't parse manifest json: " + response, ex);
            }
          });
      }
      return "";
    } else {
      return currentPreviewUrl.url;
    }
  }
}
