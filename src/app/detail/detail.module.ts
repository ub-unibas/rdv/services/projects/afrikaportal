import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "@app/shared/shared.module";
import { LocalizeRouterModule } from "@gilsdav/ngx-translate-router";
import { TranslateModule } from "@ngx-translate/core";
import {
  PipesModule,
  SearchModule,
} from "@ub-unibas/rdv_viewer-lib/components";
import { ToastrModule } from "ngx-toastr";
import { DetailPageComponent } from "./components/detail-page.component";

const routes: Routes = [{ path: "", component: DetailPageComponent }];

const DETAIL_PAGE_COMPONENTS = [DetailPageComponent];

@NgModule({
  imports: [
    CommonModule,
    PipesModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild(),
    LocalizeRouterModule.forChild(routes),
    ToastrModule,
    SharedModule,
    SearchModule,
  ],
  declarations: DETAIL_PAGE_COMPONENTS,
  providers: [],
  exports: DETAIL_PAGE_COMPONENTS,
})
export class DetailModule {}
