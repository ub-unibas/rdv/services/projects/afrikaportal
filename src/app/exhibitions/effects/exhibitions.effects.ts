import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "@env/environment";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { catchError, filter, map, of, switchMap } from "rxjs";
import {
  ExhibitionsActionTypes,
  ExhibitionsResponseError,
  ExhibitionsResponseOk,
  LoadExhibitions,
  SendExhibitionsRequest,
  SetExhibitions,
} from "../actions/exhibitions.actions";

@Injectable()
export class ExhibitionsEffects {
  requestSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ExhibitionsActionTypes.ExhibitionsResponseOk),
      map((action: ExhibitionsResponseOk) => action.payload),
      filter((x) => !!x),
      switchMap(({ language, exhibitions }) => [
        new SetExhibitions({
          language,
          exhibitions: exhibitions.filter(
            (x) => typeof x === "object" && Object.keys(x).length > 0
          ),
        }),
      ])
    )
  );

  requestFailure$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ExhibitionsActionTypes.ExhibitionsResponseError),
      map((action: ExhibitionsResponseError) => action.payload),
      switchMap((err) => {
        console.error("Can't load exhibitions", err);
        return [];
      })
    )
  );

  makeRequest$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ExhibitionsActionTypes.SendExhibitionsRequest),
      map((action: SendExhibitionsRequest) => action.payload),
      switchMap(({ url, query }) => {
        url += encodeURIComponent(query.lang) + "/";
        const qmPos = url.indexOf("?");
        const urlSeparator = qmPos >= 0 ? "&" : "?";
        url += urlSeparator + "ic=1";

        return this.http
          .get(url, {
            headers: {
              "Content-Type": "application/json",
              "X-Request-Type": "exhibitions",
            },
            withCredentials: true,
          })
          .pipe(
            map(
              (result: any) =>
                new ExhibitionsResponseOk({
                  exhibitions: result ?? [],
                  language: query.lang,
                })
            ),
            catchError((err) => of(new ExhibitionsResponseError(err)))
          );
      })
    )
  );

  loadExhibitions$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ExhibitionsActionTypes.LoadExhibitions),
      map((action: LoadExhibitions) => action.payload),
      switchMap(({ language }) => [
        new SendExhibitionsRequest({
          url: environment.exhibitionProxyUrl,
          query: {
            lang: language,
          },
        }),
      ])
    )
  );

  constructor(protected actions$: Actions, protected http: HttpClient) {}
}
