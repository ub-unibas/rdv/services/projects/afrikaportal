import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { EffectsModule } from "@ngrx/effects";
import { StoreModule } from "@ngrx/store";
import { TranslateModule } from "@ngx-translate/core";
import { ExhibitionsEffects } from "./effects/exhibitions.effects";
import { reducers } from "./reducers";

const COMPONENTS = [];

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature("exhibitions", reducers),
    EffectsModule.forFeature([ExhibitionsEffects]),
    TranslateModule.forChild(),
  ],
  declarations: COMPONENTS,
  providers: [],
  exports: COMPONENTS,
})
export class ExhibitionsModule {}
