import { Action } from "@ngrx/store";

export interface Exhibition {
  Titel: string;
  Untertitel: string;
  Beschreibung: string;
  "URL-Thumbnail": string;
  "URL-Exhibit": string;
}

/* eslint-disable @typescript-eslint/no-shadow */
export enum ExhibitionsActionTypes {
  SetExhibitions = "[Exhibitions] Set exhibitions",
  LoadExhibitions = "[Exhibitions] Load exhibitions",
  SendExhibitionsRequest = "[Exhibitions] Send exhibitions request",
  ExhibitionsResponseOk = "[Exhibitions] Exhibitions response ok",
  ExhibitionsResponseError = "[Exhibitions] Exhibitions response error",
}
/* eslint-enable @typescript-eslint/no-shadow */

/**
 * Set de Exhibitions
 */
export class SetExhibitions implements Action {
  /**
   * @ignore
   */
  readonly type = ExhibitionsActionTypes.SetExhibitions;

  /**
   * Contains all results to be displayed
   * @param payload
   */
  constructor(
    public payload: { exhibitions: Exhibition[]; language: string }
  ) {}
}

/**
 * Loads Exhibitions
 */
export class LoadExhibitions implements Action {
  /**
   * @ignore
   */
  readonly type = ExhibitionsActionTypes.LoadExhibitions;

  /**
   * Contains all results to be displayed
   * @param payload
   */
  constructor(public payload: { language: string }) {}
}

/**
 * Loads Exhibitions
 */
export class SendExhibitionsRequest implements Action {
  /**
   * @ignore
   */
  readonly type = ExhibitionsActionTypes.SendExhibitionsRequest;

  /**
   * Contains all results to be displayed
   * @param payload
   */
  constructor(public payload: { url: string; query: any }) {}
}

/**
 * Ok Exhibitions reponse
 */
export class ExhibitionsResponseOk implements Action {
  /**
   * @ignore
   */
  readonly type = ExhibitionsActionTypes.ExhibitionsResponseOk;

  /**
   * Contains all results to be displayed
   * @param payload
   */
  constructor(
    public payload: { exhibitions: Exhibition[]; language: string }
  ) {}
}

/**
 * Ok Exhibitions reponse
 */
export class ExhibitionsResponseError implements Action {
  /**
   * @ignore
   */
  readonly type = ExhibitionsActionTypes.ExhibitionsResponseError;

  /**
   * Contains all results to be displayed
   * @param payload
   */
  constructor(public payload: string) {}
}

export type ExhibitionsActions =
  | SetExhibitions
  | LoadExhibitions
  | SendExhibitionsRequest
  | ExhibitionsResponseOk
  | ExhibitionsResponseError;
