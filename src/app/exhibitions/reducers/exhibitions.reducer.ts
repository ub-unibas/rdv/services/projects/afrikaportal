import {
  Exhibition,
  ExhibitionsActionTypes,
  ExhibitionsActions,
} from "../actions/exhibitions.actions";

export interface State {
  de: Exhibition[];
  en: Exhibition[];
}

/**
 * @ignore
 */
export const initialState: State = { de: [], en: [] };

/**
 * @ignore
 */
export function reducer(
  state = initialState,
  action: ExhibitionsActions
): State {
  switch (action.type) {
    case ExhibitionsActionTypes.SetExhibitions: {
      const newState = { ...state };
      newState[action.payload.language] = action.payload.exhibitions;
      return newState;
    }

    default: {
      return state;
    }
  }
}
