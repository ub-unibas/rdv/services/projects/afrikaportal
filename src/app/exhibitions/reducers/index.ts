import {
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
} from "@ngrx/store";
import * as fromExhibitions from "./exhibitions.reducer";

interface ExhibitionsState {
  exhibitions: fromExhibitions.State;
}

export const reducers: ActionReducerMap<ExhibitionsState> = {
  exhibitions: fromExhibitions.reducer,
};

const _getExhibitions = createFeatureSelector<ExhibitionsState>("exhibitions");

export const getExhibitionsState = createSelector(
  _getExhibitions,
  (state) => state.exhibitions
);

export const getDeExhibitions = createSelector(
  getExhibitionsState,
  (state) => state.de
);

export const getEnExhibitions = createSelector(
  getExhibitionsState,
  (state) => state.en
);
