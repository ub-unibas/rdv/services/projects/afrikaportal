import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import { ChangeDetectionStrategy, Component } from "@angular/core";

@Component({
  selector: "app-contact-en",
  template: `
    <div class="static-page static-page--no-mood">
      <div class="help-page__content">
        <h1>FAQ</h1>
        <h2>Questions and answers about the map search:</h2>
        <ul class="help-page__arrow-list">
          <li>
            <b>Which entries are displayed on the map?</b><br />For an
            entry/medium to be displayed on the map, we need a geographical
            location within its metadata. If this is not the case, or if there
            is only a geo-reference to a country, the entry cannot be displayed
            as a point on the map or is grouped under a flag icon with other
            entries of this country (see below).
          </li>
          <li>
            <b>Why are there flags on the map?</b><br />The flags represent
            entries that contain only a country as geographical information. A
            click on the flag leads to a search query with this country as the
            filter.
          </li>
          <li>
            <b>How does the time filter work?</b><br />When the map search is
            started, the time filter is inactive. If a time filter is desired,
            it must be set manually by using the time bar, the input fields or
            the time facet.
          </li>
          <li>
            <b
              >Why do I always have a "Coordinates:…" filter in the map
              search?</b
            ><br />Any navigation on the map always leads to a new search. This
            allows the search results to change "live" as you move around the
            map. The filter of this search is defined by the corresponding
            "coordinates".
          </li>
          <li>
            <b>Will I see all the results on the map?</b><br />Only the largest
            300 clusters and/or points can be displayed on each map section.
          </li>
        </ul>
        <h2>Questions and answers about PARC and the collections:</h2>
        <ul class="help-page__arrow-list">
          <li>
            <b>I have a question about the collections. Who can I contact?</b
            ><br />If you have questions or suggestions, please contact the
            institution that provided the data.
          </li>
          <li>
            <b>I have questions or suggestions about PARC. Who can I contact?</b
            ><br />Please contact us. You will find our contact details in the
            footer under
            <a
              class="link-black"
              [title]="'footer.link_contact' | translate"
              [routerLink]="['/simple/contact' | localize]"
            >
              "Contact"</a
            >.
          </li>
        </ul>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class HelpEnComponent {
  constructor() {}
}
