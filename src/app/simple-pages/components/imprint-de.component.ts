import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import { ChangeDetectionStrategy, Component } from "@angular/core";

@Component({
  selector: "app-imprint-de",
  template: `
    <div class="static-page static-page--no-mood">
      <div class="imprint-page__content">
        <h1>Impressum</h1>
        <h2>Herausgeber</h2>
        <p>
          PARC, c/o Universitätsbibliothek Basel<br />
          Schönbeinstrasse 18-20<br />
          CH-4056 Basel<br />
          <a target="_blank" href="https://www.parc-portal.org/de/">
            www.parc-portal.org</a
          ><br />
        </p>

        <h2>Technische Realisierung und Betrieb</h2>
        <p>
          Universitätsbibliothek Basel, Informatik<br />
          <a target="_blank" href="https://www.ub.unibas.ch/de/">
            www.ub.unibas.ch</a
          ><br />
        </p>
        <h2>Design, User Interaction, Front-End</h2>
        <p>
          OUTERMEDIA GmbH<br />
          <a target="_blank" href="https://www.outermedia.de">
            www.outermedia.de</a
          ><br />
        </p>
        <h2>Datenverarbeitung, Normalisierung und Anreicherung</h2>
        <p>
          arbim IT<br />
          <a target="_blank" href="https://arbim.ch/">arbim.ch</a><br />
        </p>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class ImprintDeComponent {
  constructor() {}
}
