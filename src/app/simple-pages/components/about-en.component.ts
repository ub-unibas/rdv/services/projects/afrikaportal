import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import { ChangeDetectionStrategy, Component } from "@angular/core";

@Component({
  selector: "app-about-en",
  template: `
    <div class="static-page">
      <h1>About PARC</h1>
      <div class="about-page__content">
        <div class="about-page__description">
          <p>
            PARC (Portal for African Research Collections) is the new
            meta-catalogue for the Africana collections at the research location
            Basel. For the first time, the collections of the five Basel
            heritage institutions Basler Afrika Bibliographien, Mission 21,
            Museum der Kulturen Basel, Swiss TPH and the University of Basel are
            searchable in a single catalogue. This facilitates access to the
            collections regardless of location.
          </p>
          <p>
            In addition to bibliographic and archival collections, ethnographic
            objects, photographs, posters, films/videos, sound recordings and
            digital collections (incl. audio-visual media) can also be found.
          </p>
          <h2>Technicality</h2>
          <p>
            PARC is to be understood as an innovation project. Due to the
            different indexing traditions, both in the institutions and with
            regard to media types, a common data model was dispensed with, and
            instead alternative paths for data integration were taken.
          </p>
          <p>
            PARC is open to other partners, especially African ones. Therefore,
            PARC is technically oriented towards the demands and challenges of
            the African market, so that access is guaranteed and knowledge
            exchange can take place. This includes optimisation for
            low-bandwidth operation as well as tools for decentralised data
            management so that data providers can retain ownership of their
            data. The data will not be adapted to a predefined data model but
            will be adopted directly.
          </p>
          <p>
            If you are interested, please contact: Reto Ulrich,
            ru@baslerafrika.ch We would be happy to consider your participation
            in PARC.
          </p>
          <h2>Project History</h2>
          <table class="about-page__history">
            <tr>
              <td>2008</td>
              <td>
                The first technically very simple Africa portal was set up on
                the Centre for African Sutdies Basel (CASB) homepage by the
                University Library Basel. The library holdings of the five Basel
                institutions could be searched using a common search slot; the
                results from the individual catalogues were listed separately.
              </td>
            </tr>
            <tr>
              <td>2016</td>
              <td>
                The University Library is looking for partners for the
                technically improved new edition of the Africa Portal. The
                University of Basel under the leadership of the University
                Library, with the Basler Afrika Bibliographien, Mission 21, the
                Museum der Kulturen Basel and the Swiss TPH agree on cooperating
                on bringing all collections together in one catalogue.
              </td>
            </tr>
            <tr>
              <td>2018</td>
              <td>
                Internal funding for the project fails. It is decided to seek
                third-party funding.
              </td>
            </tr>
            <tr>
              <td>2019</td>
              <td>The search for third-party funding is successful.</td>
            </tr>
            <tr>
              <td>2020</td>
              <td>
                The project starts with a focus on Basel collections and
                usability for the African market.
              </td>
            </tr>
            <tr>
              <td>2023</td>
              <td>Launch of PARC</td>
            </tr>
            <tr>
              <td>2024</td>
              <td>
                Opening of the portal to other, especially African institutions
                and collections.
              </td>
            </tr>
          </table>
        </div>
        <app-about-page-links></app-about-page-links>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class AboutEnComponent {
  constructor() {}
}
