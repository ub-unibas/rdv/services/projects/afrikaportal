import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import { ChangeDetectionStrategy, Component, Input } from "@angular/core";
import { Exhibition } from "@app/exhibitions/actions/exhibitions.actions";

@Component({
  selector: "app-exhibitions-en",
  template: `
    <div>
      <h1>Exhibitions</h1>
      <p class="exhibition-page__description">
        Discover exhibition projects and digital presentations from the
        institutions and collections represented in PARC.
      </p>
      <app-exhibition-preview
        [exhibitions]="exhibitions"
        [startIndex]="0"
        [count]="300"
        [showAllButton]="false"
      ></app-exhibition-preview>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class ExhibitionEnComponent {
  @Input() exhibitions: Exhibition[];

  constructor() {}
}
