import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import { ChangeDetectionStrategy, Component } from "@angular/core";

@Component({
  selector: "app-ethical-guidelines-de",
  template: `
    <div class="static-page static-page--no-mood">
      <div class="ethical-guidelines-page__content">
        <h1>Ethische Leitlinien</h1>
        <p>
          Afrikanische Forschungssammlungen wurden oft unter kolonialen und
          rassistischen Umständen angelegt und werden bezüglich ihrer Inhalte
          und Metadaten als beleidigend und diskriminierend erachtet. Sie werfen
          eine Reihe von ethischen und anderen Fragen auf, einschliesslich
          Eigentums- und Urheberrechtsfragen.
        </p>
        <p>
          Die Online-Präsentation dieser Sammlungen kann zur Reproduktion
          kolonialer und rassistischer Anschauungen führen, die wir nicht
          teilen. Wir setzen uns für antirassistische und dekoloniale Anliegen
          ein.
        </p>
        <p>
          Als Teil dieser Bestrebungen betrachten wir es als eine wesentliche
          Aufgabe von Bibliotheken, Archiven und Museen, Sammlungen zugänglich
          zu machen, die für die Bevölkerung, die Geschichte und Kultur der
          Region und die Wissenschaft von Bedeutung sind.
        </p>
        <p>
          Wir sind bemüht, in unseren Datenbanken beleidigende, diskriminierende
          und unangemessene Inhalte visueller oder sprachlicher Art zu
          beseitigen und unsere Kataloge kontinuierlich zu aktualisieren. Um
          unseren ethischen Ansatz, unsere Erschliessungsarbeit und die
          Zugänglichkeit zu den Sammlungen zu verbessern, sind wir für Ihr
          Feedback dankbar. Bitte senden Sie Ihre Kommentare per E-Mail an die
          jeweilige Sammlungsinstitution.
        </p>
        <h2>Hinweise zur Sprache</h2>
        <p>
          Sammlungen, Dokumente und Katalogeinträge können koloniales und
          rassistisches Vokabular enthalten, welches unzutreffend,
          herabwürdigend und verletzend ist.
        </p>
        <p>
          Einerseits können Katalogeinträge wie Titel von Büchern, Manuskripten,
          Tonaufnahmen und audiovisuellen Medien sowie Bildunterschriften und
          Kommentare anstössiges Vokabular enthalten. Andererseits können die
          von den Sammlungsinstitutionen bereitgestellten Schlagwörter koloniale
          Kategorisierungen wiedergeben.
        </p>
        <p>
          Wir arbeiten daran, unsere Kataloge zu verbessern und ethisch
          verantwortungsvolle und dekoloniale Wege zu finden, mit dem kolonialen
          Erbe in unseren Datenbanken umzugehen. Dies ist ein laufender Prozess,
          und wir freuen uns über Ihre Anregungen.
        </p>
        <h2>Ethische Bedenken in Bezug auf visuelle Medien</h2>
        <p>
          Das Portal beabsichtigt, Sammlungen von Fotografien, Filmen,
          Illustrationen und anderen visuellen Medien, die derzeit in
          europäischen Institutionen aufbewahrt werden, einem breiteren Publikum
          zugänglich zu machen, insbesondere in afrikanischen Ländern. Die
          Digitalisierung und Online-Präsentation von Bildern aus kolonialen
          Kontexten ist jedoch mit zahlreichen ethischen Herausforderungen
          verbunden.
        </p>
        <p>
          Trägt die Online-Präsentation kolonialer visueller Darstellungen zur
          Reproduktion kolonialer und rassistischer Anschauungen bei?
        </p>
        <p>
          Sollen Bilder, die ohne die Zustimmung der abgebildeten Personen
          aufgenommen wurden, im Internet verbreitet werden, oder stellt dies
          möglicherweise eine neue Form kolonialer Gewaltausübung dar?
        </p>
        <p>
          Wie lässt sich der Mangel an Kontextinformationen in zahlreichen
          Bildsammlungen beheben?
        </p>
        <p>
          Wie können wir unserer Verantwortung gerecht werden, Sammlungen
          zugänglich zu machen und gleichzeitig Fragen der Ethik und Sorgfalt
          bei der Kuratierung von Online-Katalogen zu berücksichtigen?
        </p>
        <p>
          Fotografien und andere visuelle Medien haben in vielerlei Hinsicht zu
          kolonialen Projekten beigetragen. In Bezug auf fotografische
          Sammlungen verweisen wir u. a. auf die folgenden "Genres":
        </p>
        <ul class="ethical-guidelines-page__arrow-list">
          <li>
            <p>
              Personenaufnahmen ohne Namen: Während der Kolonialzeit und darüber
              hinaus haben Fotograf:innen in der Regel nicht die vollständigen
              und/oder korrekten Namen der abgebildeten Personen angegeben. Wir
              freuen uns über zusätzliche Angaben, die dazu beitragen, nicht
              verzeichnete Namen zu ermitteln und Personen zu identifizieren.
            </p>
          </li>
          <li>
            <p>
              Darstellungen von Armut und extremen Klassenverhältnissen: Der
              koloniale und rassistische Kontext, in dem die Sammlungen
              entstanden sind, führte zu extremen Klassenverhältnissen und einem
              Genre von Bildern, die sich auf Armut beziehen. Viele Bilder
              spiegeln implizit oder explizit koloniale und strukturell
              gewalttätige Verhältnisse wider.
            </p>
          </li>
          <li>
            <p>
              Fotografien, die "geheime" Anlässe oder Objekte zeigen: In manchen
              Sammlungen können sich Bilder befinden, die Anlässe, Orte oder
              Objekte zeigen, die nach den ethischen und kulturellen Kodizes der
              Herkunftsgemeinschaften nur von bestimmten Personen gesehen werden
              sollten.
            </p>
            <p>
              Wir versuchen nach bestem Wissen und Gewissen, solche Bilder nicht
              online zu zeigen. Sollten Sie solche Bilder in unseren Katalogen
              finden, kontaktieren Sie uns bitte.
            </p>
          </li>
          <li>
            <p>
              Anthropometrische Fotografien: Während der gesamten Kolonialzeit
              und darüber hinaus haben Anthropolog:innen und viele andere
              Wissenschaftler:innen, aber auch Journalist:innen und
              Fotograf:innen Einzelpersonen und Gruppen von Menschen abgebildet,
              um so genannte “Typen” zu schaffen. Dazu gehörten auch
              Inszenierungen von Körpern und Körperteilen. Solche Bilder können
              mehrere Persönlichkeitsrechte verletzen.
            </p>
          </li>
          <li>
            <p>
              Darstellungen von Nacktheit: Einige Sammlungen enthalten Bilder
              von Frauen, deren Brüste nicht bedeckt sind, sowie von nackten
              Frauen und Männern aller Altersgruppen. Während viele dieser
              Frauen sich selbst als vollständig bekleidet betrachten würden,
              ist bekannt, dass in kolonialen Aufnahmesituationen Personen
              aufgefordert wurden, sich zu entkleiden und auf bestimmte Weise zu
              posieren. Die Umstände dieser fotografischen Ereignisse sind uns
              oft nicht bekannt. Wenngleich wir keine westlichen Vorstellungen
              von Nacktheit übernehmen wollen, sind wir uns bewusst, dass die
              fotografierten Personen möglicherweise verletzenden Blicken
              ausgesetzt sind, sobald die Bilder im Internet verfügbar sind, wo
              sie leicht dekontextualisiert werden können.
            </p>
            <p>
              Einige Institutionen haben beschlossen, keine Bilder zu zeigen,
              die offenkundig pornografische Inhalte widerspiegeln oder Menschen
              und Körper auf eindeutig voyeuristische und/oder diskriminierende
              Weise darstellen. Andere Bilder, die "Nacktheit" zeigen, werden
              jedoch online gestellt. Wir befinden uns in einem laufenden
              Diskussions- und Lernprozess zu diesem Thema.
            </p>
          </li>
          <li>
            <p>
              Darstellungen extremer physischer/militärischer Gewalt: Der
              koloniale und rassistische Kontext, in dem viele der Sammlungen
              entstanden sind, ist an sich schon eine sehr gewalttätige
              Situation. Einige Institutionen haben beschlossen, bestimmte
              Bilder, die extreme physische/militärische Gewalt darstellen, in
              ihren Online-Katalogen nicht anzuzeigen.
            </p>
          </li>
        </ul>
        <p>
          Alle Katalogeinträge und Metadaten der katalogisierten Bilder werden
          in PARC angezeigt. Aus den oben genannten ethischen Bedenken sowie aus
          urheberrechtlichen Gründen werden einige Bilder jedoch nicht oder nur
          in bearbeiteter Form online angezeigt. Diese Bilder sind auf Anfrage
          bei der jeweiligen Sammlungsinstitution erhältlich. Alle Eingriffe,
          wie z.B. die Bearbeitung einzelner Bilder oder Seiten in Alben, sind
          deutlich gekennzeichnet. Gerne können Sie uns kontaktieren und weitere
          Informationen anfordern.
        </p>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class EthicalGuidelinesDeComponent {
  constructor() {}
}
