import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import { ChangeDetectionStrategy, Component } from "@angular/core";

@Component({
  selector: "app-about-de",
  template: `
    <div class="static-page">
      <h1>Über PARC</h1>
      <div class="about-page__content">
        <div class="about-page__description">
          <p>
            PARC (Portal for African Research Collections) ist der neue
            Metakatalog für die Afrikana-Sammlungen am Forschungsstandort Basel.
            Erstmals sind die Sammlungen der fünf Basler Sammlungsinstitutionen
            Basler Afrika Bibliographien, Mission 21, Museum der Kulturen Basel,
            Swiss TPH und Universität Basel in einem einzigen Katalog
            durchsuchbar. Dies erleichtert den Zugang zu den Sammlungen
            unabhängig vom Standort.
          </p>

          <p>
            Neben bibliografischen und archivalischen Sammlungen sind auch
            ethnographische Objekte, Fotografien, Plakate, Filme/Videos,
            Tonträger sowie digitale Sammlungen (inkl. audiovisuelle Medien) zu
            finden.
          </p>
          <h2>Technisches</h2>

          <p>
            PARC ist als Innovationsprojekt zu verstehen. Wegen der
            unterschiedlichen Erschliessungstraditionen, sowohl in den
            Institutionen als auch bezüglich der Medientypen wurde auf ein
            gemeinsames Datenmodell verzichtet und stattdessen alternative Wege
            bei der Datenintegration beschritten.
          </p>

          <p>
            PARC ist offen für weitere, insbesondere afrikanische Partner.
            Deshalb orientiert sich PARC technisch an den Ansprüchen und
            Herausforderungen des Afrikanischen Marktes, damit der Zugriff
            gewährleistet ist und ein Wissensaustausch stattfinden kann. Dazu
            gehören die Optimierung für den Betrieb bei geringer Bandbreite
            sowie Werkzeuge für eine dezentrale Datenverwaltung, damit die
            Datengeber*innen die Ownership (das Eigentumsrecht) über ihre Daten
            behalten können. Die Daten werden nicht einem vordefinierten
            Datenmodell angepasst, sondern werden direkt übernommen.
          </p>

          <p>
            Bei Interesse wenden Sie sich an: Reto Ulrich, ru@baslerafrika.ch
            Wir prüfen gerne eine Teilnahme an PARC.
          </p>
          <h2>Projektgeschichte</h2>
          <table class="about-page__history">
            <tr>
              <td>2008</td>
              <td>
                Erstes technisch sehr einfaches Afrikaportal wurde auf der
                Homepage des ZASB durch die Universitätsbibliothek Basel
                eingerichtet. Die Bibliotheksbestände der fünf Basler
                Institutionen konnten mittels eines gemeinsamen Suchschlitzes
                durchsucht werden; die Ergebnisse aus den einzelnen Katalogen
                wurden separat aufgelistet.
              </td>
            </tr>
            <tr>
              <td>2016</td>
              <td>
                Die Universitätsbibliothek sucht Partner*innen für die technisch
                verbesserte Neuauflage des Afrikaportals. Die Universität Basel
                unter Federführung der Universitätsbibliothek, die BAB, die
                Mission 21, das MKB und das Swiss TPH einigen sich auf eine
                Kooperation mit dem Ziel, alle Sammlungen in einem Katalog
                zusammenzuführen.
              </td>
            </tr>
            <tr>
              <td>2018</td>
              <td>
                Interne Finanzierung des Projekts scheitert. Man beschliesst,
                Drittmittel zu suchen.
              </td>
            </tr>
            <tr>
              <td>2019</td>
              <td>Die Drittmittelsuche verläuft erfolgreich.</td>
            </tr>
            <tr>
              <td>2020</td>
              <td>
                Projektstart mit Fokus auf Basler Sammlungen und Verwendbarkeit
                für den afrikanischen Markt.
              </td>
            </tr>
            <tr>
              <td>2023</td>
              <td>Launch von PARC</td>
            </tr>
            <tr>
              <td>2024</td>
              <td>
                Öffnung des Portals für weitere, insbesondere afrikanische
                Institutionen und Sammlungen.
              </td>
            </tr>
          </table>
        </div>
        <app-about-page-links></app-about-page-links>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class AboutDeComponent {
  constructor() {}
}
