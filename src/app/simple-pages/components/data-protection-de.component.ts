import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import { ChangeDetectionStrategy, Component } from "@angular/core";

@Component({
  selector: "app-data-protection-de",
  template: `
    <div>
      <div class="static-page static-page--no-mood">
        <div class="sponsors-page__content">
          <h1>Datenschutz</h1>
          <h2>Präambel</h2>
          <p>
            Die Universität Basel legt grössten Wert auf den Schutz
            personenbezogener Daten. Personenbezogene Daten sind alle Daten, die
            auf Sie persönlich beziehbar sind, zB Name, Adresse, E-Mail,
            Telefonnummer, Foto, Nutzerverhalten, IP-Adresse, etc.
          </p>
          <p>
            Mit dieser Datenschutzerklärung möchten wir Sie darüber informieren,
            welche Daten die Universitätsbibliothek Basel von Ihnen als
            Nutzerinnen und Nutzern der Website www.parc-portal.org zu welchem
            Zweck erhebt, bearbeitet, bekannt gibt und welche Massnahmen die
            Universität Basel ergreift, um die Sicherheit dieser Daten zu
            gewährleisten. Zudem klären wir Sie mittels dieser
            Datenschutzerklärung über die Ihnen zustehenden Rechte auf.
          </p>
          <h2>
            Auf welcher gesetzlichen Grundlage bearbeitet die Universität Basel
            personenbezogene Daten?
          </h2>
          <p>
            Die Bearbeitung personenbezogener Daten erfolgt immer unter
            Beachtung des für die Universität Basel geltenden Informations- und
            Datenschutzgesetzes des Kantons Basel-Stadt (IDG) und allenfalls
            auch des Schweizerischen Bundesgesetzes über den Datenschutz (DSG).
            Darüber hinaus beachtet die Universität Basel die gegebenfalls
            anwendbare Europäische Datenschutzgrundverordnung (DSGVO) und gibt
            für diesen Fall, gemäss Art. 13 Abs. 1 lit c DSGVO, jeweils die
            entsprechende Gesetzesgrundlage an.
          </p>
          <h2>
            Welche personenbezogenen Daten bearbeitet die Universität Basel und
            zu welchem Zweck?
          </h2>
          <p>
            Bei der Nutzung von www.parc-portal.org werden keine
            personenbezogenen Daten erhoben oder bearbeitet.
          </p>
          <h2>Warum werden Cookies gesetzt?</h2>
          <p>
            Cookies sind kleine Textdateien, die von unserem Server oder dem
            Server Dritter an den Browser des Nutzers, der Nutzerin übertragen
            und auf Ihrem Endgerät gespeichert werden. Hierdurch erhalten wir
            automatisch bestimmte Daten wie z. B. die IP-Adresse, verwendete
            Browser, Betriebssystem und Ihre Verbindung zum Internet. Durch den
            Einsatz von Cookies erhöht sich die Benutzerfreundlichkeit und die
            Sicherheit der Website. Über die Einstellungen Ihres Browsers können
            Sie sehen, welche Cookies von der Website www.parc-portal.org
            gesetzt wurden und können diese gegebenenfalls löschen.
          </p>
          <h2>
            Welche Dienste von Dritten verwendet die Website
            www.parc-portal.org?
          </h2>
          <p>
            Wir binden Dienste von Dritten ein, achten dabei auf die Grundsätze
            des Datenschutzes (insbesondere Zweckbindung, Datenminimierung und
            nach Möglichkeit ohne Datentransfer in unsichere Drittländer).
          </p>
          <h3>Mapbox</h3>
          <p>
            Zweck: Darstellung von Datenpunkten auf einer Kartenoberfläche<br />
            URL: https://www.mapbox.com <br />
            Datenschutzerklärung: https://www.mapbox.com/legal/privacy
          </p>
          <h2>Ist die Website www.parc-portal.org verschlüsselt?</h2>
          <p>
            Unsere Website www.parc-portal.org wird verschlüsselt angeboten.
          </p>
          <p>
            Die Verschlüsselung stellt sicher, dass die aufgerufene Website (die
            angeforderten Informationen) auch von dem Server kommt, der er
            vorgibt, zu sein. Unverschlüsselter Datenverkehr kann von Dritten
            manipuliert werden.
          </p>
          <h2>Wer haftet für Links zu Websites anderer Anbieter?</h2>
          <p>
            Die Universität Basel bzw. Universitätsbibliothek Basel hat
            Websites, die auf der Website www.parc-portal.org über Hyperlinks
            verbunden sind sowie angezeigte RSS-Feeds nicht überprüft und
            übernimmt keine Haftung für deren Inhalt sowie für die darauf
            angebotenen Produkte, Dienstleistungen und sonstigen Angebote.
          </p>
          <h2>Kann sich die Datenschutzerklärung ändern?</h2>
          <p>
            Die Universität Basel bzw. Universitätsbibliothek Basel behält sich
            vor, diese Datenschutzerklärung jederzeit bei Bedarf anzupassen.
            Aktueller Stand der vorliegenden Datenschutzerklärung ist der
            24.08.2023.
          </p>
          <h2>
            Wer ist für die Website www.parc-portal.org verantwortlich und an
            wen können Sie sich bei Bedarf wenden?
          </h2>
          <p>
            Verantwortlich:<br />Universität Basel<br />Universitätsbibliothek<br />
            Schönbeinstrasse 18-20<br />
            4056 Basel
          </p>
          <p>
            Oberverantwortung:<br />Universität Basel Petersplatz 1<br />Postfach
            4001<br />
            Basel Schweiz<br />https://www.unibas.ch
          </p>
          <p>
            Datenschutzbeauftragte der Universität Basel:<br />Danielle Kaufmann
            lic. iur.<br />Petersgraben 35<br />4001 Basel<br />datenschutz@unibas.ch
          </p>
        </div>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class DataProtectionDeComponent {
  constructor() {}
}
