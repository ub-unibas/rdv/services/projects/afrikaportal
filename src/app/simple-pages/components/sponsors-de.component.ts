import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import { ChangeDetectionStrategy, Component } from "@angular/core";

@Component({
  selector: "app-sponsors-de",
  template: `
    <div>
      <div class="static-page static-page--no-mood">
        <div class="sponsors-page__content">
          <h1>Sponsoren</h1>
          <p>
            PARC ist ein Innovationsprojekt, sowohl was das Technische anbelangt
            wie auch die Ausrichtung auf den Afrikanischen Markt. Nach mehreren
            Anläufen und Workshops, teilweise auch mit Kolleg*innen auf dem
            afrikanischen Kontinent, konnte das Projekt 2021 starten. Nach rund
            zweieinhalb Jahren ist die Suchplattform nun live. Dies wurde nur
            möglich, weil diverse Stiftungen dieses Unterfangen finanziell
            unterstützten. Ihnen möchten wir an dieser Stelle danken.
          </p>
          <p>Unser Dank geht an:</p>
          <p>
            Ernst Göhner Stiftung, Carl Schlettwein Stiftung, UBS
            Kulturstiftung, Fondation Oumou Dilly, Freiwillige Akademische
            Gesellschaft
          </p>
        </div>
        <app-sponsors></app-sponsors>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class SponsorsDeComponent {
  constructor() {}
}
