import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import { ChangeDetectionStrategy, Component, Input } from "@angular/core";
import { Exhibition } from "@app/exhibitions/actions/exhibitions.actions";

@Component({
  selector: "app-exhibitions-de",
  template: `
    <div>
      <h1>Ausstellungen</h1>
      <p class="exhibition-page__description">
        Entdecken Sie Ausstellungsprojekte und digitale Präsentationen aus den
        in PARC vertretenen Institutionen und Sammlungen.
      </p>
      <app-exhibition-preview
        [exhibitions]="exhibitions"
        [startIndex]="0"
        [count]="300"
        [showAllButton]="false"
      ></app-exhibition-preview>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class ExhibitionDeComponent {
  @Input() exhibitions: Exhibition[];

  constructor() {}
}
