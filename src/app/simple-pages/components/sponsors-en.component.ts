import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import { ChangeDetectionStrategy, Component } from "@angular/core";

@Component({
  selector: "app-sponsors-en",
  template: `
    <div>
      <div class="static-page static-page--no-mood">
        <div class="sponsors-page__content">
          <h1>Sponsors</h1>
          <p>
            PARC is an innovative project, both in terms of technology and its
            focus on the African market. After several attempts and workshops,
            some with colleagues on the African continent, the project was
            launched in 2021. After about two and a half years, the search
            platform is now live. This was only possible because various
            foundations supported the project financially. We would like to take
            this opportunity to thank them.
          </p>
          <p>Our thanks go to:</p>
          <p>
            Ernst Göhner Foundation, Carl Schlettwein Foundation, UBS Culture
            Foundation, Fondation Oumou Dilly, Freiwillige Akademische
            Gesellschaft
          </p>
        </div>
        <app-sponsors></app-sponsors>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class SponsorsEnComponent {
  constructor() {}
}
