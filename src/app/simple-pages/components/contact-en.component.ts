import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import { ChangeDetectionStrategy, Component } from "@angular/core";

@Component({
  selector: "app-contact-en",
  template: `
    <div class="static-page static-page--no-mood">
      <div class="contact-page__content">
        <h1>Contact</h1>
        <p>
          PARC, c/o Universitätsbibliothek Basel<br />
          Schönbeinstrasse 18-20<br />
          CH-4056 Basel<br />
        </p>

        <p>
          Direct phone number:
          <a target="_blank" href="tel:+41612289331">+41 (0)61 228 93 31</a
          ><br />
          E-Mail:
          <a target="_blank" href="mailto:ru@baslerafrika.ch"
            >ru@baslerafrika.ch</a
          ><br />
        </p>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class ContactEnComponent {
  constructor() {}
}
