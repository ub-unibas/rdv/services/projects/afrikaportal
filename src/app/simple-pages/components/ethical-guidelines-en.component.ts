import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import { ChangeDetectionStrategy, Component } from "@angular/core";

@Component({
  selector: "app-ethical-guidelines-en",
  template: `
    <div class="static-page static-page--no-mood">
      <div class="ethical-guidelines-page__content">
        <h1>Ethical Guidelines</h1>
        <p>
          African Research Collections were often created in colonial and racist
          circumstances and are deemed offensive and discriminatory regarding
          their contents and metadata. They raise a range of ethical and related
          issues, including ownership and copyright issues.
        </p>
        <p>
          The online display of these collections can cause the reproduction of
          colonial and racist positions which we do not share. We are committed
          to anti-racist and decolonizing agendas.
        </p>
        <p>
          As part of these agendas, we consider making collections accessible
          which are of importance to the region’s populations, history and
          culture, and scholarship to be an essential task for libraries,
          archives and museums.
        </p>
        <p>
          We are committed to addressing offensive, discriminatory and
          inaccurate content – visual or language-wise – in our databases and
          continually update our catalogs. To improve our ethical approach,
          content work and accessibility, we appreciate your feedback. Kindly
          email your comments to the respective holding institutions.
        </p>
        <h2>Notes regarding language</h2>
        <p>
          Collections, records and catalog entries may contain colonial and
          racist vocabulary, which is inaccurate, derogatory and harmful.
        </p>
        <p>
          On the one hand catalog entries, such as titles of books, manuscripts,
          sound recordings and audiovisual media as well as captions and
          comments may contain offensive vocabulary. On the other hand,
          descriptive keywords and subject headings provided by the collecting
          institutions may in some cases reproduce colonial categorizations.
        </p>
        <p>
          We work on improving our catalogs and establishing responsible,
          ethical and potentially decolonial ways to deal with the colonial
          legacies present in our databases. This is an ongoing process and we
          appreciate your inputs on the subject.
        </p>
        <h2>Ethical concerns regarding visual media</h2>
        <p>
          The portal aims to make collections of photographs, films,
          illustrations and other visual media housed currently in European
          institutions accessible to a wider public, especially in African
          countries. However, digitisation and online display of images from
          colonial contexts come with multiple ethical challenges.
        </p>
        <p>
          Will online display of colonial visual representations contribute to
          the reproduction of colonial and racist positions?
        </p>
        <p>
          Should images taken without a person's consent be recirculated on the
          internet or might this present a new form of exposure to colonial
          violence?
        </p>
        <p>
          How to address the lack of contextual information in many visual
          collections?
        </p>
        <p>
          How can we navigate our responsibilities to make collections
          accessible while considering questions of ethics and care in curating
          online catalogs?
        </p>
        <p>
          Photography and other visual media contributed in many ways to
          colonialism and is historically entangled with the exercise of
          colonial power. With regard to photography, we point, amongst others,
          to the following “genres” present in our collections:
        </p>
        <ul class="ethical-guidelines-page__arrow-list">
          <li>
            <p>
              Images without a person’s name: Throughout the colonial period,
              and beyond, photographers usually did not add the full and/or
              correct name of individuals depicted in the image. We welcome any
              additional information to recover the unrecorded names and
              identify persons.
            </p>
          </li>
          <li>
            <p>
              Images depicting poverty and extreme class relations: The colonial
              and racist context in which the collections were created, produced
              extreme class relations and a genre of images relating to poverty.
              Many images implicitly or explicitly reflect or underline colonial
              and structurally violent relations.
            </p>
          </li>
          <li>
            <p>
              Images depicting “secret” events or objects: There might be images
              within the collections showing events, locations or objects which
              according to the ethical and cultural codes of the source
              communities should be viewed only by certain members of society.
            </p>
            <p>
              We try to the best of our knowledge not to display such images
              online. In case you encounter such images in our catalogs please
              contact us.
            </p>
          </li>
          <li>
            <p>
              Physical type/anthropometrical images: Throughout the colonial
              period, and beyond, anthropologists and many other scholars but
              also journalists and photographers in general depicted individuals
              and groups of people to “create” so-called physical types. This
              included stagings of (parts of) the body. Such images might
              violate several personal rights.
            </p>
          </li>
          <li>
            <p>
              Images of nudity: Some collections include pictures of women of
              all ages whose breasts are not covered, or of women and men of all
              ages who are depicted as nude. While many of these women would
              consider themselves fully clothed, it is known that colonial
              photographic scenes led to situations where persons were asked to
              remove clothes and pose in particular ways. We often lack
              information on the circumstances of these photographic events.
              While we do not want to impose Western notions of nudity, we are
              aware of the potential exposure of the subjects of the photographs
              to harmful gazes once the images are available on the internet
              where they can easily be decontextualized.
            </p>
            <p>
              Some institutions decided not to show images which reflect
              outright pornographic content or images which zoom-in on people
              and bodies in clearly voyeuristic and/or discriminatory ways.
              However, other images depicting “nudity” are included online and
              we engage in an ongoing discussion and learning process on this
              issue.
            </p>
          </li>
          <li>
            <p>
              Images depicting extreme physical/military violence: The colonial
              and racist context which produced many of the collections is a
              very violent situation per se. Some institutions decided not to
              display particular images depicting extreme physical/military
              violence in their online catalogs.
            </p>
          </li>
        </ul>
        <p>
          The PARC online catalog shows all entries related to our cataloged
          images. However, for the ethical concerns mentioned above, as well as
          for copyright issues, some images are not displayed online or only in
          edited form. These images are available on request from the respective
          holding institution. All interferences such as editing of individual
          images or pages in albums are clearly marked and you are welcome to
          communicate with us and request more information.
        </p>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class EthicalGuidelinesEnComponent {
  constructor() {}
}
