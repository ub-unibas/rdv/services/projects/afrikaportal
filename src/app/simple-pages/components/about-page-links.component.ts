import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import { ChangeDetectionStrategy, Component } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-about-page-links",
  template: `
    <div class="about-page__links">
      <a target="_blank" href="https://www.baslerafrika.ch/?lang={{ lang }}">
        Basler Afrika Bibliographien</a
      >
      <a target="_blank" href="https://www.bmarchives.org/">Mission 21</a>
      <a target="_blank" href="https://www.mkb.ch/{{ lang }}.html">
        Museum der Kulturen Basel</a
      >
      <a target="_blank" href="https://www.swisstph.ch/{{ lang }}/">
        Swiss TPH</a
      >
      <a
        target="_blank"
        href="https://basel.swisscovery.org/discovery/search?vid=41SLSP_UBS:live&lang={{
          lang
        }}"
      >
        {{ "about-page.ub-basel" | translate }}</a
      >
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class AboutPageLinksComponent {
  constructor(protected translate: TranslateService) {}

  get lang(): string {
    return this.translate.currentLang;
  }
}
