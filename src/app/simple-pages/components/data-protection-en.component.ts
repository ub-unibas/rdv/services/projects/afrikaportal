import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import { ChangeDetectionStrategy, Component } from "@angular/core";

@Component({
  selector: "app-data-protection-en",
  template: `
    <div>
      <div class="static-page static-page--no-mood">
        <div class="sponsors-page__content">
          <h1>Privacy policy</h1>
          <h2>Preamble</h2>
          <p>
            The University of Basel attaches great importance to the protection
            of personal data. Personal data is all data that can be related to
            you personally, e.g. name, address, e-mail, telephone number, photo,
            user behavior, IP address, etc.
          </p>
          <p>
            With this privacy policy, we would like to inform you which data
            Basel University Library collects from you as a user of the website
            www.parc-portal.org, processes, discloses, and for what purpose, and
            which measures the University of Basel takes to ensure the security
            of this data. In addition, we inform you about your rights by means
            of this privacy policy.
          </p>
          <h2>
            On what legal basis does the University of Basel process personal
            data?
          </h2>
          <p>
            Personal data is always processed in compliance with the Information
            and Data Protection Act of the Canton of Basel-Stadt (IDG)
            applicable to the University of Basel and, if applicable, the Swiss
            Federal Data Protection Act (DSG). In addition, the University of
            Basel complies with the European Data Protection Regulation (DSGVO),
            which may be applicable, and states the relevant legal basis for
            this case in each case, in accordance with Art. 13 Para. 1 lit c
            DSGVO.
          </p>
          <h2>
            What personal data does the University of Basel process and for what
            purpose?
          </h2>
          <p>
            When using www.parc-portal.org, no personal data is collected or
            processed.
          </p>
          <h2>Why are cookies set?</h2>
          <p>
            Cookies are small text files that are transferred from our server or
            the server of a third party to the user's browser and stored on your
            terminal device. Through this, we automatically receive certain data
            such as the IP address, browser used, operating system and your
            connection to the Internet. The use of cookies increases the
            user-friendliness and security of the website. Via the settings of
            your browser, you can see which cookies have been set by the website
            www.parc-portal.org and can delete them if necessary.
          </p>
          <h2>
            What third party services does the www.parc-portal.org website use?
          </h2>
          <p>
            We integrate services of third parties, paying attention to the
            principles of data protection (especially purpose limitation, data
            minimization and, if possible, without data transfer to unsafe third
            countries).
          </p>
          <h3>Mapbox</h3>
          <p>
            Purpose: Display of data on a map surface<br />
            URL: https://www.mapbox.com <br />
            Privacy policy: https://www.mapbox.com/legal/privacy
          </p>
          <h2>Is the www.parc-portal.org website encrypted?</h2>
          <p>Our website www.parc-portal.org is offered encrypted.</p>
          <p>
            Encryption ensures that the website accessed (the information
            requested) also comes from the server it claims to be. Unencrypted
            traffic can be manipulated by third parties.
          </p>
          <h2>Who is liable for links to websites of other providers?</h2>
          <p>
            The University of Basel or Basel University Library has not reviewed
            websites linked to the website www.parc-portal.org via hyperlinks as
            well as displayed RSS feeds and assumes no liability for their
            content or for the products, services and other offers provided
            thereon.
          </p>
          <h2>Can the privacy policy change?</h2>
          <p>
            The University of Basel or Basel University Library reserves the
            right to modify this Privacy Policy at any time if necessary. The
            current status of this privacy policy is 24.08.2023.
          </p>
          <h2>
            Who is responsible for the www.parc-portal.org website and who can
            you contact if you need help?
          </h2>
          <p>
            Responsible:<br />University of Basel<br />Basel University
            Library<br />
            Schönbeinstrasse 18-20<br />
            4056 Basel
          </p>
          <p>
            Main Responsibility:<br />University of Basel Petersplatz 1<br />P.O.
            Box 4001<br />
            Basel Switzerland<br />https://www.unibas.ch
          </p>
          <p>
            Data Protection Officer of the University of Basel:<br />Danielle
            Kaufmann lic. iur.<br />Petersgraben 35<br />4001 Basel<br />datenschutz@unibas.ch
          </p>
        </div>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class DataProtectionEnComponent {
  constructor() {}
}
