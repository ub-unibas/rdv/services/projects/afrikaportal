import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import { ChangeDetectionStrategy, Component } from "@angular/core";

@Component({
  selector: "app-help-de",
  template: `
    <div class="static-page static-page--no-mood">
      <div class="help-page__content">
        <h1>Hilfe</h1>
        <h2>Fragen und Antworten zur Kartensuche:</h2>
        <ul class="help-page__arrow-list">
          <li>
            <b>Welche Einträge werden auf der Karte dargestellt?</b><br />Damit
            ein Eintrag/Medium auf der Karte dargestellt werden kann, benötigen
            wir einen geographischen Ort innerhalb seiner Metadaten. Ist dies
            nicht der Fall, beziehungsweise es existiert lediglich eine
            Georeferenz auf ein Land, kann der Eintrag nicht als Punkt auf der
            Karte dargestellt werden oder wird unter einem Fahnen-Icon mit
            anderen Einträgen dieses Landes zusammengefasst (siehe unten).
          </li>
          <li>
            <b>Was verbirgt sich hinter den Fahnen auf der Karte?</b><br />Die
            Fahnen repräsentieren Einträge, welche lediglich ein Land als
            geographische Information enthalten. Ein Klick auf die Fahne führt
            zu einer Suchanfrage mit ebendiesem Land als Filter.
          </li>
          <li>
            <b>Wie verhält sich die Zeit-Filter Funktion?</b><br />Beim Start
            der Kartensuche ist der Zeitfilter inaktiv. Wird ein Zeitfilter
            gewünscht, muss er manuell über Betätigung der Zeitleiste, der
            Eingabefelder oder der Zeit-Facette gesetzt werden.
          </li>
          <li>
            <b
              >Weshalb habe ich bei der Kartensuche ständig einen
              „Koordinaten:…“-Filter?</b
            ><br />Jegliches Navigieren auf der Karte führt stets zu einer neuen
            Suche. Dies ermöglicht es, dass die Suchresultate sich „Live“
            ändern, wenn man sich auf der Karte bewegt. Der Filter dieser Suche
            wird durch die entsprechenden „Koordinaten“ definiert.
          </li>
          <li>
            <b>Sehe ich auf der Karte jeweils alle Resultate?</b><br />Auf jedem
            Kartenausschnitt können jeweils nur maximal die grössten 300 Cluster
            bzw. Punkte angezeigt werden.
          </li>
        </ul>
        <h2>Fragen und Antworten zu PARC und den Sammlungen:</h2>
        <ul class="help-page__arrow-list">
          <li>
            <b
              >Ich habe eine Frage zu den Sammlungen. An wen kann ich mich
              wenden?</b
            ><br />Bei Fragen und Anregungen wenden Sie sich bitte jeweils an
            die datengebenden Institutionen.
          </li>
          <li>
            <b
              >Ich habe Fragen oder Anregungen zu PARC. An wen kann ich mich
              wenden?</b
            ><br />Melden Sie sich bei uns. Sie finden unsere Kontakt Daten im
            Footer unter
            <a
              class="link-black"
              [title]="'footer.link_contact' | translate"
              [routerLink]="['/simple/contact' | localize]"
            >
              „Kontakt“</a
            >.
          </li>
        </ul>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class HelpDeComponent {
  constructor() {}
}
