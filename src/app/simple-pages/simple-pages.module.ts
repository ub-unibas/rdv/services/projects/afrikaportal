import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "@app/shared/shared.module";
import { LocalizeRouterModule } from "@gilsdav/ngx-translate-router";
import { TranslateModule } from "@ngx-translate/core";
import { PipesModule } from "@ub-unibas/rdv_viewer-lib/components";
import { DynamicModule } from "ng-dynamic-component";
import { AboutDeComponent } from "./components/about-de.component";
import { AboutEnComponent } from "./components/about-en.component";
import { AboutPageLinksComponent } from "./components/about-page-links.component";
import { ContactDeComponent } from "./components/contact-de.component";
import { ContactEnComponent } from "./components/contact-en.component";
import { DataProtectionDeComponent } from "./components/data-protection-de.component";
import { DataProtectionEnComponent } from "./components/data-protection-en.component";
import { EthicalGuidelinesDeComponent } from "./components/ethical-guidelines-de.component";
import { EthicalGuidelinesEnComponent } from "./components/ethical-guidelines-en.component";
import { ExhibitionDeComponent } from "./components/exhibition-de.component";
import { ExhibitionEnComponent } from "./components/exhibition-en.component";
import { HelpDeComponent } from "./components/help-de.component";
import { HelpEnComponent } from "./components/help-en.component";
import { ImprintDeComponent } from "./components/imprint-de.component";
import { ImprintEnComponent } from "./components/imprint-en.component";
import { SponsorsDeComponent } from "./components/sponsors-de.component";
import { SponsorsEnComponent } from "./components/sponsors-en.component";
import { AboutPageComponent } from "./pages/about-page.component";
import { ContactPageComponent } from "./pages/contact-page.component";
import { DataProtectionPageComponent } from "./pages/data-protection-page.component";
import { EthicalGuidelinesPageComponent } from "./pages/ethical-guidelines-page.component";
import { ExhibitionPageComponent } from "./pages/exhibition-page.component";
import { HelpPageComponent } from "./pages/help-page.component";
import { ImprintPageComponent } from "./pages/imprint-page.component";
import { SponsorsPageComponent } from "./pages/sponsors-page.component";

const routes: Routes = [
  { path: "about", component: AboutPageComponent },
  { path: "sponsors", component: SponsorsPageComponent },
  { path: "ethical-guidelines", component: EthicalGuidelinesPageComponent },
  { path: "exhibition", component: ExhibitionPageComponent },
  { path: "privacy-policy", component: DataProtectionPageComponent },
  { path: "imprint", component: ImprintPageComponent },
  { path: "contact", component: ContactPageComponent },
  { path: "help", component: HelpPageComponent },
];

const COMPONENTS = [
  AboutPageComponent,
  AboutDeComponent,
  AboutEnComponent,
  AboutPageLinksComponent,
  SponsorsPageComponent,
  SponsorsDeComponent,
  SponsorsEnComponent,
  EthicalGuidelinesPageComponent,
  EthicalGuidelinesDeComponent,
  EthicalGuidelinesEnComponent,
  ExhibitionPageComponent,
  ExhibitionDeComponent,
  ExhibitionEnComponent,
  DataProtectionPageComponent,
  DataProtectionDeComponent,
  DataProtectionEnComponent,
  ImprintPageComponent,
  ImprintDeComponent,
  ImprintEnComponent,
  ContactPageComponent,
  ContactDeComponent,
  ContactEnComponent,
  HelpDeComponent,
  HelpEnComponent,
  HelpPageComponent,
];

@NgModule({
  imports: [
    CommonModule,
    PipesModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild(),
    LocalizeRouterModule.forChild(routes),
    SharedModule,
    DynamicModule,
  ],
  declarations: COMPONENTS,
  providers: [],
  exports: COMPONENTS,
})
export class SimplePagesModule {}
