import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import { ChangeDetectionStrategy, Component } from "@angular/core";
import { EthicalGuidelinesDeComponent } from "../components/ethical-guidelines-de.component";
import { EthicalGuidelinesEnComponent } from "../components/ethical-guidelines-en.component";

@Component({
  selector: "app-ethical-guidelines-page",
  template: `
    <app-simple-page
      [deComponent]="deComponent"
      [enComponent]="enComponent"
      baseClass="ethical-guidelines-page"
    ></app-simple-page>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class EthicalGuidelinesPageComponent {
  deComponent = EthicalGuidelinesDeComponent;
  enComponent = EthicalGuidelinesEnComponent;
}
