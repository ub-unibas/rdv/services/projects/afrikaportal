import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import { ChangeDetectionStrategy, Component } from "@angular/core";
import { MoodInfo } from "@app/shared/components/mood.component";
import { ContactDeComponent } from "../components/contact-de.component";
import { ContactEnComponent } from "../components/contact-en.component";

@Component({
  selector: "app-contact-page",
  template: `
    <app-simple-page
      [deComponent]="deComponent"
      [enComponent]="enComponent"
      baseClass="sponsors-page"
    ></app-simple-page>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class ContactPageComponent {
  deComponent = ContactDeComponent;
  enComponent = ContactEnComponent;
}
