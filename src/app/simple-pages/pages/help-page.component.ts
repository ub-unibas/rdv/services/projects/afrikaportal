import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import { ChangeDetectionStrategy, Component } from "@angular/core";
import { HelpDeComponent } from "../components/help-de.component";
import { HelpEnComponent } from "../components/help-en.component";

@Component({
  selector: "app-contact-page",
  template: `
    <app-simple-page
      [deComponent]="deComponent"
      [enComponent]="enComponent"
      baseClass="help-page"
    ></app-simple-page>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class HelpPageComponent {
  deComponent = HelpDeComponent;
  enComponent = HelpEnComponent;
}
