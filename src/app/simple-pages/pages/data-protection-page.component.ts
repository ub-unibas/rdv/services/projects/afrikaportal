import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import { ChangeDetectionStrategy, Component } from "@angular/core";
import { DataProtectionDeComponent } from "../components/data-protection-de.component";
import { DataProtectionEnComponent } from "../components/data-protection-en.component";

@Component({
  selector: "app-data-protection-page",
  template: `
    <app-simple-page
      [deComponent]="deComponent"
      [enComponent]="enComponent"
      baseClass="data-protection-page"
    ></app-simple-page>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class DataProtectionPageComponent {
  deComponent = DataProtectionDeComponent;
  enComponent = DataProtectionEnComponent;
}
