import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import { ChangeDetectionStrategy, Component } from "@angular/core";
import { getExhibitionsState } from "@app/exhibitions/reducers";
import { State as ExhibitionState } from "@app/exhibitions/reducers/exhibitions.reducer";
import { ExhibitionData } from "@app/shared/components/ExhibitionData";
import { MoodInfo } from "@app/shared/components/mood.component";
import { ExhibitionService } from "@app/shared/services/ExhibitionService";
import { Store, select } from "@ngrx/store";
import { SearchStateInterface } from "@ub-unibas/rdv_viewer-lib/components";
import { Observable } from "rxjs";
import { ExhibitionDeComponent } from "../components/exhibition-de.component";
import { ExhibitionEnComponent } from "../components/exhibition-en.component";

const moods: MoodInfo[] = [
  {
    forWidth: 390,
    imgUrl:
      "/assets/img/moods/ausstellung390.jpg",
  },
  {
    forWidth: 768,
    imgUrl:
      "/assets/img/moods/ausstellung768.jpg",
  },
  {
    forWidth: 1024,
    imgUrl:
      "/assets/img/moods/ausstellung1024.jpg",
  },
  {
    forWidth: 1280,
    imgUrl:
      "/assets/img/moods/ausstellung1280.jpg",
  },
  {
    forWidth: 1281,
    imgUrl: "/assets/img/moods/ausstellung1680.jpg",
  },
];

@Component({
  selector: "app-exhibitions-page",
  template: `
    <app-simple-page
      [deComponent]="deComponent"
      [enComponent]="enComponent"
      [componentInputs]="getLocalizedExhibitions(exhibitions$ | async)"
      [moodInfo]="moodInfo"
      baseClass="exhibition-page"
    ></app-simple-page>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class ExhibitionPageComponent {
  exhibitions$: Observable<ExhibitionState>;

  deComponent = ExhibitionDeComponent;
  enComponent = ExhibitionEnComponent;
  moodInfo = moods;

  constructor(
    protected searchStore: Store<SearchStateInterface>,
    protected exhibitionService: ExhibitionService
  ) {
    this.exhibitions$ = this.searchStore.pipe(select(getExhibitionsState));
  }

  getLocalizedExhibitions(exhibitions: ExhibitionState): ExhibitionData {
    return this.exhibitionService.getLocalizedExhibitions(exhibitions);
  }
}
