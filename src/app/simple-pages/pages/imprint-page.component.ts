import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import { ChangeDetectionStrategy, Component } from "@angular/core";
import { ImprintDeComponent } from "../components/imprint-de.component";
import { ImprintEnComponent } from "../components/imprint-en.component";

@Component({
  selector: "app-imprint-page",
  template: `
    <app-simple-page
      [deComponent]="deComponent"
      [enComponent]="enComponent"
      baseClass="sponsors-page"
    ></app-simple-page>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class ImprintPageComponent {
  deComponent = ImprintDeComponent;
  enComponent = ImprintEnComponent;
}
