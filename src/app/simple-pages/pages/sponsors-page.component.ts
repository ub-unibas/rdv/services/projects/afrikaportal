import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import { ChangeDetectionStrategy, Component } from "@angular/core";
import { SponsorsDeComponent } from "../components/sponsors-de.component";
import { SponsorsEnComponent } from "../components/sponsors-en.component";

@Component({
  selector: "app-sponsors-page",
  template: `
    <app-simple-page
      [deComponent]="deComponent"
      [enComponent]="enComponent"
      baseClass="sponsors-page"
    ></app-simple-page>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class SponsorsPageComponent {
  deComponent = SponsorsDeComponent;
  enComponent = SponsorsEnComponent;
}
