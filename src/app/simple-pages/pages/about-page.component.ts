import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import { ChangeDetectionStrategy, Component } from "@angular/core";
import { AboutDeComponent } from "../components/about-de.component";
import { AboutEnComponent } from "../components/about-en.component";
import { MoodInfo } from "@app/shared/components/mood.component";

const moods: MoodInfo[] = [
  {
    forWidth: 390,
    imgUrl: "/assets/img/moods/about390.jpg",
  },
  {
    forWidth: 768,
    imgUrl: "/assets/img/moods/about768.jpg",
  },
  {
    forWidth: 1024,
    imgUrl: "/assets/img/moods/about1024.jpg",
  },
  {
    forWidth: 1280,
    imgUrl: "/assets/img/moods/about1280.jpg",
  },
  {
    forWidth: 1281,
    imgUrl: "/assets/img/moods/about1680.jpg",
  },
];

@Component({
  selector: "app-about-page",
  template: `
    <app-simple-page
      [deComponent]="deComponent"
      [enComponent]="enComponent"
      [moodInfo]="moodInfo"
      baseClass="about-page"
    ></app-simple-page>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class AboutPageComponent {
  deComponent = AboutDeComponent;
  enComponent = AboutEnComponent;
  moodInfo = moods;
}
