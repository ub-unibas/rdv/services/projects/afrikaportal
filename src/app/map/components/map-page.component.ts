import { BreakpointObserver } from "@angular/cdk/layout";
import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  OnDestroy,
  ViewChild,
} from "@angular/core";
import { Router } from "@angular/router";
import { Country } from "@app/countries/actions/countries.actions";
import { getCountriesState } from "@app/countries/reducers";
import * as fromCountries from "@app/countries/reducers/countries.reducer";
import { FacetDisplayHelper } from "@app/shared/components/FacetDisplayHelper";
import {
  RangeSliderComponent,
  ThumbData,
  Tick,
} from "@app/shared/components/range-slider.component";
import { environment } from "@env/environment";
import { LocalizeRouterService } from "@gilsdav/ngx-translate-router";
import { Store, select } from "@ngrx/store";
import { LangChangeEvent, TranslateService } from "@ngx-translate/core";
import {
  AbstractHistogramFacetComponentDirective,
  AddFacetValue,
  AddHistogramBoundaries,
  Doc,
  GeoResult,
  I18nToastrService,
  RemoveAllFacetValue,
  RemoveAllHistogramBoundary,
  SearchStateInterface,
  SetGeoClusterPrecision,
  SetOffset,
  SettingsModel,
  SimpleSearch,
  buildId,
  getAllGeoResults,
  getFacetHistogramCount,
  getGeoClusterPrecision,
  getHistogramValues,
} from "@ub-unibas/rdv_viewer-lib/components";
import { MapBoxComponent, MapLimits } from "@ub-unibas/rdv_viewer-lib/mapboxgl";
import {
  MapboxEventHandler,
  MapboxImages,
} from "@ub-unibas/rdv_viewer-lib/mapboxgl/src/components/mapbox.component";
import {
  GeoBucket,
  GeoLocation,
  GeoPoint,
  MapConfig,
  RenderHint,
  SetCenter,
  SetZoom,
  ZoomToPrecision,
  geoFacetToGeoJson,
  geoSnippetsToGeoJson,
  getMapCenter,
  getMapZoom,
  precisionForZoom,
} from "@ub-unibas/rdv_viewer-lib/tool";
import { Geometry, Point } from "geojson";
import GeoHash from "latlon-geohash";
import * as mapboxgl from "mapbox-gl";
import { BehaviorSubject, Observable, Subscription, filter, take } from "rxjs";

// IMPORTANT: middleware doesn't support values below year 100!
// even not negative years!
const defaultTimeSliderTicks: Tick[] = [
  { label: "", value: -25, hideTick: true },
  { label: "100", value: 100 },
  { label: "500", value: 500 },
  { label: "1000", value: 1000 },
  { label: "1500", value: 1500 },
  { label: "2000", value: 2000 },
  { label: "", value: 2200, hideTick: true },
];

const MIN_YEAR = 100;

const images: MapboxImages[] = [
  { url: "/assets/img/map-land.png", id: "country" },
  { url: "/assets/img/map-geo-marker.png", id: "point" },
  { url: "/assets/img/red-cross.png", id: "flag" },
];

@Component({
  selector: "app-map-page",
  template: `
    <div class="page-map">
      <app-mapbox
        #mapBox
        [mapConfig]="initialArea()"
        (mapChanged)="updateMap($event)"
        [images]="mapboxImages()"
        [layers]="mapboxLayers()"
        [eventHandlers]="mapboxEventHandlers()"
      ></app-mapbox>
      <div class="page-map__time-slider">
        <app-range-slider
          #timeSlider
          [ticks]="timeSliderTicks"
          (leftThumb)="setYear($event, startYear$)"
          (rightThumb)="setYear($event, endYear$)"
          (doSearch)="switchToSearch(true)"
          [initialThumbs]="initYearSlider()"
        ></app-range-slider>
        <div class="page-map__time-inputs">
          <span class="page-map__time-spacer-right">{{
            "min-max-date-input.from" | translate
          }}</span>
          <input
            class="page-map__time-input page-map__time-spacer-right"
            #startYear
            type="number"
            [min]="defaultStartYear"
            value="{{ startYear$ | async }}"
            (keyup)="closedInput($event)"
          />
          <span class="page-map__time-spacer-right">{{
            "min-max-date-input.to" | translate
          }}</span>
          <input
            class="page-map__time-input page-map__time-spacer-right"
            #endYear
            type="number"
            [min]="defaultStartYear"
            value="{{ endYear$ | async }}"
            (keyup)="closedInput($event)"
          />
          <button
            class="page-map__time-submit"
            (click)="selectTimeRange(startYear.value, endYear.value)"
          >
            {{ "map-page.OK" | translate }}
          </button>
        </div>
      </div>
      <app-logo imgClass="page-map__logo"></app-logo>
      <app-main-navi class="page-map__navi"></app-main-navi>
      <app-search-filter
        class="search-filter"
        [initiallyClosed]="true"
        [facetDisplayHelper]="facetDisplayHelper"
        [forceToggled]="true"
      ></app-search-filter>
      <div *ngIf="showPreview$ | async" class="page-map__preview">
        <button
          class="page-map__preview-close"
          (click)="closePreview()"
          title="{{ 'map-page.Close' | translate }}"
        >
          {{ "map-page.Close" | translate }}
        </button>
        <app-parc-search-hit
          [doc]="showPreview$ | async"
          [last]="true"
        ></app-parc-search-hit>
      </div>
      <div class="page-map__map-controls" *ngIf="debugCoords()">
        <button (click)="toggleMapControls()" class="page-map__control-toggler">
          &gt;&gt;
        </button>
        <div
          *ngIf="mapControls$ | async"
          class="page-map__map-controls-content"
        >
          To lat: <input #lat type="text" /> lng: <input #lng type="text" />
          <button (click)="fly()">Fly</button>
          <br />
          To geohash: <input #geohash type="text" />
          <button (click)="flyGeoHash()">Fly</button>
          <select #precision (change)="setPrecision(precision.value)">
            <option value="auto">Precision (auto)</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
            <option value="7">7</option>
            <option value="8">8</option>
            <option value="9">9</option>
            <option value="10">10</option>
            <option value="11">11</option>
            <option value="12">12</option>
          </select>
          <button (click)="zoomHome()">Zoom Home</button>
          <select #position (change)="setUseCentroid(position.value)">
            <option value="">Position</option>
            <option
              *ngFor="let rh of positions"
              [value]="stringify(rh.pos)"
              [selected]="positionSelected(rh.pos, useCentroid$ | async)"
            >
              {{ rh.label }}
            </option>
          </select>
        </div>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class MapPageComponent implements AfterViewInit, OnDestroy {
  @ViewChild("lat") latInputRef: ElementRef<HTMLInputElement>;
  @ViewChild("lng") lngInputRef: ElementRef<HTMLInputElement>;
  @ViewChild("geohash") geoHashInputRef: ElementRef<HTMLInputElement>;
  @ViewChild("mapBox") mapBox: MapBoxComponent;
  @ViewChild("timeSlider") timeSlider: RangeSliderComponent;
  @ViewChild("precision") precisionRef: ElementRef<HTMLSelectElement>;

  protected posCentroidCenterNE = [
    RenderHint.CENTROID,
    RenderHint.CENTER,
    RenderHint.NE,
  ];
  protected posCentroidNECenter = [
    RenderHint.CENTROID,
    RenderHint.NE,
    RenderHint.CENTER,
  ];
  protected posCenterNE = [RenderHint.CENTER, RenderHint.NE];
  protected posNECenter = [RenderHint.NE, RenderHint.CENTER];
  protected posCentroidNE = [RenderHint.CENTROID, RenderHint.NE];
  protected posCentroidCenter = [RenderHint.CENTROID, RenderHint.CENTER];

  protected positions = [
    { pos: this.posCentroidCenterNE, label: "1.Centroid-Center-NE" },
    { pos: this.posCentroidNECenter, label: "2.Centroid-NE-Center" },
    { pos: this.posCenterNE, label: "3.Center-NE" },
    { pos: this.posNECenter, label: "4.NE-Center" },
    { pos: this.posCentroidNE, label: "5.Centroid-NE" },
    { pos: this.posCentroidCenter, label: "6.Centroid-Center" },
  ];

  protected precision$: Observable<number>;
  protected facetDisplayHelper: FacetDisplayHelper;

  protected facetFieldByKey$: Observable<any>;
  protected startYear$: BehaviorSubject<number | undefined>;
  protected endYear$: BehaviorSubject<number | undefined>;

  protected mapControls$: BehaviorSubject<boolean>;

  protected docs$: Observable<GeoResult[]>;
  protected docsSubscription: Subscription;
  protected clusterSubscription: Subscription;
  protected countries$: Observable<fromCountries.State>;
  protected countriesSubscription: Subscription;

  protected clusters$: Observable<GeoResult[]>;
  protected clustersSubscription: Subscription;

  protected lastBuckets: GeoJSON.FeatureCollection<Geometry>;
  protected lastPoints: GeoJSON.FeatureCollection<Geometry>;
  protected lastMarkers: GeoJSON.FeatureCollection<Geometry>;
  protected lastCountries: GeoJSON.FeatureCollection<Geometry>;

  protected useCentroid$: BehaviorSubject<RenderHint[]>;

  protected geoPointUpdateTimer: any;

  protected languageChangeSubscription: Subscription;

  protected mapZoom$: Observable<number>;
  protected mapCenter$: Observable<GeoLocation>;

  protected showPreview$: BehaviorSubject<Doc | null>;

  protected yearFacetSubscription: Subscription;

  constructor(
    protected translate: TranslateService,
    protected breakpointObserver: BreakpointObserver,
    protected toastr: I18nToastrService,
    protected searchStore: Store<SearchStateInterface>,
    protected router: Router,
    protected localize: LocalizeRouterService
  ) {
    this.useCentroid$ = new BehaviorSubject<RenderHint[]>(
      environment.mapConfig.positionHint
    );
    environment.mapConfig.credentials = (window as any).MAPBOX_TOKEN;
    if ((window as any).MAPBOX_STYLE) {
      environment.mapConfig.config.style = (window as any).MAPBOX_STYLE;
    }
    this.mapZoom$ = searchStore.pipe(select(getMapZoom));
    this.mapCenter$ = searchStore.pipe(select(getMapCenter));
    this.precision$ = searchStore.pipe(select(getGeoClusterPrecision));
    this.facetDisplayHelper = new FacetDisplayHelper(true);
    this.facetFieldByKey$ = searchStore.pipe(
      select(getFacetHistogramCount),
      filter((x) => typeof x === "object" && x !== null)
    );
    this.startYear$ = new BehaviorSubject<number | undefined>(undefined);
    this.endYear$ = new BehaviorSubject<number | undefined>(undefined);

    this.mapControls$ = new BehaviorSubject<boolean>(false);

    this.lastBuckets = { type: "FeatureCollection", features: [] };
    this.lastPoints = { type: "FeatureCollection", features: [] };
    this.lastMarkers = { type: "FeatureCollection", features: [] };
    this.lastCountries = { type: "FeatureCollection", features: [] };

    this.docs$ = searchStore.pipe(select(getAllGeoResults));
    this.docsSubscription = this.docs$.subscribe((docs) => {
      if (this.map) {
        this.processGeoPoints(docs);
        this.setGeoPoints();
      }
    });

    this.clusterSubscription = this.facetFieldByKey$.subscribe(
      (clusterFacets) => {
        if (this.map) {
          this.processGeoClusters(clusterFacets);
          this.setGeoPoints();
        }
      }
    );

    this.countries$ = searchStore.pipe(select(getCountriesState));
    this.countriesSubscription = this.countries$.subscribe((countries) => {
      this.lastCountries = this.convertCountries(countries.de);
    });

    this.languageChangeSubscription = this.translate.onLangChange.subscribe(
      (params: LangChangeEvent) => this.languageChanged(params.lang)
    );

    this.showPreview$ = new BehaviorSubject<Doc | null>(null);
  }

  protected languageChanged(_newLang: string) {
    const lang = this.translate.currentLang;
    if (lang === "en" && this.lastCountries.features.length > 0) {
      const anyCountry = this.lastCountries.features[0].properties;
      if (!anyCountry.translated) {
        // en was not loaded during map init ... fix it now
        this.lastCountries = {
          type: "FeatureCollection",
          features: this.lastCountries.features.map((f) => {
            const { enCountryName, translated } = this.getEnCountryName(
              f.properties.deLabel
            );
            return {
              ...f,
              properties: {
                ...f.properties,
                enLabel: enCountryName,
                translated,
              },
            };
          }),
        };
        this.setGeoPoints();
      }
    }
    this.map
      .getMap()
      .setLayoutProperty("country", "text-field", ["get", lang + "Label"]);
  }

  mapboxImages(): MapboxImages[] {
    return images;
  }

  protected getEnCountryName(deCountryName: string): {
    enCountryName: string;
    translated: boolean;
  } {
    let enCountryName: string = this.translate.getParsedResult(
      this.translate.translations["en"],
      `env.facetFields.label.fct_countryname.${deCountryName}`
    );
    let translated = true;
    if (enCountryName.startsWith("env.")) {
      // console.log("Fix unknown country translation:", enCountryName);
      enCountryName = deCountryName;
      translated = false;
    }
    return { enCountryName, translated };
  }

  mapboxLayers(): mapboxgl.AnyLayer[] {
    const step1 = 100;
    const step2 = 250;
    const lang = this.translate.currentLang;

    return [
      {
        id: "country",
        type: "symbol",
        source: "earthquakes",
        filter: [
          "all",
          ["==", ["get", "type"], "country"],
          [">=", ["zoom"], 4],
        ],
        layout: {
          "icon-image": ["get", "type"],
          "icon-size": 1,
          "icon-anchor": "bottom-left",
          "text-field": ["get", lang + "Label"],
          "text-font": ["DIN Offc Pro Medium", "Arial Unicode MS Bold"],
          "text-size": 20,
          "text-offset": [0, 1],
          "icon-allow-overlap": true,
          "text-allow-overlap": true,
        },
        paint: {
          "text-color": "#555453",
        },
      },
      {
        id: "unclustered-point",
        type: "symbol",
        source: "earthquakes",
        filter: ["==", ["get", "type"], "point"],
        layout: {
          "icon-image": ["get", "type"],
          "icon-size": 0.5,
          "icon-allow-overlap": true,
          "icon-anchor": "bottom",
          "text-allow-overlap": true,
        },
      },
      {
        id: "clusters",
        type: "circle",
        source: "earthquakes",
        filter: ["has", "point_count"],
        paint: {
          // Use step expressions (https://docs.mapbox.com/mapbox-gl-js/style-spec/#expressions-step)
          "circle-color": [
            "step",
            ["get", "point_count"],
            "#F2ECB5",
            step1,
            "#F6E86B",
            step2,
            "#ECD717",
          ],
          "circle-radius": [
            "step",
            ["get", "point_count"],
            15,
            step1,
            20,
            step2,
            25,
          ],
        },
      },
      {
        id: "cluster-count",
        type: "symbol",
        source: "earthquakes",
        filter: ["has", "point_count"],
        layout: {
          "text-field": ["get", "point_count_abbreviated"],
          "text-font": ["DIN Offc Pro Medium", "Arial Unicode MS Bold"],
          "text-size": 12,
          "icon-allow-overlap": true,
          // "text-allow-overlap": true,
        },
      },
      {
        id: "user-markers",
        type: "symbol",
        source: "earthquakes",
        filter: ["==", ["get", "type"], "flag"],
        layout: {
          "icon-image": ["get", "type"],
          "icon-size": 0.1,
          "icon-allow-overlap": true,
        },
      },
    ];
  }

  mapboxEventHandlers(): MapboxEventHandler<any>[] {
    return [
      {
        type: "click",
        layer: "country",
        listener: (map, e) => {
          const features = map.queryRenderedFeatures(e.point);
          if (features[0].layer.id === "country") {
            const countryValue = JSON.parse(
              e.features[0].properties.facetValue
            );
            const countryLabel = features[0].properties.deLabel;
            const cooKey = environment.mapConfig.coordinateFacetKey;
            this.searchStore.dispatch(
              new RemoveAllHistogramBoundary(cooKey, true)
            );
            const countryKey = environment.countryFacetKey;
            this.searchStore.dispatch(new RemoveAllFacetValue(countryKey));
            this.searchStore.dispatch(
              new AddFacetValue({
                facet: countryKey,
                id: countryLabel,
                label: countryLabel,
                value: countryValue,
              })
            );
            this.goToSearch();
          }
        },
      },

      {
        type: "click",
        layer: "clusters",
        listener: (map, e) => {
          const features = map.queryRenderedFeatures(e.point, {
            layers: ["clusters"],
          });
          const geoHash = features[0].properties.geoHash;
          let precision: number;
          this.precision$.pipe(take(1)).subscribe((p) => (precision = p));
          if ((window as any).DEBUG_COORDS) {
            console.log(
              "Show geohash:",
              geoHash,
              precision,
              this.map.getZoom()
            );
          }
          const mapZoomToPrecision: ZoomToPrecision[] =
            environment.mapConfig?.zoomPrecisionMapping ?? [];
          const lastMapping =
            mapZoomToPrecision.length > 0
              ? mapZoomToPrecision[mapZoomToPrecision.length - 1]
              : undefined;
          const switchToSearch =
            mapZoomToPrecision.length > 0 &&
            precision === lastMapping?.precision &&
            Math.abs(this.map.getZoom() - lastMapping?.zoomEnd) < 0.01;
          if (switchToSearch) {
            this.goToSearch();
          } else {
            // zoom in ...
            const geoHashRect = GeoHash.neighbours(geoHash);
            const sw = GeoHash.decode(geoHashRect.sw);
            const ne = GeoHash.decode(geoHashRect.ne);
            map.fitBounds(new mapboxgl.LngLatBounds(sw, ne));
          }
        },
      } as MapboxEventHandler<"click">,

      {
        type: "click",
        layer: "unclustered-point",
        listener: (map, e) => {
          const coordinates = this.featureCoo(e.features).slice() as [
            number,
            number
          ];
          const docData = e.features[0].properties;
          // const geoHash = docData.geoHash;
          // const lat = docData.lat;
          // const lng = docData.lng;

          // Ensure that if the map is zoomed out such that
          // multiple copies of the feature are visible, the
          // popup appears over the copy being pointed to.
          while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
            coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
          }

          let currentSnippet;
          this.showPreview$
            .pipe(take(1))
            .subscribe((s) => (currentSnippet = s));

          try {
            const snippet = JSON.parse(docData.snippet);
            if (
              currentSnippet?.id === snippet?.id &&
              snippet !== null &&
              typeof snippet === "object"
            ) {
              this.closePreview();
            } else {
              this.showPreview$.next(snippet);
            }
          } catch (ex) {
            console.error("Can't parse:\n", docData.snippet);
          }
        },
      } as MapboxEventHandler<"click">,

      {
        type: "mouseenter",
        layer: ["clusters", "unclustered-point", "country"],
        listener: (map, e) => {
          map.getCanvas().style.cursor = "pointer";
          if ((window as any).DEBUG_COORDS) {
            console.log("GEOHASH", e.features[0].properties.geoHash);
          }
        },
      } as MapboxEventHandler<"mouseenter">,

      {
        type: "mouseleave",
        layer: ["clusters", "unclustered-point", "country"],
        listener: (map) => {
          map.getCanvas().style.cursor = "";
        },
      } as MapboxEventHandler<"mouseleave">,
    ];
  }

  closePreview() {
    this.showPreview$.next(null);
  }

  protected goToSearch() {
    this.switchToSearch(false);
  }

  protected featureCoo(
    features: mapboxgl.MapboxGeoJSONFeature[]
  ): [number, number] {
    return (features[0].geometry as Point).coordinates as [number, number];
  }

  protected convertCountries(
    facetCountries: Country[]
  ): GeoJSON.FeatureCollection<GeoJSON.Geometry> {
    const features = [];
    facetCountries.forEach((c) => {
      const { enCountryName, translated } = this.getEnCountryName(c.label);
      const properties: { [key: string]: any } = {
        id: c.label,
        deLabel: c.label,
        enLabel: enCountryName,
        translated,
        facetValue: c.value,
        lng: c.geopoint.lon,
        lat: c.geopoint.lat,
        type: "country",
      };
      features.push({
        type: "Feature",
        properties,
        geometry: {
          type: "Point",
          coordinates: [properties.lng, properties.lat],
        },
      });
    });
    return {
      type: "FeatureCollection",
      features,
    };
  }

  protected processGeoClusters(clusterFacets: { [key: string]: GeoBucket[] }) {
    const key =
      environment.facetFields[environment.mapConfig.coordinateFacetKey].field;
    const mapBounds = this.map.currentRectangle();
    const minLon = mapBounds.topLeft.lng;
    const maxLon = mapBounds.bottomRight.lng;
    const minLat = mapBounds.bottomRight.lat;
    const maxLat = mapBounds.topLeft.lat;
    let useCentroid;
    this.useCentroid$.pipe(take(1)).subscribe((v) => (useCentroid = v));
    const geoJson = geoFacetToGeoJson(
      clusterFacets[key],
      minLon,
      maxLon,
      minLat,
      maxLat,
      useCentroid
    );
    this.lastBuckets = geoJson;
  }

  protected processGeoPoints(docs: GeoResult[]) {
    const mapBounds = this.map.currentRectangle();
    const minLon = mapBounds.topLeft.lng;
    const maxLon = mapBounds.bottomRight.lng;
    const minLat = mapBounds.bottomRight.lat;
    const maxLat = mapBounds.topLeft.lat;
    let useCentroid;
    this.useCentroid$.pipe(take(1)).subscribe((v) => (useCentroid = v));
    const geoJson = geoSnippetsToGeoJson(
      docs as unknown as GeoPoint[],
      minLon,
      maxLon,
      minLat,
      maxLat,
      useCentroid
    );
    this.lastPoints = geoJson;
  }

  protected setGeoPoints() {
    // debounce map updates ...
    if (this.geoPointUpdateTimer) {
      clearTimeout(this.geoPointUpdateTimer);
    }

    this.geoPointUpdateTimer = setTimeout(() => {
      // use latest versions!
      const allPoints = this.lastPoints.features
        .concat(this.lastBuckets.features)
        .concat(this.lastMarkers.features)
        .concat(this.lastCountries.features);
      if ((window as any).DEBUG_COORDS) {
        console.log(
          "Showing total points:",
          allPoints.length,
          ", single:",
          this.lastPoints.features.length,
          ", cluster:",
          this.lastBuckets.features.length,
          ", countries:",
          this.lastCountries.features.length,
          ", markers:",
          this.lastMarkers.features.length
        );
      }
      this.map.setGeoPoints({
        type: "FeatureCollection",
        features: allPoints,
      });
    }, 250);
  }

  stringify(v: any): string {
    return JSON.stringify(v);
  }

  positionSelected(renderHint: RenderHint[], useCentroid): boolean {
    return JSON.stringify(renderHint) === JSON.stringify(useCentroid);
  }

  updateMap(mapLimits: MapLimits) {
    const newCenter = mapLimits.center;
    this.searchStore.dispatch(
      new SetCenter({ lat: newCenter.lat, lon: newCenter.lng })
    );

    this.searchStore.dispatch(new SetZoom(mapLimits.zoom));

    const precisionSelect = this.precisionRef?.nativeElement;
    const precisionMode = precisionSelect
      ? precisionSelect.selectedOptions[precisionSelect.selectedIndex].value
      : "auto";
    if (precisionMode === "auto") {
      this.handleAutoPrecision(mapLimits.zoom);
    }

    this.updateCooSearchData();

    this.execSearch();
  }

  protected handleAutoPrecision(zoom: number) {
    const precision = precisionForZoom(
      zoom,
      environment.mapConfig.zoomPrecisionMapping
    );
    if ((window as any).DEBUG_COORDS) {
      console.log("Using precision", precision);
    }
    this.searchStore.dispatch(new SetGeoClusterPrecision(precision));
  }

  closedInput($event) {
    if ($event.code === "Enter") {
      $event.target.blur();
    }
  }

  toggleMapControls() {
    let open: boolean;
    this.mapControls$.pipe(take(1)).subscribe((v) => {
      open = v;
    });
    this.mapControls$.next(!open);
  }

  setYear(v: number, year$: BehaviorSubject<number>) {
    const currentYear = new Date().getFullYear();
    year$.next(Math.max(MIN_YEAR, Math.min(v, currentYear)));
  }

  protected showYearError(
    i18nKey: string,
    values: { [key: string]: string | number }
  ) {
    this.toastr.error(
      { key: i18nKey, values: values },
      "map-page.year-error-toast-title"
    );
  }

  protected prepareYearInput(year: string | undefined): number | undefined {
    if (year === undefined) {
      return undefined;
    }
    year = year.trim();
    if (year.length === 0) {
      return undefined;
    } else {
      const onlyDigits = year.match(/^[0-9]{3,4}$/);
      if (onlyDigits === null) {
        this.showYearError("map-page.year-bad-format", { year });
        return undefined;
      }
    }
    const yearNum = parseInt(year, 10);
    if (yearNum < MIN_YEAR) {
      this.showYearError("map-page.year-too-early", { year });
      return undefined;
    }
    const currentYear = new Date().getFullYear();
    if (yearNum > currentYear) {
      this.showYearError("map-page.future-year", { year });
      return undefined;
    }
    return Math.min(currentYear, yearNum);
  }

  protected convertLatLon(coo: mapboxgl.LatLng): { lat: number; lon: number } {
    const newCoo: { lat: number; lon: number } = {
      lat: coo.lat,
      lon: coo.lng,
    };
    return newCoo;
  }

  switchToSearch(setCoordFacet: boolean) {
    if (setCoordFacet) {
      this.updateCooSearchData();
    }
    this.router.navigate([this.localize.translateRoute("/search")]);
  }

  selectTimeRange(startYearRaw: string, endYearRaw: string) {
    const yearValidity = this.checkTimeRange(
      startYearRaw,
      endYearRaw,
      (startYear, endYear) => {
        this.updateYearSearchData(startYear, endYear);
        this.execSearch();
      }
    );
    // user hit "OK", but both input fields are empty
    // clear the facet value
    if (yearValidity === "UNDEFINED") {
      this.searchStore.dispatch(
        new RemoveAllHistogramBoundary(environment.mapConfig.yearFacetKey, true)
      );
      this.execSearch();
    }
  }

  /**
   * Returns "UNDEFINED" if both year params are undefined.
   * Returns "OK" if one of the year params is not undefined and a valid number.
   * Returns "INVALID" otherwise.
   *
   * @param startYearRaw
   * @param endYearRaw
   * @param validAction
   * @returns "OK" or "INVALID" or "UNDEFINED"
   */
  checkTimeRange(
    startYearRaw: string | undefined,
    endYearRaw: string | undefined,
    validAction: (startYear: number, endYear: number) => void
  ): "OK" | "INVALID" | "UNDEFINED" {
    try {
      const startYear = this.prepareYearInput(startYearRaw);
      const endYear = this.prepareYearInput(endYearRaw);

      if (startYear !== undefined || endYear !== undefined) {
        // can't use pipe expression in action handler!
        const wrongOrder =
          startYear !== undefined &&
          endYear !== undefined &&
          startYear > endYear;
        if (!wrongOrder) {
          validAction(startYear, endYear);
          if (startYear !== undefined) {
            this.setYear(startYear, this.startYear$);
            this.timeSlider.setLeftThumb(startYear);
          }
          if (endYear !== undefined) {
            this.setYear(endYear, this.endYear$);
            this.timeSlider.setRightThumb(endYear);
          }
          return "OK";
        } else {
          this.showYearError("map-page.wrong-year-order", {
            startYear,
            endYear,
          });
          return "INVALID";
        }
      } else {
        return "UNDEFINED";
      }
    } catch (e) {
      console.error("Bad range input", e);
      return "INVALID";
    }
  }

  protected updateYearSearchData(
    startYear: number | undefined,
    endYear: number | undefined
  ) {
    if (startYear !== undefined || endYear !== undefined) {
      const yearKey = environment.mapConfig.yearFacetKey;
      const { label: yearLabel, value: yearValue } =
        AbstractHistogramFacetComponentDirective.buildLabelAndValue(
          startYear !== undefined ? "" + startYear : undefined,
          endYear !== undefined ? "" + endYear : undefined
        );
      const yearId = buildId(yearValue);
      const year = {
        key: yearKey,
        label: yearLabel,
        id: yearId,
        value: yearValue,
      };
      this.searchStore.dispatch(new RemoveAllHistogramBoundary(year.key, true));
      this.searchStore.dispatch(new AddHistogramBoundaries(year));
    }
  }

  protected updateCooSearchData() {
    const { topLeft, bottomRight } = this.map.currentRectangle();
    const { label: cooLabel, value: cooValue } =
      AbstractHistogramFacetComponentDirective.buildLabelAndValue(
        JSON.stringify(this.convertLatLon(topLeft)),
        JSON.stringify(this.convertLatLon(bottomRight))
      );
    const cooId = buildId(cooValue);

    const coo = {
      key: environment.mapConfig.coordinateFacetKey,
      label: cooLabel,
      id: cooId,
      value: cooValue,
    };

    this.searchStore.dispatch(new RemoveAllHistogramBoundary(coo.key, true));
    this.searchStore.dispatch(new AddHistogramBoundaries(coo));
  }

  execSearch() {
    this.searchStore.dispatch(new SetOffset(0));
    this.searchStore.dispatch(new SimpleSearch());
  }

  protected get map(): MapBoxComponent {
    return this.mapBox;
  }

  setPrecision(v: string) {
    if (v !== "auto") {
      const precision = parseInt(v, 10);
      this.searchStore.dispatch(new SetGeoClusterPrecision(precision));
    } else {
      this.handleAutoPrecision(this.map.getZoom());
    }
    this.execSearch();
  }

  setUseCentroid(position: string) {
    const renderHint: RenderHint[] = JSON.parse(position);
    this.useCentroid$.next(renderHint);
    this.execSearch();
  }

  fly() {
    const lng = parseFloat(this.lngInputRef.nativeElement.value);
    const lat = parseFloat(this.latInputRef.nativeElement.value);
    console.log("Fly to lat=", lat, "lng=", lng);
    this.map.flyTo({ center: { lng, lat } });
    this.lastMarkers.features.push({
      type: "Feature",
      properties: { id: "id" + new Date().getTime(), type: "flag" },
      geometry: { type: "Point", coordinates: [lng, lat] },
    });
    this.setGeoPoints();
  }

  flyGeoHash() {
    const geoHash = this.geoHashInputRef.nativeElement.value;
    console.log("Fly to geo hash=", geoHash);
    const coo = GeoHash.decode(geoHash);
    const { ne, sw } = GeoHash.bounds(geoHash);
    this.map.flyTo({ center: { lng: coo.lon, lat: coo.lat } });
    this.lastMarkers.features.push({
      type: "Feature",
      properties: { id: "id1" + new Date().getTime(), type: "flag" },
      geometry: { type: "Point", coordinates: [ne.lon, ne.lat] },
    });
    this.lastMarkers.features.push({
      type: "Feature",
      properties: { id: "id2" + new Date().getTime(), type: "flag" },
      geometry: { type: "Point", coordinates: [sw.lon, sw.lat] },
    });
    this.setGeoPoints();
  }

  zoomHome() {
    this.map.zoomTo(environment.mapConfig.config.zoom, { duration: 2000 });
  }

  protected decodeFacetValue(
    selectedFacetValues: any
  ):
    | { gte: mapboxgl.LatLng | number; lte: mapboxgl.LatLng | number }
    | undefined {
    const allValues = selectedFacetValues?.values;
    if (allValues === undefined || allValues.length === 0) {
      return undefined;
    }
    let { gte, lte } = allValues[allValues.length - 1].value;
    if (typeof gte === "string") {
      gte = JSON.parse(gte);
    }
    if (typeof lte === "string") {
      lte = JSON.parse(lte);
    }
    return { gte, lte };
  }

  initYearSlider(): ThumbData {
    return { left: this.defaultStartYear, right: this.defaultEndYear };
  }

  ngAfterViewInit() {
    this.yearFacetSubscription = this.searchStore
      .select(getHistogramValues)
      .subscribe((histogramFacets) => {
        const yearValue = this.decodeFacetValue(
          histogramFacets[environment.mapConfig.yearFacetKey]
        );

        let startYear: number | undefined;
        let endYear: number | undefined;
        if (yearValue) {
          const { gte: yearGte, lte: yearLte } = yearValue;
          startYear = yearGte;
          endYear = yearLte;
        }
        this.timeSlider.setLeftThumb(startYear);
        this.timeSlider.setRightThumb(endYear);
        this.startYear$.next(startYear);
        this.endYear$.next(endYear);
      });

    this.searchStore
      .select(getHistogramValues)
      .pipe(take(1))
      .subscribe((histogramFacets) => {
        const cooValue = this.decodeFacetValue(
          histogramFacets[environment.mapConfig.coordinateFacetKey]
        );

        this.facetDisplayHelper.init(this.breakpointObserver);

        if (!cooValue) {
          this.updateCooSearchData();
          this.execSearch();
        } else {
          // has facet coordinate
          let geoCenter: GeoLocation;
          this.searchStore
            .pipe(take(1), select(getMapCenter))
            .subscribe((v) => (geoCenter = v));
          // if center is set to the inital value, prefer the facet's value
          if (
            geoCenter &&
            environment.mapConfig.config.lat === geoCenter.lat &&
            environment.mapConfig.config.lng === geoCenter.lon
          ) {
            const { gte, lte } = cooValue;
            const facetLat = gte.lat + (lte.lat - gte.lat) / 2;
            const facetLon = gte.lon + (lte.lon - gte.lon) / 2;
            this.searchStore.dispatch(
              new SetCenter({ lat: facetLat, lon: facetLon })
            );
            this.map
              .getMap()
              .flyTo({ center: { lat: facetLat, lng: facetLon } });
          }
          this.searchStore
            .select(getAllGeoResults)
            .pipe(take(1))
            .subscribe((docs) => {
              this.processGeoPoints(docs);
            });
          this.facetFieldByKey$.pipe(take(1)).subscribe((clusterFacets) => {
            this.processGeoClusters(clusterFacets);
          });
          this.setGeoPoints();
        }
      });
  }

  // also called on every year scale thumb move!
  initialArea(): MapConfig {
    const customMapConfig = { ...this.env.mapConfig };
    customMapConfig.config = { ...customMapConfig.config };

    // use last zoom
    this.mapZoom$
      .pipe(take(1))
      .subscribe((z) => (customMapConfig.config.zoom = z));

    // use global store ... because selected facet coords were perhaps modified for search
    this.mapCenter$.pipe(take(1)).subscribe((c) => {
      customMapConfig.config.lat =
        typeof c.lat === "number" && !isNaN(c.lat)
          ? c.lat
          : customMapConfig.config.lat;
      customMapConfig.config.lng =
        typeof c.lon === "number" && !isNaN(c.lon)
          ? c.lon
          : customMapConfig.config.lng;
    });

    return customMapConfig;
  }

  ngOnDestroy(): void {
    this.facetDisplayHelper.close();

    if (this.docsSubscription) {
      this.docsSubscription.unsubscribe();
    }

    if (this.clusterSubscription) {
      this.clusterSubscription.unsubscribe();
    }

    if (this.languageChangeSubscription) {
      this.languageChangeSubscription.unsubscribe();
    }

    if (this.yearFacetSubscription) {
      this.yearFacetSubscription.unsubscribe();
    }
  }

  get env(): SettingsModel {
    return environment;
  }

  get timeSliderTicks() {
    return defaultTimeSliderTicks;
  }

  get defaultStartYear() {
    return MIN_YEAR;
  }

  get defaultEndYear() {
    const currentYear = new Date().getFullYear();
    return Math.min(
      currentYear,
      defaultTimeSliderTicks[defaultTimeSliderTicks.length - 1].value
    );
  }

  debugCoords(): boolean {
    return !!(window as any).DEBUG_COORDS;
  }
}
