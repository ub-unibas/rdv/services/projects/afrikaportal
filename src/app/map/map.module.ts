import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { CountriesModule } from "@app/countries/countries.module";
import { SharedModule } from "@app/shared/shared.module";
import { LocalizeRouterModule } from "@gilsdav/ngx-translate-router";
import { StoreModule } from "@ngrx/store";
import { TranslateModule } from "@ngx-translate/core";
import { PipesModule, SearchModule } from "@ub-unibas/rdv_viewer-lib/components";
import { MapBoxModule } from "@ub-unibas/rdv_viewer-lib/mapboxgl";
import {
  ToolModule,
  reducers as mapReducers,
} from "@ub-unibas/rdv_viewer-lib/tool";
import { ToastrModule } from "ngx-toastr";
import { MapPageComponent } from "./components/map-page.component";

const routes: Routes = [{ path: "", component: MapPageComponent }];

const MAP_PAGE_COMPONENTS = [MapPageComponent];

@NgModule({
  imports: [
    CommonModule,
    PipesModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild(),
    LocalizeRouterModule.forChild(routes),
    StoreModule.forFeature("map", mapReducers),
    FormsModule,
    ToastrModule,
    MapBoxModule,
    SharedModule,
    ToolModule,
    CountriesModule,
    SearchModule
  ],
  declarations: MAP_PAGE_COMPONENTS,
  providers: [],
  exports: MAP_PAGE_COMPONENTS,
})
export class MapModule {}
