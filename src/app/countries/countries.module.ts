import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { EffectsModule } from "@ngrx/effects";
import { StoreModule } from "@ngrx/store";
import { TranslateModule } from "@ngx-translate/core";
import { CountriesEffects } from "./effects/country.effects";
import { reducers } from "./reducers";

const COMPONENTS = [];

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature("countries", reducers),
    EffectsModule.forFeature([CountriesEffects]),
    TranslateModule.forChild(),
  ],
  declarations: COMPONENTS,
  providers: [],
  exports: COMPONENTS,
})
export class CountriesModule {}
