import {
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
} from "@ngrx/store";
import * as fromCountries from "./countries.reducer";

interface CountriesState {
  countries: fromCountries.State;
}

export const reducers: ActionReducerMap<CountriesState> = {
  countries: fromCountries.reducer,
};

const _getCountries = createFeatureSelector<CountriesState>("countries");

export const getCountriesState = createSelector(
  _getCountries,
  (state) => state.countries
);

export const getDeCountries = createSelector(
  getCountriesState,
  (state) => state.de
);
