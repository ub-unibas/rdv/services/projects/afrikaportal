import {
  CountriesActions,
  CountriesActionTypes,
  Country,
} from "../actions/countries.actions";

export interface State {
  de: Country[];
}

/**
 * @ignore
 */
export const initialState: State = { de: [] };

/**
 * @ignore
 */
export function reducer(state = initialState, action: CountriesActions): State {
  switch (action.type) {
    case CountriesActionTypes.SetCountries: {
      const newState = { ...state };
      newState[action.payload.language] = action.payload.countries;
      return newState;
    }

    default: {
      return state;
    }
  }
}
