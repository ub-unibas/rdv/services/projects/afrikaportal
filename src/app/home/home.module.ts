import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "@app/shared/shared.module";
import { LocalizeRouterModule } from "@gilsdav/ngx-translate-router";
import { TranslateModule } from "@ngx-translate/core";
import { PipesModule } from "@ub-unibas/rdv_viewer-lib/components";
import { ToastrModule } from "ngx-toastr";
import { FlagComponent } from "./components/flag.component";
import { HomeMoodComponent } from "./components/home-mood.component";
import { HomePageComponent } from "./components/home-page.component";
import { HomeComponent } from "./components/home.component";

const routes: Routes = [{ path: "", component: HomePageComponent }];

const HOME_PAGE_COMPONENTS = [
  HomePageComponent,
  HomeComponent,
  HomeMoodComponent,
  FlagComponent,
];

@NgModule({
  imports: [
    CommonModule,
    PipesModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild(),
    LocalizeRouterModule.forChild(routes),
    FormsModule,
    ToastrModule,
    SharedModule,
  ],
  declarations: HOME_PAGE_COMPONENTS,
  providers: [],
  exports: HOME_PAGE_COMPONENTS,
})
export class HomeModule {}
