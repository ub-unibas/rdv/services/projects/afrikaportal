import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import { ChangeDetectionStrategy, Component } from "@angular/core";
import { getExhibitionsState } from "@app/exhibitions/reducers";
import { State as ExhibitionState } from "@app/exhibitions/reducers/exhibitions.reducer";
import { ExhibitionData } from "@app/shared/components/ExhibitionData";
import { ExhibitionService } from "@app/shared/services/ExhibitionService";
import { Store, select } from "@ngrx/store";
import { SearchStateInterface } from "@ub-unibas/rdv_viewer-lib/components";
import { Observable } from "rxjs";
import { HomeMoodComponent } from "./home-mood.component";
import { HomeComponent } from "./home.component";

@Component({
  selector: "app-home-page",
  template: `
    <app-simple-page
      [deComponent]="component"
      [enComponent]="component"
      baseClass="home-page"
      [componentInputs]="getLocalizedExhibitions(exhibitions$ | async)"
      [moodComponent]="moodComponent"
    ></app-simple-page>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class HomePageComponent {
  exhibitions$: Observable<ExhibitionState>;
  component = HomeComponent;
  moodComponent = HomeMoodComponent;

  constructor(
    protected searchStore: Store<SearchStateInterface>,
    protected exhibitionService: ExhibitionService
  ) {
    this.exhibitions$ = this.searchStore.pipe(select(getExhibitionsState));
  }

  getLocalizedExhibitions(exhibitions: ExhibitionState): ExhibitionData {
    return this.exhibitionService.getLocalizedExhibitions(exhibitions);
  }
}
