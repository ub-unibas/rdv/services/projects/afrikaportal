import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from "@angular/core";

@Component({
  selector: "app-flag",
  template: `
    <button (click)="triggerClicked()">
      <svg
        preserveAspectRatio="xMidYMid meet"
        viewBox="0 0 240 181"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path d="M0 0H240V150H0V0Z" fill="#ECD717" />
        <text
          fill="black"
          xml:space="preserve"
          style="white-space: pre"
          font-family="Titillium Web"
          font-size="26"
          font-weight="600"
          letter-spacing="0.01em"
        >
          <tspan x="36" y="60.685">{{ line1 }}</tspan>
          <tspan x="36" y="90.685">{{ line2 }}</tspan>
        </text>
        <path d="M17.5 181L0 150H35L17.5 181Z" fill="#ECD717" />
      </svg>
      <div class="exhibition-preview__arrow-container">
        <div class="exhibition-preview__arrow"></div>
      </div>
    </button>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class FlagComponent {
  @Input() line1: string;
  @Input() line2: string;
  @Output() clicked: EventEmitter<void>;

  constructor() {
    this.clicked = new EventEmitter();
  }

  triggerClicked() {
    this.clicked.emit();
  }
}
