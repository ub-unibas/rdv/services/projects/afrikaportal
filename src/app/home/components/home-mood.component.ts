import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import { ChangeDetectionStrategy, Component } from "@angular/core";
import { Router } from "@angular/router";
import { MoodInfo } from "@app/shared/components/mood.component";
import { LocalizeRouterService } from "@gilsdav/ngx-translate-router";

const moods: MoodInfo[] = [
  {
    forWidth: 768,
    imgUrl: "/assets/img/moods/home-768-new.svg",
  },
  {
    forWidth: 769,
    imgUrl: "/assets/img/moods/home-1680.svg",
  },
];

@Component({
  selector: "app-home-mood",
  template: `
    <div class="home-mood">
      <div class="mood-container">
        <div class="mood-img">
          <div class="home-mood__mission-container">
            <div class="home-mood__mission">
              <p>{{ "home-page.mission.line1" | translate }}</p>
              <p>{{ "home-page.mission.line2" | translate }}</p>
              <div class="blue-navi-button">
                <a
                  [routerLink]="['/simple/about' | localize]"
                  [title]="'main-navi.about' | translate"
                >
                  {{ "home-page.about" | translate }}
                </a>
              </div>
            </div>
            <app-mood [moodInfo]="moodInfo"></app-mood>
            <app-flag
              class="home-mood__search-filter"
              line1="{{
                'home-page.mood-flag-search-filter.line1' | translate
              }}"
              line2="{{
                'home-page.mood-flag-search-filter.line2' | translate
              }}"
              (clicked)="goSearch()"
            ></app-flag>
            <app-flag
              class="home-mood__map"
              line1="{{ 'home-page.mood-flag-map.line1' | translate }}"
              line2="{{ 'home-page.mood-flag-map.line2' | translate }}"
              (clicked)="goMap()"
            ></app-flag>
          </div>
        </div>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class HomeMoodComponent {
  moodInfo = moods;

  constructor(
    protected router: Router,
    protected localize: LocalizeRouterService
  ) {}

  goSearch() {
    this.router.navigate([this.localize.translateRoute("/search")]);
  }

  goMap() {
    this.router.navigate([this.localize.translateRoute("/map")]);
  }
}
