import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import { ChangeDetectionStrategy, Component, Input } from "@angular/core";
import { Exhibition } from "@app/exhibitions/actions/exhibitions.actions";

@Component({
  selector: "app-home-de",
  template: `
    <div>
      <h1>{{ "home-page.exhibitions" | translate }}</h1>
      <app-exhibition-preview
        [exhibitions]="exhibitions"
      ></app-exhibition-preview>
      <app-sponsors></app-sponsors>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class HomeComponent {
  @Input() exhibitions: Exhibition[];

  constructor() {}
}
