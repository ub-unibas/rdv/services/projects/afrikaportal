import { Location, registerLocaleData } from "@angular/common";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import {
  Route,
  RouterModule,
  Routes,
  UrlMatchResult,
  UrlSegment,
  UrlSegmentGroup,
  UrlSerializer,
} from "@angular/router";

import localeDe from "@angular/common/locales/de";
import localeEn from "@angular/common/locales/en";
import localeDeExtra from "@angular/common/locales/extra/de";
import localeEnExtra from "@angular/common/locales/extra/en";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {
  LocalizeParser,
  LocalizeRouterModule,
  LocalizeRouterService,
  LocalizeRouterSettings,
  ManualParserLoader,
} from "@gilsdav/ngx-translate-router";
import { EffectsModule } from "@ngrx/effects";
import { StoreModule } from "@ngrx/store";
import {
  TranslateCompiler,
  TranslateLoader,
  TranslateModule,
  TranslateService,
} from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import {
  CoreModule,
  EnvTranslateCompiler,
  LOCALIZED_ROUTER_PREFIX,
  PreviousRouteServiceProvider,
  RemoteFilterConfigsEffects,
  URLSearchParamsUrlSerializer,
  backendSearchServiceProvider,
  coreReducers,
  metaReducers,
} from "@ub-unibas/rdv_viewer-lib/components";
import { DynamicModule } from "ng-dynamic-component";
import { ToastrModule } from "ngx-toastr";
import { AppComponent } from "./app.component";
import { CountriesModule } from "./countries/countries.module";
import { ExhibitionsModule } from "./exhibitions/exhibitions.module";

registerLocaleData(localeDe, "de-DE", localeDeExtra);
registerLocaleData(localeEn, "en-GB", localeEnExtra);

// needs custom url matcher, to catch not localized urls e.g. "/popup?..."
export function popupUrlMatcher(
  segments: UrlSegment[],
  group: UrlSegmentGroup,
  route: Route
): UrlMatchResult {
  let res: UrlMatchResult = null;
  if (segments && segments.length === 1 && segments[0].path === "popup") {
    res = { consumed: segments };
  }
  return res;
}

export const routes: Routes = [
  {
    path: "home",
    loadChildren: () => import("./home/home.module").then((m) => m.HomeModule),
  },
  {
    path: "detail/:id",
    loadChildren: () =>
      import("./detail/detail.module").then((m) => m.DetailModule),
  },
  {
    path: "search",
    loadChildren: () =>
      import("./search/search.module").then((m) => m.AfricaPortalSearchModule),
  },
  {
    path: "map",
    loadChildren: () => import("./map/map.module").then((m) => m.MapModule),
  },
  {
    path: "simple",
    loadChildren: () =>
      import("./simple-pages/simple-pages.module").then(
        (m) => m.SimplePagesModule
      ),
  },
  {
    path: "",
    pathMatch: "full",
    redirectTo: "/home",
  },
  {
    path: "**",
    redirectTo: "/home",
  },
];

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

/**
 * Root module
 */
@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    TranslateModule.forRoot({
      useDefaultLang: true,
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
      compiler: {
        provide: TranslateCompiler,
        useClass: EnvTranslateCompiler,
      },
    }),
    RouterModule.forRoot(routes),
    CoreModule,
    LocalizeRouterModule.forRoot(routes, {
      defaultLangFunction: (
        _languages: string[],
        _cachedLang?: string,
        browserLang?: string
      ) => (browserLang?.startsWith("de") ? "de" : "en"),
      parser: {
        provide: LocalizeParser,
        useFactory: (translate, location, settings) =>
          new ManualParserLoader(
            translate,
            location,
            settings,
            ["de", "en"],
            LOCALIZED_ROUTER_PREFIX
          ),
        deps: [TranslateService, Location, LocalizeRouterSettings],
      },
    }),
    StoreModule.forRoot(coreReducers, {
      metaReducers,
      runtimeChecks: {
        strictStateImmutability: false,
        strictActionImmutability: false,
      },
    }),
    EffectsModule.forRoot([RemoteFilterConfigsEffects]),
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    DynamicModule,
    CountriesModule,
    ExhibitionsModule,
  ],
  declarations: [AppComponent],
  providers: [
    backendSearchServiceProvider,
    PreviousRouteServiceProvider,
    LocalizeRouterService,
    {
      provide: UrlSerializer,
      useClass: URLSearchParamsUrlSerializer,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
