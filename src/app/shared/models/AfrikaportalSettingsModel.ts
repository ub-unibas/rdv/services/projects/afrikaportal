import {
  CoreSettings,
  Endpoints,
  ExtendedSettings,
} from "@ub-unibas/rdv_viewer-lib/components";

export interface ParcEndpoints extends Endpoints {
  /**
   * URL of countries proxy script
   */
  readonly countriesProxyUrl: string;

  /**
   * URL of exhibitions proxy script
   */
  readonly exhibitionProxyUrl: string;
}

export type ParcBaseSettings = CoreSettings & ParcEndpoints;

export type ParcSettingsModel = ParcBaseSettings &
  ExtendedSettings & {
    /**
     * country facet key in environment.facetFields
     */
    countryFacetKey: string;
  };
