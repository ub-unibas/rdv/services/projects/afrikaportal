import {
  endpointBasePaths,
  endpoints,
} from "@ub-unibas/rdv_viewer-lib/components";
import { ParcEndpoints } from "./AfrikaportalSettingsModel";

export function parcEndpointBasePaths(hostVersion: string): ParcEndpoints {
  return {
    ...endpointBasePaths(hostVersion),
    countriesProxyUrl: hostVersion + "/rdv_query/es_proxy/",
    exhibitionProxyUrl: hostVersion + "/content/exhibit/",
  };
}

export function parcEndpoints(
  project: string,
  basePaths: ParcEndpoints
): ParcEndpoints {
  return endpoints(project, basePaths) as ParcEndpoints;
}
