import { BreakpointObserver } from "@angular/cdk/layout";
import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnDestroy,
} from "@angular/core";
import { Exhibition } from "@app/exhibitions/actions/exhibitions.actions";
import { BehaviorSubject, Subscription, debounceTime } from "rxjs";

@Component({
  selector: "app-exhibition-preview",
  template: `
    <div>
      <div #exhibition class="exhibition-preview">
        <ng-container *ngFor="let ex of firstExhibitionObjects()">
          <div
            class="exhibition-preview__item"
            [style.max-width]="itemWidth(exhibition, breakpoint$ | async)"
          >
            <div class="exhibition-preview__thumb-container">
              <img
                class="exhibition-preview__thumb"
                src="{{ ex['URL-Thumbnail'] }}"
              />
            </div>
            <button
              class="exhibition-preview__show"
              (click)="goToObject(ex['URL-Exhibit'])"
              [title]="ex.Titel"
            >
              <div class="exhibition-preview__title">{{ ex.Titel }}</div>
              <div class="exhibition-preview__arrow"></div>
            </button>
          </div>
        </ng-container>
      </div>
      <ng-container *ngIf="showAllButton">
        <div class="blue-navi-button">
          <a
            [routerLink]="['/simple/exhibition' | localize]"
            [title]="'main-navi.to_home' | translate"
          >
            {{ "home-page.all-exhibitions" | translate }}
          </a>
        </div>
      </ng-container>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class ExhibitionPreviewComponent implements OnDestroy {
  @Input() exhibitions: Exhibition[];
  @Input() startIndex = 0;
  @Input() count = 3;
  @Input() showAllButton = true;

  protected breakPointSubscription: Subscription;

  breakpoint$: BehaviorSubject<any>;

  constructor(protected breakpointObserver: BreakpointObserver) {
    this.breakpoint$ = new BehaviorSubject<any>(null);

    this.breakPointSubscription = breakpointObserver
      .observe(["(min-width: 601px)", "(max-width:600"]) // see _page-home.scss!
      .pipe(debounceTime(250))
      .subscribe(() => this.rerenderUi());
  }

  ngOnDestroy(): void {
    if (this.breakPointSubscription) {
      this.breakPointSubscription.unsubscribe();
    }
  }

  rerenderUi() {
    this.breakpoint$.next(new Date().getTime());
  }

  firstExhibitionObjects(): Exhibition[] {
    if (this.exhibitions.length <= this.count - this.startIndex) {
      return this.exhibitions;
    }
    return this.exhibitions.slice(
      this.startIndex,
      this.startIndex + this.count
    );
  }

  goToObject(url: string) {
    window.open(url, "_blank");
  }

  itemWidth(exhibition: HTMLDivElement, _breakpoint: any): string {
    const flexDirection = window.getComputedStyle(exhibition).flexDirection;
    if (flexDirection === "column") {
      return "100%";
    }
    const itemsToShow = this.firstExhibitionObjects().length;
    if (itemsToShow === 0) {
      return "100%";
    }
    return Math.floor(90 / itemsToShow) + "%";
  }
}
