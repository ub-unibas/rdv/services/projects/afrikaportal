import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
  ViewChild,
} from "@angular/core";
import { Store, select } from "@ngrx/store";
import { TranslateService } from "@ngx-translate/core";
import {
  SearchStateInterface,
  getTotalResultsCount,
} from "@ub-unibas/rdv_viewer-lib/components";
import { BehaviorSubject, Observable, take } from "rxjs";

export interface Tick {
  label: string;
  value: number;
  hideTick?: boolean;
}

export interface ThumbData {
  left: number;
  right: number;
}

// compare styles.scss at range-slider rules!
const rangeSliderThumbWidth = 44;
const rangeSliderTickWidth = 6;

@Component({
  selector: "app-range-slider",
  template: `
    <div #rangeSliderArea class="range-slider" draggable="false">
      <div #track class="range-slider__inner" draggable="false">
        <div
          class="range-slider__track {{
            activeSliderClass(leftPos$ | async, rightPos$ | async)
          }}"
          draggable="false"
        >
          <ng-container *ngFor="let tick of ticks; index as i">
            <div
              *ngIf="tick.hideTick !== true"
              class="range-slider__tick"
              [style.left]="tickPos(tick.value)"
              draggable="false"
            >
              <label
                draggable="false"
                class="{{
                  'range-slider__tick-label range-slider__tick-label-' + i
                }}"
                >{{ tick.label }}</label
              >
            </div>
          </ng-container>
        </div>
        <div
          #leftThumb
          draggable="false"
          class="range-slider__thumb range-slider__left-thumb {{
            activeClass(leftPos$ | async)
          }}"
          [style.left]="cssCoo(leftPos$ | async, initialThumbs.left)"
        ></div>
        <div
          #rightThumb
          draggable="false"
          class="range-slider__thumb range-slider__right-thumb {{
            activeClass(rightPos$ | async)
          }}"
          [style.left]="cssCoo(rightPos$ | async, initialThumbs.right)"
        ></div>
      </div>
      <div class="range-slider__top-count" draggable="false">
        <span class="range-slider__hits" draggable="false">
          {{
            "map-page.search-result-count"
              | translate : { count: resultFormatter(searchCount$ | async) }
          }}</span
        >
      </div>
      <div class="range-slider__top-do-search" draggable="false">
        <button
          class="range-slider__do-search"
          draggable="false"
          (click)="fireDoSearchEvent()"
        >
          {{ "map-page.search-in-this-area" | translate }}
        </button>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class RangeSliderComponent implements AfterViewInit, OnDestroy {
  @Input() ticks: Tick[];
  @Input() initialThumbs: ThumbData;
  @Output() leftThumb: EventEmitter<number>;
  @Output() rightThumb: EventEmitter<number>;
  @Output() doSearch: EventEmitter<void>;

  @ViewChild("leftThumb") leftThumbTag: ElementRef<HTMLDivElement>;
  @ViewChild("rightThumb") rightThumbTag: ElementRef<HTMLDivElement>;
  @ViewChild("track") trackRef: ElementRef<HTMLDivElement>;
  @ViewChild("rangeSliderArea") sliderRef: ElementRef<HTMLDivElement>;

  protected downX: number | null;
  protected pos$: BehaviorSubject<number | undefined> | null;
  protected leftPos$: BehaviorSubject<number | undefined>;
  protected rightPos$: BehaviorSubject<number | undefined>;
  protected moveConstraint: ((newPos: number) => boolean) | null;
  protected emitter: EventEmitter<number>;

  protected searchCount$: Observable<number>;

  constructor(
    protected translate: TranslateService,
    protected searchStore: Store<SearchStateInterface>
  ) {
    this.downX = null;
    this.leftPos$ = new BehaviorSubject(undefined);
    this.rightPos$ = new BehaviorSubject(undefined);
    this.leftThumb = new EventEmitter<number>();
    this.rightThumb = new EventEmitter<number>();
    this.doSearch = new EventEmitter<void>();

    this.searchCount$ = searchStore.pipe(select(getTotalResultsCount));

    this.stopMove = this.stopMove.bind(this);
    this.moveMouse = this.moveMouse.bind(this);
    this.startMove = this.startMove.bind(this);
  }

  fireDoSearchEvent() {
    this.doSearch.emit();
  }

  resultFormatter(n: number): string {
    return new Intl.NumberFormat(this.translate.currentLang, {
      style: "decimal",
    }).format(n);
  }

  ngOnDestroy(): void {
    const track = this.trackRef.nativeElement;
    track.removeEventListener("mouseup", this.stopMove);
    track.removeEventListener("mouseleave", this.stopMove);
    track.removeEventListener("touchend", this.stopMove);
    track.removeEventListener("touchcancel", this.stopMove);
    track.removeEventListener("mousemove", this.moveMouse);
    track.removeEventListener("touchmove", this.moveMouse);
    track.removeEventListener("mousedown", this.startMove);
    track.removeEventListener("touchstart", this.startMove);
  }

  ngAfterViewInit(): void {
    const options = { capture: true };

    const track = this.trackRef.nativeElement;
    track.addEventListener("mouseup", this.stopMove, options);
    track.addEventListener("mouseleave", this.stopMove, options);
    track.addEventListener("touchend", this.stopMove, options);
    track.addEventListener("touchcancel", this.stopMove, options);
    track.addEventListener("mousemove", this.moveMouse, options);
    track.addEventListener("touchmove", this.moveMouse, options);
    track.addEventListener("mousedown", this.startMove, options);
    track.addEventListener("touchstart", this.startMove, options);
  }

  protected clientX($event): number | null {
    return $event.clientX ?? $event.touches[0]?.clientX ?? null;
  }

  protected leftDown() {
    this.pos$ = this.leftPos$;
    let rightPos: number | undefined;
    this.rightPos$.pipe(take(1)).subscribe((v) => {
      rightPos = v;
    });
    if (rightPos === undefined) {
      rightPos = 100 * this.valueToPercent(this.initialThumbs.right);
    }
    this.moveConstraint = (newPos: number): boolean => newPos <= rightPos;
    this.emitter = this.leftThumb;
  }

  protected rightDown() {
    this.pos$ = this.rightPos$;
    let leftPos: number | undefined;
    this.leftPos$.pipe(take(1)).subscribe((v) => {
      leftPos = v;
    });
    if (leftPos === undefined) {
      leftPos = 100 * this.valueToPercent(this.initialThumbs.left);
    }
    this.moveConstraint = (newPos: number): boolean => newPos >= leftPos;
    this.emitter = this.rightThumb;
  }

  cssCoo(x: number | undefined, initialYear: number): string {
    x = x ?? 100 * this.valueToPercent(initialYear);
    return `calc(${x}% - ${rangeSliderThumbWidth / 2}px)`;
  }

  activeClass(pos: number | undefined): string {
    if (pos === undefined) {
      return "range-slider__thumb--inactive";
    }
    return "range-slider__thumb--active";
  }

  activeSliderClass(
    left: number | undefined,
    right: number | undefined
  ): string {
    if (left === undefined && right === undefined) {
      return "range-slider__track--inactive";
    }
    return "range-slider__track--active";
  }

  stopMove($event) {
    if (this.downX != null) {
      const target = this.getTarget($event);
      // mouseleave on left or right thumb -> still inside slide
      // so don't stop moving
      if ($event.type === "mouseleave" && target !== "slider") {
        return false;
      }
      this.downX = null;
      this.pos$ = null;
      this.moveConstraint = null;
    }
    return false;
  }

  protected getTarget($event): "left-thumb" | "right-thumb" | "slider" {
    const targetCss = $event.target.classList ?? [];
    if (targetCss.contains("range-slider__left-thumb")) {
      return "left-thumb";
    }
    if (targetCss.contains("range-slider__right-thumb")) {
      return "right-thumb";
    }
    return "slider";
  }

  startMove($event) {
    $event.stopPropagation();
    $event.preventDefault();
    const target = this.getTarget($event);
    if (target !== "slider") {
      this.downX = this.clientX($event);
      if (this.downX !== null) {
        if (target === "left-thumb") {
          this.leftDown();
        } else if (target === "right-thumb") {
          this.rightDown();
        }
      }
    }
    return false;
  }

  moveMouse($event) {
    $event.stopPropagation();
    $event.preventDefault();
    if (this.downX != null) {
      const track = this.trackRef.nativeElement;
      const trackBBox = track.getBoundingClientRect();
      const newPos = (this.clientX($event) - trackBBox.left) / trackBBox.width;
      const newPosPercent = 100 * newPos;
      if (
        newPosPercent >= 0 &&
        newPosPercent <= 100 &&
        this.moveConstraint(newPosPercent)
      ) {
        this.pos$.next(newPosPercent);
        const totalValueRange =
          this.ticks[this.ticks.length - 1].value - this.ticks[0].value;
        const currentValue = Math.floor(
          newPos * totalValueRange + this.ticks[0].value
        );
        this.emitter.emit(currentValue);
      }
    }
    return false;
  }

  /**
   * Converts tick number to number 0..1.
   *
   * @param v Number in the range of "ticks"
   * @returns
   */
  protected valueToPercent(v: number): number {
    const valueRange =
      this.ticks[this.ticks.length - 1].value - this.ticks[0].value;
    return (v - this.ticks[0].value) / valueRange;
  }

  tickPos(tickValue: number): string {
    const valueScale = this.valueToPercent(tickValue);
    return `calc(${100 * valueScale}% - ${rangeSliderTickWidth / 2}px)`;
  }

  protected setThumb(
    thumb: BehaviorSubject<number | undefined>,
    v: number | undefined
  ) {
    if (v !== undefined) {
      v = 100 * this.valueToPercent(v);
    }
    thumb.next(v);
  }

  /**
   * Set the position of the left thumb.
   *
   * @param v Number in the range of "ticks"
   */
  setLeftThumb(v: number | undefined) {
    this.setThumb(this.leftPos$, v);
  }

  /**
   * Set the position of the right thumb.
   *
   * @param v Number in the range of "ticks"
   */
  setRightThumb(v: number | undefined) {
    this.setThumb(this.rightPos$, v);
  }
}
