import { Exhibition } from "@app/exhibitions/actions/exhibitions.actions";

export interface ExhibitionData {
  exhibitions: Exhibition[];
}
