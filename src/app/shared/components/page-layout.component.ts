import { BreakpointObserver } from "@angular/cdk/layout";
import {
  Location,
  LocationStrategy,
  PathLocationStrategy,
} from "@angular/common";
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  Input,
  OnDestroy,
} from "@angular/core";
import { FacetDisplayHelper } from "@app/shared/components/FacetDisplayHelper";
import { environment } from "@env/environment";
import { SettingsModel } from "@ub-unibas/rdv_viewer-lib/components";
import { Observable } from "rxjs";

@Component({
  selector: "app-page-layout",
  template: `
    <div class="page-layout {{ baseClass }}">
      <div class="page-layout__logo">
        <div class="page-layout__logo-inner">
          <app-logo></app-logo>
          <div
            *ngIf="(smallFacet$ | async) === true"
            class="flex-grow-1 text-end"
          >
            <app-main-navi></app-main-navi>
          </div>
        </div>
      </div>
      <div *ngIf="(smallFacet$ | async) === true && showSearchAndFilter">
        <app-search-filter
          class="search-filter search-filter--small"
          [initiallyClosed]="true"
          [facetDisplayHelper]="facetDisplayHelper"
          [forceToggled]="false"
        ></app-search-filter>
      </div>
      <div class="page-layout__hits-outer">
        <ng-content></ng-content>
        <app-custom-footer class="footer-small"></app-custom-footer>
      </div>
      <div class="page-layout__facets">
        <app-main-navi></app-main-navi>
        <div *ngIf="(smallFacet$ | async) !== true && showSearchAndFilter">
          <app-search-filter
            class="search-filter search-filter--big"
            [initiallyClosed]="facetsInitiallyClosed"
            [facetDisplayHelper]="facetDisplayHelper"
            [forceToggled]="false"
          ></app-search-filter>
        </div>
      </div>
      <app-custom-footer class="footer-big"></app-custom-footer>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class PageLayoutComponent implements AfterViewInit, OnDestroy {
  @Input() baseClass: string;
  @Input() facetsInitiallyClosed: boolean;
  @Input() showSearchAndFilter: boolean;

  protected facetDisplayHelper: FacetDisplayHelper;

  constructor(protected breakpointObserver: BreakpointObserver) {
    this.facetDisplayHelper = new FacetDisplayHelper(true);
  }

  toggleFacets() {
    this.facetDisplayHelper.toggleFacets();
  }

  ngAfterViewInit() {
    this.facetDisplayHelper.init(this.breakpointObserver);
  }

  ngOnDestroy(): void {
    this.facetDisplayHelper.close();
  }

  get env(): SettingsModel {
    return environment;
  }

  get showFacets$(): Observable<boolean> {
    return this.facetDisplayHelper.getShowFacets$();
  }

  get smallFacet$(): Observable<boolean> {
    return this.facetDisplayHelper.getSmallFacet$();
  }
}
