import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  Input,
} from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { BehaviorSubject, Observable } from "rxjs";
import { FacetDisplayHelper } from "./FacetDisplayHelper";
import {
  FacetFieldModel,
  FacetFieldType,
  SearchStateInterface,
  getTotalResultsCount,
} from "@ub-unibas/rdv_viewer-lib/components";
import { environment } from "@env/environment";
import { Store, select } from "@ngrx/store";

type FacetFieldModelWithKey = { key: string } & FacetFieldModel;

@Component({
  selector: "app-search-filter",
  template: `
    <app-collapsible
      [title]="'collapsible-facets.filter' | translate"
      [open]="openFacets$ | async"
      [key]="'facets-search-collapsible'"
      (changed)="toggleSearchFilter($event)"
      buttonClass="collapsible-facets__title"
      [closeOnBlur]="false"
    >
      <app-custom-simple-search
        [showBoolFacets]="false"
      ></app-custom-simple-search>
      <div class="search-filter__top-spacer"></div>
      <div class="facet-search-collapsible__scrollable-content">
        <div>
          <h4 class="search-results__title">
            {{
              "search-results.title"
                | translate : { value: resultFormatter(searchCount$ | async) }
            }}
          </h4>
        </div>
        <app-selected-facets></app-selected-facets>
        <ng-container
          *ngFor="let bf of getAllBooleanFacets(); let index = index"
        >
          <div class="form-control simple-search__filter">
            <app-checkbox-facet
              [key]="bf.key"
              [visible]="true"
              [label]="bf.label | translate"
            ></app-checkbox-facet>
          </div>
        </ng-container>
        <app-collapsible-facets
          (closeFacetEmitter)="toggleFacets()"
          [forceToggled]="forceToggled"
          [showFacets]="showFacets$ | async"
        ></app-collapsible-facets>
      </div>
      <div class="search-filter__bottom-spacer"></div>
    </app-collapsible>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class SearchFilterComponent implements AfterViewInit {
  @Input() forceToggled: boolean;
  @Input() facetDisplayHelper: FacetDisplayHelper;
  @Input() initiallyClosed = false;

  protected searchCount$: Observable<number>;
  protected openFacets$: BehaviorSubject<boolean>;

  constructor(
    protected translate: TranslateService,
    protected searchStore: Store<SearchStateInterface>
  ) {
    this.openFacets$ = new BehaviorSubject<boolean>(false);
    this.searchCount$ = searchStore.pipe(select(getTotalResultsCount));
  }

  resultFormatter(n: number): string {
    return new Intl.NumberFormat(this.translate.currentLang, {
      style: "decimal",
    }).format(n);
  }

  getAllBooleanFacets(): FacetFieldModelWithKey[] {
    const facetConfig = environment.facetFields;
    const boolFacets: FacetFieldModelWithKey[] = [];
    for (const key of Object.keys(facetConfig)) {
      const bf = facetConfig[key];
      if (bf.facetType === FacetFieldType.BOOLEAN && !bf.hidden) {
        boolFacets.push({ ...bf, key });
      }
    }
    return boolFacets;
  }

  ngAfterViewInit(): void {
    this.toggleSearchFilter(!this.initiallyClosed);
  }

  toggleSearchFilter(open: boolean) {
    this.openFacets$.next(open);
  }

  toggleFacets() {
    this.facetDisplayHelper.toggleFacets();
  }

  get showFacets$(): Observable<boolean> {
    return this.facetDisplayHelper.getShowFacets$();
  }
}
