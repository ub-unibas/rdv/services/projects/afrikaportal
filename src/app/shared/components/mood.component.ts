import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import { ChangeDetectionStrategy, Component, Input } from "@angular/core";

export interface MoodInfo {
  forWidth: number;
  imgUrl: string;
}

@Component({
  selector: "app-mood",
  template: `
    <picture class="mood">
      <ng-container *ngFor="let mood of moodInfo; let last = last">
        <source [media]="mediaQuery(mood, last)" [srcset]="mood.imgUrl" />
      </ng-container>
      <img src="{{ lastMoodUrl(moodInfo) }}" alt="" />
    </picture>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class MoodComponent {
  @Input() moodInfo: MoodInfo[];

  mediaQuery(mood: MoodInfo, isLast: boolean): string {
    return isLast
      ? `(min-width: ${mood.forWidth}px)`
      : `(max-width: ${mood.forWidth}px)`;
  }

  lastMoodUrl(moods: MoodInfo[]): string {
    return moods[moods.length - 1].imgUrl;
  }
}
