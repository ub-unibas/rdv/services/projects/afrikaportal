import { ChangeDetectionStrategy, Component } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-custom-footer",
  template: `
    <footer>
      <a
        class="link-black text-decoration-none"
        [title]="'footer.link_data_protection' | translate"
        [routerLink]="['/simple/privacy-policy' | localize]"
      >
        {{ "footer.link_data_protection" | translate }}
      </a>
      <a
        class="link-black text-decoration-none"
        [title]="'footer.link_imprint' | translate"
        [routerLink]="['/simple/imprint' | localize]"
      >
        {{ "footer.link_imprint" | translate }}
      </a>
      <a
        class="link-black text-decoration-none"
        [title]="'footer.link_contact' | translate"
        [routerLink]="['/simple/contact' | localize]"
      >
        {{ "footer.link_contact" | translate }}
      </a>
      <div class="footer--last-link">
        <a
          class="link-black text-decoration-none"
          href="https://ub.unibas.ch/{{ translate.currentLang }}/"
          target="_blank"
        >
          {{ "footer.link_ub-basel" | translate }}
        </a>
      </div>
    </footer>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ParcFooterComponent {
  constructor(protected translate: TranslateService) {}
}
