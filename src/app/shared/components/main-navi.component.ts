import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import { ChangeDetectionStrategy, Component } from "@angular/core";
import { ActivatedRoute, NavigationExtras } from "@angular/router";
import { LocalizeRouterService } from "@gilsdav/ngx-translate-router";
import { TranslateService } from "@ngx-translate/core";
import { BehaviorSubject } from "rxjs";
import packageJson from "../../../../package.json";

@Component({
  selector: "app-main-navi",
  template: `
    <div class="main-navi">
      <a
        class="main-navi__button main-navi__button--map"
        [routerLink]="['/map' | localize]"
        [title]="'main-navi.to_map' | translate"
      >
        {{ "main-navi.to_map" | translate }}
      </a>
      <a
        class="main-navi__button main-navi__button--search"
        [routerLink]="['/search' | localize]"
        [title]="'main-navi.to_search' | translate"
      >
        {{ "main-navi.to_search" | translate }}
      </a>
      <app-collapsible
        class="main-navi__button main-navi__button--lang"
        [title]="'main-navi.lang' | translate"
        [open]="openLang$ | async"
        [key]="'main-navi-lang-collapsible'"
        buttonClass="main-navi__lang-title"
        (changed)="toggleLang($event)"
      >
        <div>
          <button
            class="btn main-navi__lang-button"
            type="button"
            (click)="changeLanguage('de')"
            [disabled]="langButtonDisabled('de')"
          >
            {{ "main-navi.lang_de" | translate }}
          </button>
        </div>
        <div>
          <button
            class="btn main-navi__lang-button"
            type="button"
            (click)="changeLanguage('en')"
            [disabled]="langButtonDisabled('en')"
          >
            {{ "main-navi.lang_en" | translate }}
          </button>
        </div>
      </app-collapsible>
      <app-collapsible
        class="main-navi__button main-navi__button--menu"
        [title]="'main-navi.menu' | translate"
        [open]="openMenu$ | async"
        [key]="'main-navi-menu-collapsible'"
        buttonClass="main-navi__menu-title"
        (changed)="toggleMenu($event)"
      >
        <div>
          <a
            [routerLink]="['/home' | localize]"
            [title]="'main-navi.to_home' | translate"
          >
            {{ "main-navi.to_home" | translate }}
          </a>
        </div>
        <div>
          <a
            [routerLink]="['/map' | localize]"
            [title]="'main-navi.to_map' | translate"
          >
            {{ "main-navi.to_map" | translate }}
          </a>
        </div>
        <div>
          <a
            [routerLink]="['/search' | localize]"
            [title]="'main-navi.to_search' | translate"
          >
            {{ "main-navi.to_search" | translate }}
          </a>
        </div>
        <div>
          <a
            [routerLink]="['/simple/about' | localize]"
            [title]="'main-navi.to_about' | translate"
          >
            {{ "main-navi.to_about" | translate }}
          </a>
        </div>
        <div>
          <a
            [routerLink]="['/simple/exhibition' | localize]"
            [title]="'main-navi.to_exhibition' | translate"
          >
            {{ "main-navi.to_exhibition" | translate }}
          </a>
        </div>
        <div>
          <a
            [routerLink]="['/simple/ethical-guidelines' | localize]"
            [title]="'main-navi.to_ethical_guidelines' | translate"
          >
            {{ "main-navi.to_ethical_guidelines" | translate }}
          </a>
        </div>
        <div>
          <a
            [routerLink]="['/simple/sponsors' | localize]"
            [title]="'main-navi.to_sponsors' | translate"
          >
            {{ "main-navi.to_sponsors" | translate }}
          </a>
        </div>
        <div>
          <a
            [routerLink]="['/simple/help' | localize]"
            [title]="'main-navi.to_help' | translate"
          >
            {{ "main-navi.to_help" | translate }}
          </a>
        </div>
        <div class="main-navi__app-version">Version: {{ appVersion }}</div>
      </app-collapsible>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class MainNaviComponent {
  protected openLang$ = new BehaviorSubject<boolean>(false);
  protected openMenu$ = new BehaviorSubject<boolean>(false);

  appVersion = packageJson.version;

  constructor(
    protected translate: TranslateService,
    protected localize: LocalizeRouterService,
    protected route: ActivatedRoute
  ) {}

  toggleLang(open: boolean) {
    this.openMenu$.next(false);
    this.openLang$.next(open);
  }

  toggleMenu(open: boolean) {
    this.openLang$.next(false);
    this.openMenu$.next(open);
  }

  changeLanguage(newLanguage: string) {
    const extras: NavigationExtras = {
      relativeTo: this.route,
      queryParamsHandling: "preserve",
    };
    // don't use useNavigateMethod=false, because this will merge query
    // params with "," instead of repeating them
    this.localize.changeLanguage(newLanguage, extras, true);
    // this.languageChanged(newLanguage);
    this.openLang$.next(false);
  }

  isCurrentLang(langCode: string): boolean {
    return this.translate.currentLang === langCode;
  }

  langButtonDisabled(langCode: string): string {
    if (this.isCurrentLang(langCode)) {
      return "disabled";
    }
    return "";
  }
}
