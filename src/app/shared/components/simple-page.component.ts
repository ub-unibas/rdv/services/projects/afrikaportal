import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  Input,
} from "@angular/core";
import { MoodInfo } from "@app/shared/components/mood.component";
import { LangChangeEvent, TranslateService } from "@ngx-translate/core";
import { BehaviorSubject, Subscription } from "rxjs";

@Component({
  selector: "app-simple-page",
  template: `
    <app-simple-page-layout
      baseClass="{{ baseClass }} simple-page"
      [moodInfo]="moodInfo"
      [moodComponent]="moodComponent"
    >
      <ng-container [ngSwitch]="language$ | async">
        <ng-container *ngSwitchCase="'de'">
          <ndc-dynamic
            [ndcDynamicComponent]="deComponent"
            [ndcDynamicInputs]="componentInputs"
          ></ndc-dynamic>
        </ng-container>
        <ng-container *ngSwitchCase="'en'">
          <ndc-dynamic
            [ndcDynamicComponent]="enComponent"
            [ndcDynamicInputs]="componentInputs"
          ></ndc-dynamic>
        </ng-container>
      </ng-container>
    </app-simple-page-layout>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class SimplePageComponent implements AfterViewInit {
  @Input() baseClass: string;
  @Input() moodInfo: MoodInfo[];
  @Input() moodComponent: any;
  @Input() deComponent: any;
  @Input() enComponent: any;
  @Input() componentInputs: any;

  language$: BehaviorSubject<string>;
  languageChangeSubscription: Subscription;

  constructor(protected translate: TranslateService) {
    this.language$ = new BehaviorSubject<string>(translate.currentLang);
    this.languageChangeSubscription = this.translate.onLangChange.subscribe(
      (params: LangChangeEvent) => this.language$.next(params.lang)
    );
  }

  ngAfterViewInit(): void {
    setTimeout(() => window.scrollTo(0, 0), 250);
  }
}
