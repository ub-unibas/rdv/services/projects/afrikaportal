import { ChangeDetectionStrategy, Component } from "@angular/core";
import {
  SimpleSearchComponent
} from "@ub-unibas/rdv_viewer-lib/components";

@Component({
  selector: "app-custom-simple-search",
  template: `
    <div class="simple-search">
      <div
        class="row g-0 simple-search__field-container"
        *ngFor="let field of searchFields$ | async | objectKeys"
      >
        <div class="col-10 position-relative">
          <app-simple-search-autocomplete
            #ngSelect
            field="{{ field }}"
            [initialValues]="getInitialValues()"
            (add)="selectedSearchWord($event, field)"
            (remove)="unselectSearchWord($event, field)"
            [typeahead]="searchTerm"
            [keyDownFn]="keydown"
          ></app-simple-search-autocomplete>
        </div>
        <div class="col-2">
          <button
            type="button"
            class="btn btn-red simple-search__search"
            (click)="updateSearchValue(field)"
          >
            {{ "simple-search.search" | translate }}
          </button>
        </div>
        <div class="d-flex justify-content-start align-items-start flex-wrap">
          <div class="form-control simple-search__filter">
            <input
              id="keepFilters"
              type="checkbox"
              [checked]="keepSearchFilters$ | async"
              (change)="changeKeepFiltersCheckbox($event)"
            />
            <label for="keepFilters">{{
              "simple-search.keep_filters" | translate
            }}</label>
          </div>
        </div>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomSimpleSearchComponent extends SimpleSearchComponent {}
