import { ChangeDetectionStrategy, Component } from "@angular/core";
import { environment } from "@env/environment";
import { TranslateService } from "@ngx-translate/core";
import { SearchHitComponent } from "@ub-unibas/rdv_viewer-lib/components";

const mediaTypeToIconMap = {
  Akte: "Document",
  Archivsammlung: "Dossier",
  Artikel: "Newsarticle",
  Bild: "Image",
  Brief: "Handschrift",
  Buch: "Print",
  Diverses: "Print",
  Dokument: "Document",
  Dossier: "Dossier",
  "Elektronische Publikation": "Electronic resource",
  "Elektronischer Datenträger": "Electronic resource",
  Ephemera: "Print",
  Film: "Film",
  Fotografie: "Image",
  Illustration: "Image",
  Kalender: "Print",
  Karte: "Map",
  Musikinstrument: "Object",
  Objekt: "Object",
  Philatelie: "Print",
  Plakat: "Image",
  Projekt: "Project",
  Schmuck: "Object",
  Skulptur: "Object",
  Spiel: "Object",
  Textil: "Object",
  Tonaufzeichnung: "Audio",
  Waffe: "Object",
  Zeitschrift: "Print",
  Zeitung: "Print",
};

@Component({
  selector: "app-parc-search-hit",
  template: `
    <ng-template #snippetTextTpl>
      <a
        *ngIf="!doc.fulltext && doc.fulltext_snippet"
        class="search-hit__title-link link-black text-decoration-none"
        [title]="'search-results.to_doc' | translate"
        [routerLink]="['/detail' | localize, doc.id]"
        [ngStyle]="clickable()"
        [state]="{ cursor: doc.search_after_values }"
      >
        <div class="field" [innerHTML]="doc.fulltext_snippet | safeHtml"></div>
      </a>
      <app-collapsible
        *ngIf="doc.fulltext"
        [open]="fullTextOpened$ | async"
        titleHtml="{{ doc.fulltext_snippet }}"
        (changed)="toggleFulltext($event)"
        buttonClass="field"
        containerClass="field"
      >
        <span [innerHTML]="doc.fulltext | safeHtml"></span>
      </app-collapsible>
      <ng-container *ngIf="doc.link">
        <div *ngFor="let l of doc.link">
          <a
            class="search-hit__external-link"
            href="{{ l.url }}"
            target="_blank"
          >
            <span *ngIf="!l.label">{{
              "search-results.external_link" | translate
            }}</span>
            <span *ngIf="l.label" [innerHTML]="l.label | safeHtml"></span>
          </a>
        </div>
      </ng-container>
    </ng-template>
    <div class="search-hit row g-0">
      <div
        class="search-hit__basket text-start text-sm-center mb-2 mb-sm-0 col-12 col-sm-1"
      >
        <app-basket-icon
          *ngIf="env?.showExportList?.basket === true"
          [basketElement]="doc.id"
        ></app-basket-icon>
      </div>
      <hr class="search-hit__separator" />
      <div class="search-hit__info col row g-0">
        <div
          *ngIf="env.showImagePreview !== false"
          class="col-3 search-hit__info-preview"
        >
          <a
            class="search-hit__info-preview-container"
            [title]="'search-results.to_doc' | translate"
            [routerLink]="['/detail' | localize, doc.id]"
            [ngStyle]="clickable()"
            [state]="{ cursor: doc.search_after_values }"
          >
            <img
              [src]="buildPreviewUrls(doc.preview_image, doc.fct_mediatype)[0]"
              [appBrokenImage]="
                buildPreviewUrls(doc.preview_image, doc.fct_mediatype)
              "
            />
          </a>
        </div>
        <ng-container *ngIf="env.showImagePreview !== false">
          <div [ngStyle]="{ 'flex-shrink': 0, width: '1.3rem' }"></div>
        </ng-container>
        <div
          class="search-hit__info-text"
          [class.col-8]="env.showImagePreview !== false"
        >
          <ng-container *ngIf="showHitObjectType()">
            <div class="search-hit__type">
              <span *ngIf="showHitNr()">{{ doc.hitNr }}. </span>
              <app-search-hit-value
                [fieldValue]="getObjectType()"
              ></app-search-hit-value>
            </div>
          </ng-container>
          <ng-container *ngIf="!empty(doc.title)">
            <a
              class="search-hit__title-link link-black"
              [title]="'search-results.to_doc' | translate"
              [ngStyle]="clickable()"
              [routerLink]="['/detail' | localize, doc.id]"
              [state]="{ cursor: doc.search_after_values }"
            >
              <h3 class="search-hit__title">
                <span *ngIf="showsHitNrInTitle()">{{ doc.hitNr }}. </span>
                <app-search-hit-value
                  [fieldValue]="doc.title"
                ></app-search-hit-value>
              </h3>
            </a>
          </ng-container>
          <div class="d-none d-sm-block d-md-block d-lg-block d-xl-block">
            <ng-container *ngIf="!empty(getLine1())">
              <h4 class="search-hit__line_1">
                <app-search-hit-value
                  [fieldValue]="getLine1()"
                ></app-search-hit-value>
              </h4>
            </ng-container>
            <ng-container [ngTemplateOutlet]="snippetTextTpl"></ng-container>
          </div>
        </div>
        <div class="d-block d-sm-none d-md-none d-lg-none d-xl-none">
          <ng-container *ngIf="!empty(getLine1())">
            <h4 class="search-hit__line_1">
              <app-search-hit-value
                [fieldValue]="getLine1()"
              ></app-search-hit-value>
            </h4>
          </ng-container>
          <ng-container [ngTemplateOutlet]="snippetTextTpl"></ng-container>
        </div>
      </div>
      <hr class="search-hit__separator" *ngIf="last" />
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ParcSearchHitComponent extends SearchHitComponent {
  constructor(protected translate: TranslateService) {
    super(translate);
  }

  /**
   * @override
   *
   * @param previewImage
   * @param objectType
   * @returns
   */
  buildPreviewUrls(previewImage, mediaType): string[] {
    const list = [];
    if (previewImage) {
      if (environment.scaleDocThumbsByIiif !== false) {
        previewImage += "/full/120,/0/default.jpg";
      }
      list.push(previewImage);
    }
    if (mediaTypeToIconMap[mediaType]) {
      list.push(this.objectTypeImageUrl(mediaTypeToIconMap[mediaType]));
    }
    list.push("/assets/img/icon-default.png");
    return list;
  }
}
