import { BreakpointObserver } from "@angular/cdk/layout";
import {
  Location,
  LocationStrategy,
  PathLocationStrategy,
} from "@angular/common";
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  Input,
  OnDestroy,
} from "@angular/core";
import { FacetDisplayHelper } from "@app/shared/components/FacetDisplayHelper";
import { MoodInfo } from "@app/shared/components/mood.component";
import { Observable } from "rxjs";

@Component({
  selector: "app-simple-page-layout",
  template: `
    <div class="simple-page-layout {{ baseClass }}">
      <ng-container *ngIf="!!moodInfo">
        <div class="mood-container">
          <div class="mood-img">
            <app-mood [moodInfo]="moodInfo"></app-mood>
          </div>
        </div>
      </ng-container>
      <ng-container *ngIf="!!moodComponent">
        <ndc-dynamic [ndcDynamicComponent]="moodComponent"></ndc-dynamic>
      </ng-container>
      <div class="simple-page-layout__top">
        <div class="simple-page-layout__logo">
          <app-logo></app-logo>
        </div>
        <app-main-navi></app-main-navi>
      </div>
      <div class="simple-page-layout__content">
        <ng-content></ng-content>
      </div>
      <app-custom-footer class="footer-big"></app-custom-footer>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class SimplePageLayoutComponent implements AfterViewInit, OnDestroy {
  @Input() baseClass: string;
  @Input() moodInfo: MoodInfo[];
  @Input() moodComponent: any;

  protected facetDisplayHelper: FacetDisplayHelper;

  constructor(protected breakpointObserver: BreakpointObserver) {
    this.facetDisplayHelper = new FacetDisplayHelper(false);
  }

  ngAfterViewInit() {
    this.facetDisplayHelper.init(this.breakpointObserver);
  }

  ngOnDestroy(): void {
    this.facetDisplayHelper.close();
  }

  get showSmallLayout$(): Observable<boolean> {
    return this.facetDisplayHelper.getSmallFacet$();
  }
}
