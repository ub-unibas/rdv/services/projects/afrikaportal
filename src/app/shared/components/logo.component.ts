import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import { ChangeDetectionStrategy, Component, Input } from "@angular/core";

@Component({
  selector: "app-logo",
  template: `
    <a
      [routerLink]="['/home' | localize]"
      [title]="'main-navi.to_home' | translate"
    >
      <picture>
        <source
          media="(max-width: 768px)"
          srcset="/assets/img/Logo-small.svg"
        />
        <source media="(min-width: 769px)" srcset="/assets/img/Logo.svg" />
        <img
          class="logo {{ imgClass }}"
          src="/assets/img/Logo.svg"
          alt="Logo"
        />
      </picture>
    </a>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class LogoComponent {
  @Input() imgClass = "";
}
