import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import { ChangeDetectionStrategy, Component } from "@angular/core";

@Component({
  selector: "app-sponsors",
  template: `
    <div class="sponsors">
      <div class="sponsors__goehner">
        <a
          target="_blank"
          href="{{ 'sponsors.page.goehner.url' | translate }}"
          title="{{ 'sponsors.page.goehner.title' | translate }}"
        >
          <img src="/assets/img/logos/Go%CC%88hner%20Logo.png" />
        </a>
      </div>
      <div class="sponsors__schlettwein">
        <a
          target="_blank"
          href="{{ 'sponsors.page.schlettwein.url' | translate }}"
          title="{{ 'sponsors.page.schlettwein.title' | translate }}"
        >
          <img src="/assets/img/logos/CarlSchlettweinStiftung.png" />
        </a>
      </div>
      <div class="sponsors__fod">
        <a
          target="_blank"
          href="{{ 'sponsors.page.fod.url' | translate }}"
          title="{{ 'sponsors.page.fod.title' | translate }}"
        >
          <img src="/assets/img/logos/FOD%20Logo.png" />
        </a>
      </div>
      <div class="sponsors__fag">
        <a
          target="_blank"
          href="{{ 'sponsors.page.fag.url' | translate }}"
          title="{{ 'sponsors.page.fag.title' | translate }}"
        >
          <img src="/assets/img/logos/FAG%20Logo.png" />
        </a>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class SponsorsComponent {}
