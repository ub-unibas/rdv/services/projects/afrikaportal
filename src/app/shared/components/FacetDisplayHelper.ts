import { BreakpointObserver, BreakpointState } from "@angular/cdk/layout";
import {
  BehaviorSubject,
  Observable,
  Subject,
  Subscription,
  debounceTime,
  take,
} from "rxjs";

const BP_SMALL = "(max-width: 768px)";
const BP_NORMAL = "(min-width: 769px)";

export class FacetDisplayHelper {
  protected showFacetsSubject: Subject<boolean>;
  protected showFacets$: Observable<boolean>;
  protected smallFacetSubject: Subject<boolean>;
  protected smallFacet$: Observable<boolean>;
  protected breakPointSubscription: Subscription;
  protected showFacetsOnTablet: boolean;

  constructor(showFacetsOnTablet: boolean) {
    this.showFacetsOnTablet = showFacetsOnTablet;
    this.showFacetsSubject = new BehaviorSubject<boolean>(true);
    this.showFacets$ = this.showFacetsSubject.asObservable();
    this.smallFacetSubject = new BehaviorSubject<boolean>(false);
    this.smallFacet$ = this.smallFacetSubject.asObservable();
  }

  init(breakpointObserver: BreakpointObserver) {
    this.breakPointSubscription = breakpointObserver
      .observe([BP_SMALL, BP_NORMAL])
      .pipe(debounceTime(250))
      .subscribe((result) => this.adaptUi(result));
  }

  adaptUi(result: BreakpointState) {
    if (result.matches) {
      if (result.breakpoints[BP_SMALL]) {
        this.smallFacetSubject.next(true);
        this.showFacetsSubject.next(this.showFacetsOnTablet);
      }
      if (result.breakpoints[BP_NORMAL]) {
        this.smallFacetSubject.next(false);
        this.showFacetsSubject.next(true);
      }
    }
  }

  close() {
    if (this.breakPointSubscription) {
      this.breakPointSubscription.unsubscribe();
    }
  }

  getSmallFacet$(): Observable<boolean> {
    return this.smallFacet$;
  }

  getShowFacets$(): Observable<boolean> {
    return this.showFacets$;
  }

  toggleFacets() {
    let val;
    this.showFacetsSubject.pipe(take(1)).subscribe((v) => (val = v));
    this.showFacetsSubject.next(!val);
  }

  showFacets(flag: boolean) {
    this.showFacetsSubject.next(flag);
  }
}
