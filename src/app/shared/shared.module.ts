import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LocalizeRouterModule } from "@gilsdav/ngx-translate-router";
import { TranslateModule } from "@ngx-translate/core";
import {
  PipesModule,
  SearchModule,
} from "@ub-unibas/rdv_viewer-lib/components";
import { DynamicModule } from "ng-dynamic-component";
import { ToastrModule } from "ngx-toastr";
import { CustomSimpleSearchComponent } from "./components/custom-simple-search.component";
import { ExhibitionPreviewComponent } from "./components/exhibition-preview.component";
import { LogoComponent } from "./components/logo.component";
import { MainNaviComponent } from "./components/main-navi.component";
import { MoodComponent } from "./components/mood.component";
import { PageLayoutComponent } from "./components/page-layout.component";
import { ParcFooterComponent } from "./components/parc-footer.component";
import { RangeSliderComponent } from "./components/range-slider.component";
import { SearchFilterComponent } from "./components/search-filter.component";
import { SimplePageLayoutComponent } from "./components/simple-page-layout.component";
import { SimplePageComponent } from "./components/simple-page.component";
import { SponsorsComponent } from "./components/sponsors.component";
import { ExhibitionService } from "./services/ExhibitionService";
import { ParcSearchHitComponent } from "./components/parc-search-hit.component";

const SHARED_COMPONENTS = [
  MainNaviComponent,
  SearchFilterComponent,
  RangeSliderComponent,
  CustomSimpleSearchComponent,
  PageLayoutComponent,
  ParcFooterComponent,
  MoodComponent,
  LogoComponent,
  SimplePageLayoutComponent,
  SimplePageComponent,
  MoodComponent,
  ExhibitionPreviewComponent,
  SponsorsComponent,
  ParcSearchHitComponent,
];

@NgModule({
  imports: [
    CommonModule,
    PipesModule,
    RouterModule.forChild([]),
    TranslateModule.forChild(),
    LocalizeRouterModule.forChild([]),
    FormsModule,
    ToastrModule,
    SearchModule,
    DynamicModule,
  ],
  declarations: SHARED_COMPONENTS,
  providers: [ExhibitionService],
  exports: SHARED_COMPONENTS,
})
export class SharedModule {}
