import { Injectable } from "@angular/core";
import { LoadExhibitions } from "@app/exhibitions/actions/exhibitions.actions";
import { State as ExhibitionState } from "@app/exhibitions/reducers/exhibitions.reducer";
import { Store } from "@ngrx/store";
import { TranslateService } from "@ngx-translate/core";
import { SearchStateInterface } from "@ub-unibas/rdv_viewer-lib/components";
import { ExhibitionData } from "../components/ExhibitionData";

@Injectable()
export class ExhibitionService {
  constructor(
    protected translate: TranslateService,
    protected searchStore: Store<SearchStateInterface>
  ) {}

  getLocalizedExhibitions(exhibitions: ExhibitionState): ExhibitionData {
    const exhibitionsForLang = exhibitions[this.translate.currentLang] ?? {};
    if (Object.keys(exhibitionsForLang).length === 0) {
      this.searchStore.dispatch(
        new LoadExhibitions({ language: this.translate.currentLang })
      );
    }
    return { exhibitions: exhibitionsForLang };
  }
}
