import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "@app/shared/shared.module";
import { LocalizeRouterModule } from "@gilsdav/ngx-translate-router";
import { TranslateModule } from "@ngx-translate/core";
import {
  PipesModule,
  SearchModule,
} from "@ub-unibas/rdv_viewer-lib/components";
import { ToastrModule } from "ngx-toastr";
import { SearchPageComponent } from "./components/search-page.component";

const routes: Routes = [{ path: "", component: SearchPageComponent }];

const SEARCH_COMPONENTS = [SearchPageComponent];

@NgModule({
  imports: [
    CommonModule,
    PipesModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild(),
    LocalizeRouterModule.forChild(routes),
    FormsModule,
    ToastrModule,
    SearchModule,
    SharedModule,
  ],
  declarations: SEARCH_COMPONENTS,
  providers: [],
  exports: SEARCH_COMPONENTS,
})
export class AfricaPortalSearchModule {}
