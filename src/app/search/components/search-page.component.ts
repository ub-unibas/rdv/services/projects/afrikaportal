import { BreakpointObserver } from "@angular/cdk/layout";
import {
  Location,
  LocationStrategy,
  PathLocationStrategy,
} from "@angular/common";
import { ChangeDetectionStrategy, Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { environment } from "@env/environment";
import { Store, select } from "@ngrx/store";
import { TranslateService } from "@ngx-translate/core";
import {
  BackendSearchService,
  NewSearchComponent,
  PreviousRouteServiceProvider,
  SearchStateInterface,
  SetOffset,
  SetSortField,
  SetSortOrder,
  SettingsModel,
  SimpleSearch,
  SortFieldsModel,
  UrlParamsService,
  UserConfigState,
  getAllResults,
  getResultOffset,
  getResultRows,
  getResultSortField,
  getResultSortOrder,
  getTotalResultsCount,
} from "@ub-unibas/rdv_viewer-lib/components";
import { Observable, Subscription, combineLatest } from "rxjs";

@Component({
  selector: "app-custom-search",
  template: `
    <app-page-layout
      baseClass="page-search"
      [facetsInitiallyClosed]="false"
      [showSearchAndFilter]="true"
    >
      <div>
        <h4 class="search-results__title">
          {{
            "search-results.title"
              | translate : { value: resultFormatter(searchCount$ | async) }
          }}
        </h4>
      </div>
      <div class="page-search__hits">
        <div
          class="mb-2 mb-sm-0 col-12 ps-lg-0 pe-0 justify-content-start"
          [class.search-results-paging-container]="(searchCount$ | async) > 0"
        >
          <div class="styled-select inline-block">
            <select
              class="mb-3"
              [title]="'search-results.sort.title' | translate"
              [ngModel]="sorting"
              name="sorting"
              (ngModelChange)="switchSorting($event)"
            >
              <option
                *ngFor="let sortOption of sortFields"
                [ngValue]="sortOption"
              >
                {{ sortOption.display | translate }}
              </option>
            </select>
          </div>
          <ng-container *ngIf="(searchCount$ | async) > 0">
            <div class="d-inline-block flex-grow-1">
              <app-search-results-paging
                [currentOffset]="offset$ | async"
                [numberOfRows]="searchCount$ | async"
                [rowsPerPage]="rowsPerPage$ | async"
                [pageButtonNr]="pageButtonNr()"
                (offset)="setSearchOffset($event)"
              >
              </app-search-results-paging>
            </div>
          </ng-container>
        </div>
        <div
          class="search-results__hit"
          *ngFor="let doc of docs$ | async; index as i; let last = last"
        >
          <app-parc-search-hit [doc]="doc" [last]="last"></app-parc-search-hit>
        </div>
      </div>
    </app-page-layout>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class SearchPageComponent extends NewSearchComponent {
  readonly sortFields = environment.sortFields;
  docs$: Observable<any>;
  searchCount$: Observable<number>;
  sort$: Observable<any>;
  offset$: Observable<number>;
  sorting: SortFieldsModel;
  protected sortSubscriber: Subscription;
  rowsPerPage$: Observable<number>;

  constructor(
    protected route: ActivatedRoute,
    protected rootStore: Store<UserConfigState>,
    protected searchStore: Store<SearchStateInterface>,
    protected backendSearchService: BackendSearchService,
    protected urlService: UrlParamsService,
    protected routeEventsService: PreviousRouteServiceProvider,
    protected translate: TranslateService,
    protected location: Location,
    protected router: Router,
    protected breakpointObserver: BreakpointObserver
  ) {
    super(
      route,
      rootStore,
      searchStore,
      backendSearchService,
      urlService,
      routeEventsService,
      translate,
      location,
      router
    );
    this.docs$ = searchStore.pipe(select(getAllResults));
    this.searchCount$ = searchStore.pipe(select(getTotalResultsCount));
    this.sort$ = combineLatest([
      searchStore.pipe(select(getResultSortField)),
      searchStore.pipe(select(getResultSortOrder)),
    ]);
    this.sortSubscriber = this.sort$.subscribe((v) => {
      this.sorting = this.findSortField(v[0], v[1]);
    });
    this.offset$ = searchStore.pipe(select(getResultOffset));
    this.rowsPerPage$ = searchStore.pipe(select(getResultRows));
  }

  resultFormatter(n: number): string {
    return new Intl.NumberFormat(this.translate.currentLang, {
      style: "decimal",
    }).format(n);
  }

  findSortField(sortField: string, sortDir: string): SortFieldsModel {
    if (this.sortFields) {
      for (const sortFieldConfig of this.sortFields) {
        if (
          sortField === sortFieldConfig.field &&
          sortDir === sortFieldConfig.order.toString()
        ) {
          return sortFieldConfig;
        }
      }
    }
    return undefined;
  }

  switchSorting(newSorting: SortFieldsModel) {
    this.searchStore.dispatch(new SetSortOrder(newSorting.order));
    this.searchStore.dispatch(new SetSortField(newSorting.field));
    this.searchStore.dispatch(new SetOffset(0));
    this.searchStore.dispatch(new SimpleSearch());
  }

  setSearchOffset(offset) {
    window.scrollTo(0, 0);
    this.searchStore.dispatch(new SetOffset(offset));
    this.searchStore.dispatch(new SimpleSearch());
  }

  pageButtonNr(): number {
    return environment.searchResultSettings?.pageButtonsInBar ?? 5;
  }

  get env(): SettingsModel {
    return environment;
  }
}
