import { ParcSettingsModel } from "@app/shared/models/AfrikaportalSettingsModel";

// dummy to satisfy IDE
// will be replaced with real settings
export const environment: ParcSettingsModel = {} as ParcSettingsModel;
