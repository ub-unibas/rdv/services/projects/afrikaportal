import { initParcLib } from "@app/init";
import { ParcSettingsModel } from "@app/shared/models/AfrikaportalSettingsModel";
import { parcEndpoints } from "@app/shared/models/endpoints";
import { environment as proj } from "@env/afrikaportal/environment";
import { environment as compose } from "@env_temp/environment.type-compose";
import { addComposeNamePostfix } from "@env_temp/util";

export const environment: ParcSettingsModel = {
  ...compose,
  ...proj,

  headerSettings: addComposeNamePostfix(proj.headerSettings),

  ...parcEndpoints("afrikaportal-compose", compose),
};

initParcLib(environment);
