/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import { initParcLib } from "@app/init";
import { ParcSettingsModel } from "@app/shared/models/AfrikaportalSettingsModel";
import {
  Backend,
  DisplayConstraintType,
  ExtraInfoFieldType,
  FacetFieldType,
  FacetValueOrder,
  HistogramFieldModel,
  HistogramFieldType,
  MiradorPlugins,
  Page,
  SortOrder,
  ViewerType,
} from "@ub-unibas/rdv_viewer-lib/components";
import { MapProvider, RenderHint } from "@ub-unibas/rdv_viewer-lib/tool";

export const environment: ParcSettingsModel = {
  production: false,

  backend: Backend.ELASTIC,
  // unter welcher Domaine (und Verzeichnis) läuft der RDV (wird benutzt um Links zu generieren)
  baseUrl: "http://ub-ap.ub.unibas.ch/afrikaportal_v6",

  proxyUrl: undefined,
  moreProxyUrl: undefined,
  inFacetSearchProxyUrl: undefined,
  popupQueryProxyUrl: undefined,
  documentViewerProxyUrl: undefined,
  countriesProxyUrl: undefined,

  // default is "/"
  searchUrl: "/search",

  // Header-Anzeige-Einstellungen
  headerSettings: {
    // Fallback für alle Sprachen
    default: {
      // i18n key für den Portal-Namen
      // name: "top.headerSettings.name",
      // wenn Angabe fehlt oder false ist, wird der Header angezeigt; bei true nicht
      disable: false,
      // Portal-Namen unterhalb des Headers anzeigen
      showPortalName: true,
      // wenn true, wird ein anderer Text als Department-Name verwendet, anstatt des Portal-Namens
      // (i18n key: top.headerSettings.logoSubTitle)
      useLogoSubTitle: true,
      // optional dieses Logo an der rechten Seite anzeigen
      // (i18n key: top.headerSettings.departmentLogoUrl, top.headerSettings.departmentUrl)
      useDepartmentLogoUrl: false,
      // den Sprachauswahl-Abschnitt in der UI ausblenden bei true, ansonsten anzeigen
      // disableLanguageBar: false,
      // Beta-Balken anzeigen, wenn true (nicht angezeigt, wenn Eintrag fehlt oder false)
      // (i18n keys: top.headerSettings.betaBarContact.name, top.headerSettings.betaBarContact.email)
      showBetaBar: true,
    },
    // sprachspezifische Einstellungen (ohne Übersetzungen)
    // "de": {
    // },
    // "en": {
    // }
  },

  // sollten mehrere documentViewer enabled sein, wir dem User in der UI eine
  // Auswahlmöglichkeit gegeben
  documentViewer: {
    // [ViewerType.UV]: { // not working if aot/prod is built
    UV: {
      enabled: true,
      type: ViewerType.UV,
      enabledForPage: [
        { view: Page.Search, order: 20 },
        { view: Page.Detail, order: 20 },
      ], // default: on all views
      viewerUrl: "assets/uv/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/zas_en.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "hidden_digitized",
          messageTitleKey:
            "search-results.view-mode.UV.object_type.message.title",
          messageTextKey:
            "search-results.view-mode.UV.object_type.message.text",
          values: [{ label: "ja", value: { id: "ja" } }],
        },
      ],
    },
    UVScroll: {
      enabled: true,
      type: ViewerType.UV,
      enabledForPage: [{ view: Page.Search, order: 20 }], // default: on all views
      viewerUrl: "assets/uv/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/scroll_view.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "hidden_digitized",
          messageTitleKey:
            "search-results.view-mode.UV.object_type.message.title",
          messageTextKey:
            "search-results.view-mode.UV.object_type.message.text",
          values: [{ label: "ja", value: { id: "ja" } }],
        },
      ],
    },
    // [ViewerType.MIRADOR]: { // not working if aot/prod is built
    Mirador: {
      enabled: true,
      type: ViewerType.MIRADOR,
      enabledForPage: [{ view: Page.Search, order: 10 }],
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_singlenopreview.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "hidden_digitized",
          messageTitleKey:
            "search-results.view-mode.UV.object_type.message.title",
          messageTextKey:
            "search-results.view-mode.UV.object_type.message.text",
          values: [{ label: "ja", value: { id: "ja" } }],
        },
      ],
    },
    MiradorScroll: {
      enabled: false,
      enabledForPage: [{ view: Page.Search, order: 10 }],
      type: ViewerType.MIRADOR,
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // mögliche Werte: "share", "download","image-tools","text-overlay"
      disabledPlugins: ["text-overlay"],

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl:
        "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_scroll.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "hidden_digitized",
          messageTitleKey:
            "search-results.view-mode.UV.object_type.message.title",
          messageTextKey:
            "search-results.view-mode.UV.object_type.message.text",
          values: [{ label: "ja", value: { id: "ja" } }],
        },
      ],
    },
    MiradorSinglePage: {
      enabled: true,
      enabledForPage: [{ view: Page.Detail, order: 10 }],
      type: ViewerType.MIRADOR,
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl:
        "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_singlenopreview.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "hidden_digitized",
          messageTitleKey:
            "search-results.view-mode.UV.object_type.message.title",
          messageTextKey:
            "search-results.view-mode.UV.object_type.message.text",
          values: [{ label: "ja", value: { id: "ja" } }],
        },
      ],
    },
  },

  defaultDocumentViewerName: "List",

  //Welche Felder sind durchsuchbar, Anzahl der Felder in preselect regelt wie viele Suchfelder erscheinen
  searchFields: {
    options: {
      all_text: "Freitext",
      "fct_location.search": "Geographikum",
      "fct_mediatype.search": "Datentr\u00e4ger/Inhaltstyp",
      "fct_person_organisation.search": "Person/Institution",
      // "fct_topic.search": "Schlagwort",
      "gnd_topic.search": "Schlagwort",
      field_title: "Titel/Benennung",
    },
    preselect: ["all_text"],
  },

  //Infos zu Filtern (z.B. Filterung nach Einrichtung)
  filterFields: {},

  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  facetFields: {
    fct_year: {
      field: "fct_year",
      facetType: FacetFieldType.HISTOGRAM,
      data_type: HistogramFieldType.INT,
      label: "env.facetFields.fct_year",
      operator: "AND",
      showAggs: false,
      order: 6,
      size: 31,
      expandAmount: 31,
    } as HistogramFieldModel,
    fct_countryname: {
      field: "country_coordinates.id.keyword",
      facetType: FacetFieldType.SIMPLE,
      label: "env.facetFields.fct_countryname",
      operator: "AND",
      order: 2,
      expandAmount: 10,
      translatesValues: true,
      size: 200,
    },
    fct_institution: {
      field: "fct_institution.keyword",
      facetType: FacetFieldType.SIMPLE,
      label: "env.facetFields.fct_institution",
      operator: "OR",
      order: 0,
      expandAmount: 10,
      translatesValues: true,
    },
    fct_coord: {
      field: "coordinates.location",
      facetType: FacetFieldType.HISTOGRAM,
      data_type: HistogramFieldType.GEO,
      label: "env.facetFields.fct_coord",
      operator: "AND",
      order: 0,
      translatesValues: true,
      size: 300,
      valueOrder: FacetValueOrder.COUNT,
      valueOrders: [FacetValueOrder.COUNT],
      hidden: true,
    } as HistogramFieldModel,
    fct_location: {
      field: "fct_location.keyword",
      facetType: FacetFieldType.SIMPLE,
      label: "env.facetFields.fct_location",
      operator: "AND",
      order: 3,
      expandAmount: 10,
      searchWithin: true,
      autocomplete_size: 3,
    },
    hidden_digitized: {
      field: "hidden_digitized.keyword",
      facetType: FacetFieldType.BOOLEAN,
      label: "env.facetFields.hidden_digitized",
      operator: "OR",
      order: 2,
      expandAmount: 10,
    },
    fct_mediatype: {
      field: "fct_mediatype.keyword",
      facetType: FacetFieldType.SIMPLE,
      label: "env.facetFields.fct_mediatype",
      operator: "OR",
      order: 1,
      expandAmount: 10,
      translatesValues: true,
      size: 100,
    },
    fct_person_organisation: {
      field: "fct_person_organisation.keyword",
      facetType: FacetFieldType.SIMPLE,
      label: "env.facetFields.fct_person_organisation",
      operator: "AND",
      operators: ["AND", "OR"],
      order: 7,
      expandAmount: 10,
      searchWithin: true,
      autocomplete_size: 3,
    },
    // fct_topic: {
    //   field: "fct_topic.keyword",
    //   facetType: FacetFieldType.SIMPLE,
    //   label: "env.facetFields.fct_topic",
    //   operator: "AND",
    //   order: 4,
    //   expandAmount: 10,
    //   searchWithin: true,
    //   autocomplete_size: 3,
    // },
    gnd_topic: {
      field: "gnd_topic.id.keyword",
      facetType: FacetFieldType.SIMPLE,
      label: "env.facetFields.gnd_topic",
      operator: "AND",
      order: 5,
      expandAmount: 10,
      searchWithin: true,
      autocomplete_size: 3,
      displayConstraints: [
        { type: DisplayConstraintType.LANGUAGE, value: ["de"] },
      ],
    },
    gnd_translation: {
      field: "gnd_translation.keyword",
      facetType: FacetFieldType.SIMPLE,
      label: "env.facetFields.gnd_translation",
      operator: "AND",
      order: 6,
      expandAmount: 10,
      searchWithin: true,
      autocomplete_size: 3,
      displayConstraints: [
        { type: DisplayConstraintType.LANGUAGE, value: ["en"] },
      ],
    },

    // removed for speed
    // "hierarchy_filter.keyword": {
    //   field: "hierarchy_filter.keyword",
    //   facetType: FacetFieldType.HIERARCHIC,
    //   label: "BAB Thesaurus",
    //   operator: "AND",
    //   order: 8,
    //   size: 100,
    //   expandAmount: 10,
    // },
  },
  hideBoolFacetsInList: true,

  // @Depreacted: only used in "old" search form, for new search add to "facetFields"
  //Infos zu Ranges (z.B. Label)
  //order gilt fuer Facetten und Ranges
  rangeFields: {},

  //Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [20, 50, 100, 200],

  queryParams: {
    rows: 20,
    offset: 0,
    sortField: "_score",
    sortOrder: SortOrder.DESC,
  },

  sortFields: [
    {
      field: "_score",
      order: SortOrder.DESC,
      display: "select-sort-fields.score_desc",
    },
    {
      field: "fct_year",
      order: SortOrder.ASC,
      display: "select-sort-fields.date_asc",
    },
    {
      field: "fct_year",
      order: SortOrder.DESC,
      display: "select-sort-fields.date_desc",
    },
  ],

  //Config fuer Merkliste
  basketConfig: {
    queryParams: {
      rows: 10,
      sortField: "_score",
      sortOrder: SortOrder.DESC,
    },
  },

  showExportList: {
    basket: false,
    table: false,
  },

  //Tabellenspalten mit Sortierkriterium (Solr-Feld oder false)
  tableFields: [],

  //Welche Felder sollen in zusaetzlicher Zeile angezeigt werden
  extraInfos: {
    fct_location: {
      display: ExtraInfoFieldType.TEXT,
      field: "fct_location",
      label: "Geographikum",
    },
    fct_mediatype: {
      display: ExtraInfoFieldType.TEXT,
      field: "fct_mediatype",
      label: "Datentr\u00e4ger/Inhaltstyp",
    },
    // fct_topic: {
    //   display: ExtraInfoFieldType.TEXT,
    //   field: "fct_topic",
    //   label: "Schlagwort",
    // },
    gnd_topic: {
      display: ExtraInfoFieldType.TEXT,
      field: "gnd_topic",
      label: "Schlagwort",
    },
    field_date_input_values: {
      display: ExtraInfoFieldType.TEXT,
      field: "field_date_input_values",
      label: "Zeit (Ausgangswert)",
    },
  },

  i18n: {
    de: {
      "top.headerSettings.name": "Portal for African research collections",
      "top.headerSettings.name.Dev":
        "Portal for African research collections (Dev)",
      "top.headerSettings.name.Loc":
        "Portal for African research collections (Loc)",
      "top.headerSettings.name.Test":
        "Portal for African research collections (Test)",
      "top.headerSettings.betaBarContact.name": "Reto Ulrich",
      "top.headerSettings.betaBarContact.email": "ru@baslerafrika.ch",
      "env.facetFields.fct_year": "Zeitraum",
      "env.facetFields.fct_countryname": "Land",
      "env.facetFields.fct_institution": "Sammlungen",
      "env.facetFields.fct_location": "Region/Ort",
      "env.facetFields.hidden_digitized": "Digitalisiert",
      "env.facetFields.fct_mediatype": "Ressourcentyp",
      "env.facetFields.fct_person_organisation": "Person/Institution",
      "env.facetFields.fct_topic": "Thema",
      "env.facetFields.gnd_topic": "Thema",
      "env.facetFields.gnd_translation": "Thema",
      "env.facetFields.fct_coord": "Koordinaten",
    },
    en: {
      "top.headerSettings.name": "Portal for African research collections",
      "top.headerSettings.name.Dev":
        "Portal for African research collections (Dev)",
      "top.headerSettings.name.Loc":
        "Portal for African research collections (Loc)",
      "top.headerSettings.name.Test":
        "Portal for African research collections (Test)",
      "top.headerSettings.betaBarContact.name": "Reto Ulrich",
      "top.headerSettings.betaBarContact.email": "ru@baslerafrika.ch",
      "env.facetFields.fct_year": "Period",
      "env.facetFields.fct_countryname": "Country",
      "env.facetFields.fct_institution": "Collections",
      "env.facetFields.fct_location": "Region/Place",
      "env.facetFields.hidden_digitized": "Digitized",
      "env.facetFields.fct_mediatype": "Type of resource",
      "env.facetFields.fct_person_organisation": "Person/Institution",
      "env.facetFields.fct_topic": "Subject",
      "env.facetFields.gnd_topic": "Subject",
      "env.facetFields.gnd_translation": "Subject",
      "env.facetFields.fct_coord": "Coordinates",
    },
  },
  searchResultSettings: {
    showHitNumber: false,
    showHitObjectType: true,
    paginationMethod: "bar",
    pageButtonsInBar: 5, // go to last page and check if there is no overflow!
    // order of view style buttons (left top side on the search result page)
    viewStyleButtons: [
      // "PAGE_SIZE_SELECTOR",
      "LIST_STYLE_BUTTON",
      // "IIIF_SELECTOR",
      "IIIF_BUTTON_GROUP",
    ],
    showKeepFiltersButton: true,
  },

  mapConfig: {
    config: {
      // style is an url or json object conforming to https://mapbox.com/mapbox-gl-style-spec/
      // created with mapbox studio
      style: "mapbox://styles/mapbox/streets-v11",
      zoom: 2.5,
      lat: -10.554314452399936,
      lng: 21.807009732286303,
    },
    provider: MapProvider.MAPBOX,
    coordinateFacetKey: "fct_coord",
    yearFacetKey: "fct_year",
    // zoom range: 2.5 .. 14
    // precision range: 1 .. 12
    zoomPrecisionMapping: [
      { zoomStart: 2.5, zoomEnd: 3.75, precision: 2 },
      { zoomStart: 3.75, zoomEnd: 6.42, precision: 3 },
      { zoomStart: 6.42, zoomEnd: 8.42, precision: 4 },
      { zoomStart: 8.42, zoomEnd: 10, precision: 5 },
      { zoomStart: 10, zoomEnd: 12, precision: 6 },
      { zoomStart: 12, zoomEnd: 14, precision: 7 },
    ],
    positionHint: [RenderHint.CENTROID, RenderHint.CENTER, RenderHint.NE],
    projection:  { name: 'mercator' }
  },
  countryFacetKey: "fct_countryname",
  exhibitionProxyUrl: undefined,
};

initParcLib(environment, [MiradorPlugins.PLUGIN_OCR_HELPER]);
