import { initParcLib } from "@app/init";
import { ParcSettingsModel } from "@app/shared/models/AfrikaportalSettingsModel";
import { parcEndpoints } from "@app/shared/models/endpoints";
import { environment as proj } from "@env/afrikaportal/environment";
import { environment as loc } from "@env_temp/environment.type-loc";
import { addLocNamePostfix } from "@env_temp/util";

export const environment: ParcSettingsModel = {
  ...loc,
  ...proj,

  headerSettings: addLocNamePostfix(proj.headerSettings),

  ...parcEndpoints("afrikaportal-loc", loc),
};

initParcLib(environment);
