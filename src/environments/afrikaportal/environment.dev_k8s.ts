import { initParcLib } from "@app/init";
import { ParcSettingsModel } from "@app/shared/models/AfrikaportalSettingsModel";
import { parcEndpoints } from "@app/shared/models/endpoints";
import { environment as proj } from "@env/afrikaportal/environment";
import { environment as dev } from "@env_temp/environment.type-dev_k8s";
import { addTestNamePostfix } from "@env_temp/util";

export const environment: ParcSettingsModel = {
  ...dev,
  ...proj,
  headerSettings: addTestNamePostfix(proj.headerSettings),

  ...parcEndpoints("afrikaportal-dev_k8s", dev),
};

initParcLib(environment);
