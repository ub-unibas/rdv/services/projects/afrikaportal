import { initParcLib } from "@app/init";
import { ParcSettingsModel } from "@app/shared/models/AfrikaportalSettingsModel";
import { parcEndpoints } from "@app/shared/models/endpoints";
import { environment as proj } from "@env/afrikaportal/environment";
import { environment as prod } from "@env_temp/environment.type-prod_k8s";

export const environment: ParcSettingsModel = {
  ...proj,
  ...prod,

  ...parcEndpoints("afrikaportal-prod_k8s", prod),
};

initParcLib(environment);
