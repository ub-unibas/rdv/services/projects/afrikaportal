import { initParcLib } from "@app/init";
import { ParcSettingsModel } from "@app/shared/models/AfrikaportalSettingsModel";
import { parcEndpoints } from "@app/shared/models/endpoints";
import { environment as proj } from "@env/afrikaportal/environment";
import { environment as outermedia } from "@env_temp/environment.type-out";
import { addLocNamePostfix } from "@env_temp/util";

export const environment: ParcSettingsModel = {
  ...outermedia,
  ...proj,

  headerSettings: addLocNamePostfix(proj.headerSettings),

  ...parcEndpoints("afrikaportal-test", outermedia),
};

initParcLib(environment);
