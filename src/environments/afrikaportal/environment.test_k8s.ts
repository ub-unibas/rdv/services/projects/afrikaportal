import { initParcLib } from "@app/init";
import { ParcSettingsModel } from "@app/shared/models/AfrikaportalSettingsModel";
import { parcEndpoints } from "@app/shared/models/endpoints";
import { environment as proj } from "@env/afrikaportal/environment";
import { environment as test } from "@env_temp/environment.type-test_k8s";
import { addTestNamePostfix } from "@env_temp/util";

export const environment: ParcSettingsModel = {
  ...test,
  ...proj,
  headerSettings: addTestNamePostfix(proj.headerSettings),

  ...parcEndpoints("afrikaportal-test_k8s", test),
};

initParcLib(environment);
