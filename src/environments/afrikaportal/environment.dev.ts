import { initParcLib } from "@app/init";
import { ParcSettingsModel } from "@app/shared/models/AfrikaportalSettingsModel";
import { parcEndpoints } from "@app/shared/models/endpoints";
import { environment as proj } from "@env/afrikaportal/environment";
import { environment as dev } from "@env_temp/environment.type-dev";
import { addDevNamePostfix } from "@env_temp/util";

export const environment: ParcSettingsModel = {
  ...proj,
  ...dev,
  headerSettings: addDevNamePostfix(proj.headerSettings),

  ...parcEndpoints("afrikaportal-test", dev),
};

initParcLib(environment);
