import { ParcBaseSettings } from "@app/shared/models/AfrikaportalSettingsModel";
import { parcEndpointBasePaths } from "@app/shared/models/endpoints";
import { viewer } from "@env_temp/viewer";

//used in docker-compose setting
export const environment: ParcBaseSettings = {
  ...viewer,
  production: false,
  editable: false,

  ...parcEndpointBasePaths("http://127.0.0.1:5000/v2"),
};
