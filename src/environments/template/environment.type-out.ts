import { ParcBaseSettings } from "@app/shared/models/AfrikaportalSettingsModel";
import { parcEndpointBasePaths } from "@app/shared/models/endpoints";
import { viewer } from "@env_temp/viewer";

export const environment: ParcBaseSettings = {
  ...viewer,
  production: false,
  ...parcEndpointBasePaths("https://ub-rdv-outermedia-proxy.ub.unibas.ch/v2"),
};
