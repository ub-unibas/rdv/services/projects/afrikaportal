import { ParcBaseSettings } from "@app/shared/models/AfrikaportalSettingsModel";
import { parcEndpointBasePaths } from "@app/shared/models/endpoints";
import { viewer } from "@env_temp/viewer";

export const environment: ParcBaseSettings = {
  ...viewer,
  production: true,

  ...parcEndpointBasePaths(
    "https://dev.rdv-query-builder.ub-digitale-dienste.k8s-001.unibas.ch/v2"
  ),
};
