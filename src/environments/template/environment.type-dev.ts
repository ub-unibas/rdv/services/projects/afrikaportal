import { ParcBaseSettings } from "@app/shared/models/AfrikaportalSettingsModel";
import { parcEndpointBasePaths } from "@app/shared/models/endpoints";
import { viewer } from "@env_temp/viewer";

export const environment: ParcBaseSettings = {
  ...viewer,
  production: false,

  // creatable: [
  //   {
  //     label: {
  //       "de": "retrodigitalisierter Zeitungsausschnitt",
  //       "en": "scanned press clipping"
  //     },
  //     value: "dizas"
  //   },
  //   {
  //     label: {
  //       "de": "elektronischer Zeitungsausschnitt (ab 2013)",
  //       "en": "digitial press clipping (since 2013)"
  //     },
  //     value: "ezas"
  //   }
  // ],

  editable: false,
  ...parcEndpointBasePaths("https://ub-rdv-dev22-proxy.ub.unibas.ch/v2"),
};
